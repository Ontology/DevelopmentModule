﻿using DevelopmentModule.Models;
using OntologyClasses.BaseClasses;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DevelopmentModule.Factories
{
    public class BaseDataFactory
    {

        public List<LanguageItem> CreateLanguagelist(List<clsOntologyItem> oLanguageList, clsOntologyItem emptyItem)
        {
            var emptyLanguage = new LanguageItem
            {
                IdLanguage = emptyItem.GUID,
                NameLanguage = emptyItem.Name
            };
            var languageList = oLanguageList.Select(lngItm => new LanguageItem
            {
                IdLanguage = lngItm.GUID,
                NameLanguage = lngItm.Name
            }).ToList();

            languageList.Insert(0, emptyLanguage);
            return languageList;
        }

        public List<LanguageItem> CreateAdditionalLanguagelist(List<clsOntologyItem> oLanguageList)
        {
            
            var languageList = oLanguageList.Select(lngItm => new LanguageItem
            {
                IdLanguage = lngItm.GUID,
                NameLanguage = lngItm.Name
            }).ToList();

            return languageList;
        }

        public List<PLanguageItem> CreatePLanguagelist(List<clsOntologyItem> oPLanguageList, clsOntologyItem emptyItem)
        {
            var emptyLanguage = new PLanguageItem
            {
                IdPLanguage = emptyItem.GUID,
                NamePLanguage = emptyItem.Name
            };
            var languageList = oPLanguageList.Select(lngItm => new PLanguageItem
            {
                IdPLanguage = lngItm.GUID,
                NamePLanguage = lngItm.Name
            }).ToList();

            languageList.Insert(0, emptyLanguage);
            return languageList;
        }

        public List<UserItem> CreateUserlist(List<clsOntologyItem> oUserList, clsOntologyItem emptyItem)
        {
            var emptyLanguage = new UserItem
            {
                IdUser = emptyItem.GUID,
                NameUser = emptyItem.Name
            };
            var userList = oUserList.Select(lngItm => new UserItem
            {
                IdUser = lngItm.GUID,
                NameUser = lngItm.Name
            }).ToList();

            userList.Insert(0, emptyLanguage);
            return userList;
        }

        public List<StateItem> CreateStatelist(List<clsOntologyItem> oStateList, clsOntologyItem emptyItem)
        {
            var emptyLanguage = new StateItem
            {
                IdState = emptyItem.GUID,
                NameState = emptyItem.Name
            };
            var userList = oStateList.Select(lngItm => new StateItem
            {
                IdState = lngItm.GUID,
                NameState = lngItm.Name
            }).ToList();

            userList.Insert(0, emptyLanguage);
            return userList;
        }
    }
}
