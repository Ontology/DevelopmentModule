﻿using DevelopmentModule.Notifications;
using Newtonsoft.Json;
using OntologyClasses.BaseClasses;
using OntoMsg_Module.Models;
using OntoWebCore.Models;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DevelopmentModule.Factories
{
    public class ViewTreeFactory
    {
        private clsLocalConfig localConfig;

        private List<clsOntologyItem> projectList;
        private List<clsObjectRel> developmentTree;
        private List<clsObjectRel> projectToDevelopment;

        public List<ViewTreeNode> RootTreeNodes { get; private set; }

        public ViewTreeNode CreateRootNode()
        {
            return new ViewTreeNode
            {
                Id = localConfig.Globals.Root.GUID,
                Name = localConfig.Globals.Root.Name
            };

        }

        public ViewTreeNode CrateSearchNode()
        {
            return new ViewTreeNode
            {
                Id = Guid.NewGuid().ToString(),
                Name = "Search"
            };
        }

        public clsOntologyItem GetSelectedNode(string idNode)
        {
            if (idNode == localConfig.Globals.Root.GUID)
            {
                return localConfig.Globals.Root;
            }

            var projectNode = projectList.FirstOrDefault(projNode => projNode.GUID == idNode);

            if (projectNode != null)
            {
                return projectNode.Clone() ;
            }

            var devNode = projectToDevelopment.FirstOrDefault(dev => dev.ID_Other == idNode);

            if (devNode != null)
            {
                return new clsOntologyItem
                {
                    GUID = devNode.ID_Other,
                    Name = devNode.Name_Other,
                    GUID_Parent = localConfig.OItem_type_softwaredevelopment.GUID,
                    Type = localConfig.Globals.Type_Object,
                    Level = 0
                };
                
            }

            devNode = developmentTree.FirstOrDefault(dev => dev.ID_Object == idNode);

            if (devNode != null)
            {
                return new clsOntologyItem
                {
                    GUID = devNode.ID_Object,
                    Name = devNode.Name_Object,
                    GUID_Parent = localConfig.OItem_type_softwaredevelopment.GUID,
                    Type = localConfig.Globals.Type_Object,
                    Level = 1
                };
            }

            return null;
        }


        public clsOntologyItem WriteJson(List<clsOntologyItem> projectList, List<clsObjectRel> developmentTree, List<clsObjectRel> projectToDevelopment, SessionFile streamSessionFile, string idParent = null, string search = null)
        {
            var result = localConfig.Globals.LState_Success.Clone();

            this.projectList = projectList;
            this.developmentTree = developmentTree;
            this.projectToDevelopment = projectToDevelopment;

            // Suche
            if (!string.IsNullOrEmpty(search))
            {
                var projNodes = projectList.Where(project => project.Name.ToLower().Contains(search.ToLower())).Select(project => new ViewTreeNode
                {
                    Id = project.GUID,
                    Name =  System.Web.HttpUtility.HtmlEncode(project.Name),
                    NameNodeImage = Notifications.NotifyChanges.NodeIconProject,
                    ChildCount = projectToDevelopment.Count(projToDev => projToDev.ID_Object == project.GUID)
                }).ToList();

                RootTreeNodes = projNodes;

                var devNodes = projectToDevelopment.Where(projToDev => projToDev.Name_Other.ToLower().Contains(search.ToLower())).Select(projToDev => new ViewTreeNode
                {
                    Id = projToDev.ID_Other,
                    Name = System.Web.HttpUtility.HtmlEncode(projToDev.Name_Other),
                    NameNodeImage = Notifications.NotifyChanges.NodeIconDevelopment,
                    ChildCount = developmentTree.Count(dev => dev.ID_Other == projToDev.ID_Other)
                });

                RootTreeNodes.AddRange(devNodes);

                var subDevNodes = projectToDevelopment.Where(subNode => subNode.Name_Object.ToLower().Contains(search.ToLower())).Select(projToDev => new ViewTreeNode
                {
                    Id = projToDev.ID_Other,
                    Name = System.Web.HttpUtility.HtmlEncode(projToDev.Name_Other),
                    NameNodeImage = Notifications.NotifyChanges.NodeIconDevelopment,
                    ChildCount = developmentTree.Count(dev => dev.ID_Other == projToDev.ID_Other)
                });
            }
            // Development
            else if (!string.IsNullOrEmpty(idParent) && idParent != localConfig.Globals.Root.GUID)
            {
                var developmentsOfProjects = projectToDevelopment.Where(projToDev => projToDev.ID_Object == idParent);
                if (developmentsOfProjects.Any())
                {
                    RootTreeNodes = developmentsOfProjects.Select(devTreeItem => new ViewTreeNode
                    {
                        Id = devTreeItem.ID_Other,
                        Name = System.Web.HttpUtility.HtmlEncode(devTreeItem.Name_Other),
                        NameNodeImage = Notifications.NotifyChanges.NodeIconSubDevelopment,
                        ChildCount = developmentTree.Count(dev => dev.ID_Other == devTreeItem.ID_Other)
                    }).ToList();
                }
                else
                {
                    RootTreeNodes = developmentTree.Where(devTreItem => devTreItem.ID_Other == idParent).Select(devTreeItem => new ViewTreeNode
                    {
                        Id = devTreeItem.ID_Object,
                        Name = System.Web.HttpUtility.HtmlEncode(devTreeItem.Name_Object),
                        NameNodeImage = Notifications.NotifyChanges.NodeIconSubDevelopment,
                        ChildCount = developmentTree.Count(dev => dev.ID_Other == devTreeItem.ID_Object)
                    }).ToList();
                }
                

            }
            // Projects
            else
            {
                RootTreeNodes = projectList.Select(proj => new ViewTreeNode
                {
                    Id = proj.GUID,
                    Name = System.Web.HttpUtility.HtmlEncode(proj.Name),
                    NameNodeImage = Notifications.NotifyChanges.NodeIconProject,
                    ChildCount = projectToDevelopment.Count(projToDev => projToDev.ID_Object == proj.GUID)
                }).ToList();

            }

            result = localConfig.Globals.LState_Success.Clone();

            RootTreeNodes.ForEach(rootNode =>
            {
                if (rootNode.ChildCount > 0)
                {
                    rootNode.Html = string.Format(HTMLTemplates.ListDiv, rootNode.Name, rootNode.ChildCount.ToString(), "fa fa-plus-square-o", rootNode.NameNodeImage);
                }
                else
                {
                    rootNode.Html = string.Format(HTMLTemplates.ListDiv, rootNode.Name, rootNode.ChildCount.ToString(), "", rootNode.NameNodeImage);
                }

            });

            result = WriteJsonDoc(streamSessionFile);
            return result;
            
        }

        private clsOntologyItem WriteJsonDoc(SessionFile sessionFile)
        {
            try
            {
                bool success = true;
                using (sessionFile.StreamWriter)
                {
                    using (var jsonWriter = new JsonTextWriter(sessionFile.StreamWriter))
                    {
                        jsonWriter.WriteStartArray();

                        var rootTreeNodes = RootTreeNodes.OrderBy(rootNode => rootNode.Name).ToList();
                        rootTreeNodes.ForEach(rootTreeNode =>
                        {
                            rootTreeNode.WriteJSON(jsonWriter);
                        });
                        
                        jsonWriter.WriteEndArray();
                    }
                }
                
                if (success)
                {
                    return localConfig.Globals.LState_Success.Clone();
                }
                else
                {
                    return localConfig.Globals.LState_Error.Clone();
                }

            }
            catch (Exception ex)
            {
                return localConfig.Globals.LState_Error.Clone();
            }
        }
        
        public List<ViewTreeNode> CreatePathNodes(List<clsOntologyItem> projectList, List<clsObjectRel> projectToDevelopment, List<clsObjectRel> developmentTree, string idNode)
        {
            var treeNodeList = new List<ViewTreeNode>();
            treeNodeList.Add(CreateRootNode());
            if (string.IsNullOrEmpty(idNode) || idNode == localConfig.Globals.Root.GUID) return treeNodeList;
            var projectItem = projectList.FirstOrDefault(proj => proj.GUID == idNode);

            if (projectItem != null)
            {
                
                treeNodeList.Add(new ViewTreeNode
                {
                    Id = projectItem.GUID,
                    Name = System.Web.HttpUtility.HtmlEncode(projectItem.Name)
                });
                return treeNodeList;
            }

            var developmentItem = developmentTree.FirstOrDefault(proj => proj.ID_Object == idNode || proj.ID_Other == idNode);

            if (developmentItem == null) return treeNodeList;

            
            var node = new ViewTreeNode
            {
                Id = idNode,
                Name = System.Web.HttpUtility.HtmlEncode( developmentItem.ID_Object == idNode ? developmentItem.Name_Object : developmentItem.Name_Other)
            };

            var nodeList = new List<ViewTreeNode> { node };

            var parentItem = developmentTree.FirstOrDefault(dev => dev.ID_Object == node.Id);

            while (parentItem != null)
            {
                nodeList.Insert(0,new ViewTreeNode
                {
                    Id = parentItem.ID_Other,
                    Name = System.Web.HttpUtility.HtmlEncode(parentItem.Name_Other)
                });

                parentItem = developmentTree.FirstOrDefault(dev => dev.ID_Object == parentItem.ID_Other);
            }

            var firstDev = nodeList.FirstOrDefault();

            if (firstDev == null)
            {
                treeNodeList.AddRange(nodeList);
                return treeNodeList;
            }


            var project = projectToDevelopment.FirstOrDefault(projToDev => projToDev.ID_Other == firstDev.Id);


            if (project == null)
            {
                treeNodeList.AddRange(nodeList);
                return treeNodeList;
            }

            nodeList.Insert(0,new ViewTreeNode
            {
                Id = project.ID_Object,
                Name = project.Name_Object
            });

            treeNodeList.AddRange(nodeList);
            return treeNodeList;
        }

        public ViewTreeFactory(clsLocalConfig localConfig)
        {
            this.localConfig = localConfig;
        }
    }
}
