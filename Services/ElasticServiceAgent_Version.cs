﻿using OntologyAppDBConnector;
using OntologyAppDBConnector.Base;
using OntologyClasses.BaseClasses;
using OntoMsg_Module.Base;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DevelopmentModule.Services
{
    public class ElasticServiceAgent_Version : NotifyPropertyChange
    {
        private clsLocalConfig localConfig;

        private object serviceLocker = new object();

        private clsOntologyItem resultDepList;
        public clsOntologyItem ResultDepList
        {
            get { return resultDepList; }
            set
            {
                resultDepList = value;
                RaisePropertyChanged(nameof(ResultDepList));
            }
        }

        private List<clsOntologyItem> dependentList;
        public List<clsOntologyItem> DependentList
        {
            get { return dependentList; }
            set
            {
                dependentList = value;
                RaisePropertyChanged(nameof(DependentList));
            }
        }

        public void StopRead()
        {

        }

        public clsOntologyItem GetOItem(string guidItem, string type)
        {
            var dbReaderOItem = new OntologyModDBConnector(localConfig.Globals);
            return dbReaderOItem.GetOItem(guidItem, type);
        }

        public async Task<List<clsOntologyItem>> GetDependentDevelopments(clsOntologyItem oItemDev)
        {

            var depList = new List<clsOntologyItem>();
            var result = localConfig.Globals.LState_Success.Clone();
            lock (serviceLocker)
            {
                result = GetDependentSubDevelopments(oItemDev, depList);
            }
            
            DependentList = depList;
            ResultDepList = result;
            return depList;
        }

        private clsOntologyItem GetDependentSubDevelopments(clsOntologyItem oItemDev, List<clsOntologyItem> depList)
        {
            clsOntologyItem result = localConfig.Globals.LState_Success.Clone();
            var dbReader = new OntologyModDBConnector(localConfig.Globals);
            
            if (!depList.Any(depItm => depItm.GUID == oItemDev.GUID))
            {
                var searchDependent = new List<clsObjectRel>
                {
                    new clsObjectRel
                    {
                        ID_Other = oItemDev.GUID,
                        ID_RelationType = localConfig.OItem_relationtype_needs.GUID,
                        ID_Parent_Object = localConfig.OItem_type_softwaredevelopment.GUID
                    }
                };

                result = dbReader.GetDataObjectRel(searchDependent);


                if (result.GUID == localConfig.Globals.LState_Success.GUID)
                {
                    dbReader.ObjectRels.ForEach(dep =>
                    {
                        var depDev = new clsOntologyItem
                        {
                            GUID = dep.ID_Object,
                            Name = dep.Name_Object,
                            GUID_Parent = dep.ID_Parent_Object
                        };
                        depList.Add(depDev);
                        result = GetDependentSubDevelopments(depDev, depList);

                    });
                }
            }
            
            

            return result;
        }

        public ElasticServiceAgent_Version(clsLocalConfig localConfig)
        {
            this.localConfig = localConfig;
        }
    }
}
