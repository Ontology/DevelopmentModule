﻿using DevelopmentModule.Notifications;
using OntologyAppDBConnector;
using OntologyAppDBConnector.Base;
using OntologyClasses.BaseClasses;
using OntoMsg_Module.Base;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace DevelopmentModule.Services
{
    public class ElasticServiceAgent_DevTree : NotifyPropertyChange
    {
        private clsLocalConfig localConfig;

        private OntologyModDBConnector dbReaderProjects;
        private OntologyModDBConnector dbReaderDevelopments;
        private OntologyModDBConnector dbReaderDevToProject;

        private Thread getSoftwareProjectsAsync;

        private clsOntologyItem resultData_Projects;
        public clsOntologyItem ResultData_Projects
        {
            get { return resultData_Projects; }
            set
            {
                resultData_Projects = value;
                RaisePropertyChanged(NotifyChanges.Elastic_ResultData_Projects);
            }
        }

        public List<clsOntologyItem> Projects
        {
            get
            {
                return dbReaderProjects.Objects1;
            }
        }

        public List<clsObjectRel> ProjectsToDevelopments
        {
            get
            {
                return dbReaderDevToProject.ObjectRels;
            }
        }

        public List<clsObjectRel> DevelopmentTree
        {
            get
            {
                return dbReaderDevelopments.ObjectRels;
            }
        }

        public ElasticServiceAgent_DevTree(clsLocalConfig localConfig)
        {
            this.localConfig = localConfig;
            Initialize();
        }

        public clsOntologyItem GetSoftwareProjects()
        {
            Abort();
            getSoftwareProjectsAsync = new Thread(GetSoftwareProjectsAsync);
            getSoftwareProjectsAsync.Start();

            return localConfig.Globals.LState_Success.Clone();
        }

        private void GetSoftwareProjectsAsync()
        {
            var result = Get_001_SoftwareProjects();

            if (result.GUID == localConfig.Globals.LState_Error.GUID)
            {
                ResultData_Projects = result;
            }

            result = Get_002_DevelopmentTree();

            if (result.GUID == localConfig.Globals.LState_Error.GUID)
            {
                ResultData_Projects = result;
            }

            result = Get_003_DevToProject();

            ResultData_Projects = result;
        }

        private clsOntologyItem Get_001_SoftwareProjects()
        {
            var searchSoftwareProjects = new List<clsOntologyItem>
            {
                new clsOntologyItem
                {
                    GUID_Parent = localConfig.OItem_class_software_project.GUID
                }
            };

            var result = dbReaderProjects.GetDataObjects(searchSoftwareProjects);

            return result;
        }

        private clsOntologyItem Get_002_DevelopmentTree()
        {
            var searchDevelopmentTree = new List<clsObjectRel>
            {
                new clsObjectRel
                {
                    ID_Parent_Object = localConfig.OItem_type_softwaredevelopment.GUID,
                    ID_RelationType = localConfig.OItem_relationtype_issubordinated.GUID,
                    ID_Parent_Other = localConfig.OItem_type_softwaredevelopment.GUID
                }
            };

            var result = dbReaderDevelopments.GetDataObjectRel(searchDevelopmentTree);

            return result;
        }

        private clsOntologyItem Get_003_DevToProject()
        {
            var searchDevToProject = dbReaderProjects.Objects1.Select(proj => new clsObjectRel
            {
                ID_Object = proj.GUID,
                ID_RelationType = localConfig.OItem_relationtype_contains.GUID,
                ID_Parent_Other = localConfig.OItem_type_softwaredevelopment.GUID
            }).ToList();

            var result = dbReaderDevToProject.GetDataObjectRel(searchDevToProject);

            return result;
        }

        public void Abort()
        {
            if (getSoftwareProjectsAsync != null)
            {
                try
                {
                    getSoftwareProjectsAsync.Abort();
                }
                catch(Exception ex)
                {

                }
            }
        }

        private void Initialize()
        {
            dbReaderProjects = new OntologyModDBConnector(localConfig.Globals);
            dbReaderDevelopments = new OntologyModDBConnector(localConfig.Globals);
            dbReaderDevToProject = new OntologyModDBConnector(localConfig.Globals);
        }
    }
}
