﻿using DevelopmentModule.Notifications;
using OntologyAppDBConnector;
using OntologyAppDBConnector.Base;
using OntologyClasses.BaseClasses;
using OntoMsg_Module.Base;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace DevelopmentModule.Services
{
    public class ElasticServiceAgent_BaseData : NotifyPropertyChange
    {
        private clsLocalConfig localConfig;


        private Thread getDataLanguagesAsync;
        private Thread getDataPLanguagesAsync;
        private Thread getDataUsersAsync;
        private Thread getDataStatesAsync;

        private Thread getDevelopmentDataAsync;

        private clsRelationConfig relationManager;
        private clsTransaction transactionManager;

        private List<clsOntologyItem> states { get; set; }
        public List<clsOntologyItem> States
        {
            get
            {
                return states;
            }
            set
            {
                states = value;
                RaisePropertyChanged(NotifyChanges.ElasticSearchAgent_States);
            }
        }

        private List<clsOntologyItem> users { get; set; }
        public List<clsOntologyItem> Users
        {
            get
            {
                return users;
            }
            set
            {
                users = value;
                RaisePropertyChanged(NotifyChanges.ElasticSearchAgent_Users);
            }
        }

        private List<clsOntologyItem> pLanguages { get; set; }
        public List<clsOntologyItem> PLanguages
        {
            get
            {
                return pLanguages;
            }
            set
            {
                pLanguages = value;
                RaisePropertyChanged(NotifyChanges.ElasticSearchAgent_PLanguages);
            }
        }

        private List<clsOntologyItem> languages { get; set; }
        public List<clsOntologyItem> Languages
        {
            get
            {
                return languages;
            }
            set
            {
                languages = value;
                RaisePropertyChanged(NotifyChanges.ElasticSearchAgent_Languages);
            }
        }

        private clsOntologyItem oItemResultDevelopmentData;
        public clsOntologyItem OItemResultDevelopmentData
        {
            get { return oItemResultDevelopmentData; }
            set
            {
                oItemResultDevelopmentData = value;
                RaisePropertyChanged(NotifyChanges.ElasticSearchAgent_DevData);
            }
        }

        public ElasticServiceAgent_BaseData(clsLocalConfig localConfig)
        {
            this.localConfig = localConfig;

            Initialize();
        }

        public void StopRead()
        {
            StopReadLanguages();
            StopReadPLanguages();
            StopReadUsers();
            StopReadStates();
            StopReadDevData();
        }

        public void StopReadLanguages()
        {
            if (getDataLanguagesAsync != null)
            {
                try
                {
                    getDataLanguagesAsync.Abort();
                }
                catch (Exception ex) { }
            }
            
        }

        public void StopReadPLanguages()
        {
            if (getDataPLanguagesAsync != null)
            {
                try
                {
                    getDataPLanguagesAsync.Abort();
                }
                catch (Exception ex) { }
            }

        }

        public void StopReadUsers()
        {
            if (getDataUsersAsync != null)
            {
                try
                {
                    getDataUsersAsync.Abort();
                }
                catch (Exception ex) { }
            }

        }

        public void StopReadStates()
        {
            if (getDataStatesAsync != null)
            {
                try
                {
                    getDataStatesAsync.Abort();
                }
                catch (Exception ex) { }
            }

        }

        public void StopReadDevData()
        {
            if (getDevelopmentDataAsync != null)
            {
                try
                {
                    getDevelopmentDataAsync.Abort();
                }
                catch (Exception ex) { }
            }

        }


        public clsObjectRel ProjectToDev { get; private set; }
        public clsObjectRel DevToState { get; private set; }
        public clsObjectRel DevToVersion { get; private set; }
        public clsObjectRel DevToCreator { get; private set; }
        public clsObjectRel DevToPlanguage { get; private set; }
        public clsObjectRel DevToFolder { get; private set; }
        public clsObjectRel DevToVersionSubPath { get; private set; }
        public clsObjectRel DevToProjectFile { get; private set; }
        public clsObjectRel DevToStdLanguage { get; private set; }
        public List<clsObjectRel> DevToAdditionalLanguages { get; private set; }

        public clsOntologyItem GetDataBase()
        {
            StopReadLanguages();

            getDataLanguagesAsync = new Thread(GetDataLanguagesAsync);
            getDataLanguagesAsync.Start();

            getDataPLanguagesAsync = new Thread(GetDataPLanguagesAsync);
            getDataPLanguagesAsync.Start();

            getDataUsersAsync = new Thread(GetDataUsersAsync);
            getDataUsersAsync.Start();

            getDataStatesAsync = new Thread(GetDataStatesAsync);
            getDataStatesAsync.Start();


            return localConfig.Globals.LState_Success.Clone();
        }
        
        private void GetDataLanguagesAsync()
        {
            var dbReader = new OntologyModDBConnector(localConfig.Globals);

            var searchLanguages = new List<clsOntologyItem>
            {
                new clsOntologyItem
                {
                    GUID_Parent = localConfig.OItem_type_language.GUID
                }
            };

            var result = dbReader.GetDataObjects(searchLanguages);
            if (result.GUID == localConfig.Globals.LState_Success.GUID)
            {
                Languages = dbReader.Objects1;
            }
            else
            {
                Languages = null;
            }
        }

        private void GetDataPLanguagesAsync()
        {
            var dbReader = new OntologyModDBConnector(localConfig.Globals);

            var searchPLanguages = new List<clsOntologyItem>
            {
                new clsOntologyItem
                {
                    GUID_Parent = localConfig.OItem_type_programinglanguage.GUID
                }
            };

            var result = dbReader.GetDataObjects(searchPLanguages);
            if (result.GUID == localConfig.Globals.LState_Success.GUID)
            {
                PLanguages = dbReader.Objects1;
            }
            else
            {
                PLanguages = null;
            }
        }

        private void GetDataUsersAsync()
        {
            var dbReader = new OntologyModDBConnector(localConfig.Globals);

            var searchUsers = new List<clsOntologyItem>
            {
                new clsOntologyItem
                {
                    GUID_Parent = localConfig.OItem_type_user.GUID
                }
            };

            var result = dbReader.GetDataObjects(searchUsers);
            if (result.GUID == localConfig.Globals.LState_Success.GUID)
            {
                Users = dbReader.Objects1;
            }
            else
            {
                Users = null;
            }
        }

        private void GetDataStatesAsync()
        {
            var dbReader = new OntologyModDBConnector(localConfig.Globals);

            var searchStates = new List<clsOntologyItem>
            {
                new clsOntologyItem
                {
                    GUID_Parent = localConfig.OItem_type_logstate.GUID
                }
            };

            var result = dbReader.GetDataObjects(searchStates);
            if (result.GUID == localConfig.Globals.LState_Success.GUID)
            {
                States = dbReader.Objects1;
            }
            else
            {
                States = null;
            }
        }

        public clsOntologyItem GetDataDevelopment(clsOntologyItem development)
        {
            StopReadDevData();

            getDevelopmentDataAsync = new Thread(GetDevelopmentDataAsync);
            getDevelopmentDataAsync.Start(development);

            return localConfig.Globals.LState_Success.Clone();
        }

        private void GetDevelopmentDataAsync(object developmentItem)
        {
            if (!(developmentItem is clsOntologyItem))
            {
                OItemResultDevelopmentData = localConfig.Globals.LState_Error.Clone();
            }

            var oItemDev = (clsOntologyItem)developmentItem;

            var dbReaderLeftRight = new OntologyModDBConnector(localConfig.Globals);
            var dbReaderRightLeft = new OntologyModDBConnector(localConfig.Globals);

            var searchDevelopmentLeftRight = new List<clsObjectRel>
            {
                new clsObjectRel
                {
                    ID_Object = oItemDev.GUID,
                    ID_RelationType = localConfig.OItem_relationtype_isinstate.GUID,
                    ID_Parent_Other = localConfig.OItem_type_logstate.GUID
                },
                new clsObjectRel
                {
                    ID_Object = oItemDev.GUID,
                    ID_RelationType = localConfig.OItem_relationtype_isinstate.GUID,
                    ID_Parent_Other = localConfig.OItem_type_developmentversion.GUID
                },
                new clsObjectRel
                {
                    ID_Object = oItemDev.GUID,
                    ID_RelationType = localConfig.OItem_relationtype_project_file.GUID,
                    ID_Parent_Other = localConfig.OItem_type_file.GUID
                },
                new clsObjectRel
                {
                    ID_Object = oItemDev.GUID,
                    ID_RelationType = localConfig.OItem_relationtype_sourceslocatedin.GUID,
                    ID_Parent_Other = localConfig.OItem_type_folder.GUID
                },
                new clsObjectRel
                {
                    ID_Object = oItemDev.GUID,
                    ID_RelationType = localConfig.OItem_relationtype_standard.GUID,
                    ID_Parent_Other = localConfig.OItem_type_language.GUID
                },
                new clsObjectRel
                {
                    ID_Object = oItemDev.GUID,
                    ID_RelationType = localConfig.OItem_relationtype_additional.GUID,
                    ID_Parent_Other = localConfig.OItem_type_language.GUID
                },
                new clsObjectRel
                {
                    ID_Object = oItemDev.GUID,
                    ID_RelationType = localConfig.OItem_relationtype_iswrittenin.GUID,
                    ID_Parent_Other = localConfig.OItem_type_programinglanguage.GUID
                },
                new clsObjectRel
                {
                    ID_Object = oItemDev.GUID,
                    ID_RelationType = localConfig.OItem_relationtype_wasdevelopedby.GUID,
                    ID_Parent_Other = localConfig.OItem_type_user.GUID
                },
                new clsObjectRel
                {
                    ID_Object = oItemDev.GUID,
                    ID_RelationType = localConfig.OItem_relationtype_subpath_versionfile.GUID,
                    ID_Parent_Other = localConfig.OItem_class_path.GUID
                }
            };

            var result = dbReaderLeftRight.GetDataObjectRel(searchDevelopmentLeftRight);

            if (result.GUID == localConfig.Globals.LState_Error.GUID)
            {
                OItemResultDevelopmentData = result;
            }

            var searchDevelopmentRightLeft = new List<clsObjectRel>
            {
                new clsObjectRel
                {
                    ID_Other = oItemDev.GUID,
                    ID_RelationType = localConfig.OItem_relationtype_contains.GUID,
                    ID_Parent_Object = localConfig.OItem_class_software_project.GUID
                }
            };

            result = dbReaderRightLeft.GetDataObjectRel(searchDevelopmentRightLeft);

            if (result.GUID == localConfig.Globals.LState_Success.GUID)
            {
                DevToAdditionalLanguages = dbReaderLeftRight.ObjectRels.Where(leftRight => leftRight.ID_RelationType == localConfig.OItem_relationtype_additional.GUID && leftRight.ID_Parent_Other == localConfig.OItem_type_language.GUID).ToList();
                DevToCreator = dbReaderLeftRight.ObjectRels.FirstOrDefault(leftRight => leftRight.ID_RelationType == localConfig.OItem_relationtype_wasdevelopedby.GUID && leftRight.ID_Parent_Other == localConfig.OItem_type_user.GUID);
                DevToFolder = dbReaderLeftRight.ObjectRels.FirstOrDefault(leftRight => leftRight.ID_RelationType == localConfig.OItem_relationtype_sourceslocatedin.GUID && leftRight.ID_Parent_Other == localConfig.OItem_type_folder.GUID);
                DevToPlanguage = dbReaderLeftRight.ObjectRels.FirstOrDefault(leftRight => leftRight.ID_RelationType == localConfig.OItem_relationtype_iswrittenin.GUID && leftRight.ID_Parent_Other == localConfig.OItem_type_programinglanguage.GUID);
                DevToProjectFile = dbReaderLeftRight.ObjectRels.FirstOrDefault(leftRight => leftRight.ID_RelationType == localConfig.OItem_relationtype_project_file.GUID && leftRight.ID_Parent_Other == localConfig.OItem_type_file.GUID);
                DevToState = dbReaderLeftRight.ObjectRels.FirstOrDefault(leftRight => leftRight.ID_RelationType == localConfig.OItem_relationtype_isinstate.GUID && leftRight.ID_Parent_Other == localConfig.OItem_type_logstate.GUID);
                DevToStdLanguage = dbReaderLeftRight.ObjectRels.FirstOrDefault(leftRight => leftRight.ID_RelationType == localConfig.OItem_relationtype_standard.GUID && leftRight.ID_Parent_Other == localConfig.OItem_type_language.GUID);
                DevToVersion = dbReaderLeftRight.ObjectRels.FirstOrDefault(leftRight => leftRight.ID_RelationType == localConfig.OItem_relationtype_isinstate.GUID && leftRight.ID_Parent_Other == localConfig.OItem_type_developmentversion.GUID);
                DevToVersionSubPath = dbReaderLeftRight.ObjectRels.FirstOrDefault(leftRight => leftRight.ID_RelationType == localConfig.OItem_relationtype_subpath_versionfile.GUID && leftRight.ID_Parent_Other == localConfig.OItem_class_path.GUID);

                ProjectToDev = dbReaderRightLeft.ObjectRels.FirstOrDefault(rightLeft => rightLeft.ID_RelationType == localConfig.OItem_relationtype_contains.GUID && rightLeft.ID_Parent_Object == localConfig.OItem_class_software_project.GUID);

            }
            OItemResultDevelopmentData = result;
        }

        public clsOntologyItem AddLanguageToAdditionalList(string idLanguage, clsOntologyItem oItemDev)
        {
            var language = Languages.FirstOrDefault(lng => lng.GUID == idLanguage);

            if (language == null) return localConfig.Globals.LState_Error.Clone();

            transactionManager.ClearItems();

            var relDevToTransaction = relationManager.Rel_ObjectRelation(oItemDev, language, localConfig.OItem_relationtype_additional);

            var result = transactionManager.do_Transaction(relDevToTransaction);

            result.add_OItem(language);

            return result;
        }

        public clsOntologyItem RemoveLanguageFromAdditionalList(string idLanguage, clsOntologyItem oItemDev)
        {
            var language = Languages.FirstOrDefault(lng => lng.GUID == idLanguage);

            if (language == null) return localConfig.Globals.LState_Error.Clone();

            transactionManager.ClearItems();

            var relDevToTransaction = relationManager.Rel_ObjectRelation(oItemDev, language, localConfig.OItem_relationtype_additional);

            var result = transactionManager.do_Transaction(relDevToTransaction,boolRemoveItem:true);

            result.add_OItem(language);

            return result;
        }

        public clsOntologyItem RelateItem(string id, clsOntologyItem oItemDev, string idClass)
        {
            clsOntologyItem relationType = null;
            if (idClass == localConfig.OItem_type_language.GUID)
            {
                relationType = localConfig.OItem_relationtype_standard;
            }
            else if (idClass == localConfig.OItem_type_user.GUID)
            {
                relationType = localConfig.OItem_relationtype_wasdevelopedby;
            }
            else if (idClass == localConfig.OItem_type_programinglanguage.GUID)
            {
                relationType = localConfig.OItem_relationtype_iswrittenin;
            }
            else if (idClass == localConfig.OItem_type_logstate.GUID)
            {
                relationType = localConfig.OItem_relationtype_isinstate;
            }
            else
            {
                return localConfig.Globals.LState_Error.Clone();

            }

            clsOntologyItem related = null;
            if (idClass == localConfig.OItem_type_language.GUID)
            {
                related = Languages.FirstOrDefault(lng => lng.GUID == id);
            }
            else if (idClass == localConfig.OItem_type_logstate.GUID)
            {
                related = States.FirstOrDefault(lng => lng.GUID == id);
            }
            else if (idClass == localConfig.OItem_type_user.GUID)
            {
                related = Users.FirstOrDefault(lng => lng.GUID == id);
            }
            else if (idClass == localConfig.OItem_type_programinglanguage.GUID)
            {
                related = PLanguages.FirstOrDefault(lng => lng.GUID == id);
            }

            if (related == null) return localConfig.Globals.LState_Error.Clone();

            return RelateToDev(oItemDev, related, relationType);
        }


        private clsOntologyItem RelateToDev(clsOntologyItem oItemDev, clsOntologyItem oItemRef, clsOntologyItem oItemRelationType)
        {
            transactionManager.ClearItems();

            var relDevToTransaction = relationManager.Rel_ObjectRelation(oItemDev, oItemRef, oItemRelationType);

            var result = transactionManager.do_Transaction(relDevToTransaction, true);

            return result;
        }

        private clsOntologyItem RemoveRelationToDev(clsOntologyItem oItemDev, clsOntologyItem oItemRef, clsOntologyItem oItemRelationType)
        {
            transactionManager.ClearItems();

            var relDevToTransaction = relationManager.Rel_ObjectRelation(oItemDev, oItemRef, oItemRelationType);

            var result = transactionManager.do_Transaction(relDevToTransaction, boolRemoveItem:true);

            return result;
        }

        public clsOntologyItem RelatedSoftwareProject(clsOntologyItem oItemDev, clsOntologyItem oItemSoftwareProject)
        {
            transactionManager.ClearItems();

            if (ProjectToDev != null)
            {
                var dbDeletor = new OntologyModDBConnector(localConfig.Globals);
                var relToDel = new clsObjectRel
                {
                    ID_Other = oItemDev.GUID,
                    ID_Parent_Object = localConfig.OItem_class_software_project.GUID,
                    ID_RelationType = localConfig.OItem_relationtype_contains.GUID
                };

                var resultDel = dbDeletor.DelObjectRels(new List<clsObjectRel> { relToDel });
                if (resultDel.GUID == localConfig.Globals.LState_Error.GUID)
                {
                    return resultDel;
                }
            }
            
            var rel = relationManager.Rel_ObjectRelation(oItemSoftwareProject, oItemDev, localConfig.OItem_relationtype_contains).Clone();

            var result = transactionManager.do_Transaction(rel);

            if (result.GUID == localConfig.Globals.LState_Success.GUID)
            {
                ProjectToDev = rel;
            }

            return result;
        }

        public clsOntologyItem RelatedReference(clsOntologyItem oItemDev, clsOntologyItem oItemRef)
        {
            clsOntologyItem oItemRelationType;
            if (oItemRef.GUID_Parent == localConfig.OItem_type_developmentversion.GUID)
            {
                oItemRelationType = localConfig.OItem_relationtype_isinstate;
            }
            else if (oItemRef.GUID_Parent == localConfig.OItem_type_folder.GUID)
            {
                oItemRelationType = localConfig.OItem_relationtype_sourceslocatedin;
            }
            else if (oItemRef.GUID_Parent == localConfig.OItem_class_path.GUID)
            {
                oItemRelationType = localConfig.OItem_relationtype_subpath_versionfile;
            }
            else if (oItemRef.GUID_Parent == localConfig.OItem_type_file.GUID)
            {
                oItemRelationType = localConfig.OItem_relationtype_project_file;
            }
            else
            {
                return localConfig.Globals.LState_Error.Clone();
            }

            transactionManager.ClearItems();

            var rel = relationManager.Rel_ObjectRelation(oItemDev, oItemRef, oItemRelationType).Clone();

            var result = transactionManager.do_Transaction(rel, true);

            if (result.GUID == localConfig.Globals.LState_Success.GUID)
            {
                if (oItemRef.GUID_Parent == localConfig.OItem_type_developmentversion.GUID)
                {
                    DevToVersion = rel;
                }
                else if (oItemRef.GUID_Parent == localConfig.OItem_type_folder.GUID)
                {
                    DevToFolder = rel;
                }
                else if (oItemRef.GUID_Parent == localConfig.OItem_class_path.GUID)
                {
                    DevToVersionSubPath = rel;
                }
                else if (oItemRef.GUID_Parent == localConfig.OItem_type_file.GUID)
                {
                    DevToProjectFile = rel;
                }
            }

            return result;
        }

        private void Initialize()
        {
            transactionManager = new clsTransaction(localConfig.Globals);
            relationManager = new clsRelationConfig(localConfig.Globals);
        }
    }
}
