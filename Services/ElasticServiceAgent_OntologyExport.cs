﻿using OntologyAppDBConnector;
using OntologyAppDBConnector.Base;
using OntologyClasses.BaseClasses;
using OntoMsg_Module.Base;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DevelopmentModule.Services
{
    public class ElasticServiceAgent_OntologyExport : NotifyPropertyChange
    {
        private clsLocalConfig localConfig;

        private object serviceLocker = new object();

        private clsOntologyItem resultVersionList;
        public clsOntologyItem ResultVersionList
        {
            get { return resultVersionList; }
            set
            {
                resultVersionList = value;
                RaisePropertyChanged(nameof(ResultVersionList));
            }
        }

        public async Task<clsOntologyItem> GetDevelopmentVersions(clsOntologyItem oItemDev)
        {
            var result = localConfig.Globals.LState_Success.Clone();

            return result;
        }
        
        public clsOntologyItem GetOItem(string guidItem, string type)
        {
            var dbReaderOItem = new OntologyModDBConnector(localConfig.Globals);
            return dbReaderOItem.GetOItem(guidItem, type);
        }

        public ElasticServiceAgent_OntologyExport(clsLocalConfig localConfig)
        {
            this.localConfig = localConfig;
        }
    }
}
