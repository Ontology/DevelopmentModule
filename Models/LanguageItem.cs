﻿using OntoWebCore.Attributes;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DevelopmentModule.Models
{
    public class LanguageItem : IRelationItem
    {
        [Json(Property = "Id")]
        [Combobox(FieldType = CellType.String, IsIdField = true, Property = "Id")]
        public string IdLanguage { get; set; }

        [Json(Property = "Name")]
        [Combobox(FieldType = CellType.String, IsIdField = false, Property = "Name")]
        public string NameLanguage { get; set; }

        public string IdRelatedItem
        {
            get { return IdLanguage; }
        }
    }
}
