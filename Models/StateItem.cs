﻿using OntoWebCore.Attributes;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DevelopmentModule.Models
{
    public class StateItem : IRelationItem
    {
        [Json(Property = "Id")]
        [Combobox(FieldType = CellType.String, IsIdField = true, Property = "Id")]
        public string IdState { get; set; }

        [Json(Property = "Name")]
        [Combobox(FieldType = CellType.String, IsIdField = false, Property = "Name")]
        public string NameState { get; set; }

        public string IdRelatedItem
        {
            get { return IdState; }
        }
    }
}
