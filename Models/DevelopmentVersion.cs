﻿using DevelopmentModule.Notifications;
using OntoMsg_Module.Base;
using OntoWebCore.Attributes;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DevelopmentModule.Models
{
    public class DevelopmentVersion : ViewModelBase
    {
        private string version;
		[DataViewColumn(DisplayOrder = 1, IsIdField = false, IsVisible = true)]
        public string Version
        {
            get { return version; }
            set
            {
                if (version == value) return;

                version = value;

                RaisePropertyChanged(NotifyChanges.DataModel_Version);

            }
        }

        private long? major;
		[DataViewColumn(CellType = CellType.Integer, DisplayOrder = 2, IsIdField = false, IsVisible = true)]
        public long? Major
        {
            get { return major; }
            set
            {
                if (major == value) return;

                major = value;

                RaisePropertyChanged(NotifyChanges.DataModel_Major);

            }
        }

        private long? minor;
		[DataViewColumn(CellType = CellType.Integer, DisplayOrder = 3, IsIdField = false, IsVisible = true)]
        public long? Minor
        {
            get { return minor; }
            set
            {
                if (minor == value) return;

                minor = value;

                RaisePropertyChanged(NotifyChanges.DataModel_Minor);

            }
        }

        private long? build;
		[DataViewColumn(CellType = CellType.Integer, DisplayOrder = 4, IsIdField = false, IsVisible = true)]
        public long? Build
        {
            get { return build; }
            set
            {
                if (build == value) return;

                build = value;

                RaisePropertyChanged(NotifyChanges.DataModel_Build);

            }
        }

        private long? revision;
		[DataViewColumn(CellType = CellType.Integer, DisplayOrder = 5, IsIdField = false, IsVisible = true)]
        public long? Revision
        {
            get { return revision; }
            set
            {
                if (revision == value) return;

                revision = value;

                RaisePropertyChanged(NotifyChanges.DataModel_Revision);

            }
        }

        private string idAttributeRevision;
		[DataViewColumn(IsVisible = false)]
        public string IdAttributeRevision
        {
            get { return idAttributeRevision; }
            set
            {
                if (idAttributeRevision == value) return;

                idAttributeRevision = value;

                RaisePropertyChanged(NotifyChanges.DataModel_IdAttributeRevision);

            }
        }

        private string idAttributeBuild;
		[DataViewColumn(IsVisible = false)]
        public string IdAttributeBuild
        {
            get { return idAttributeBuild; }
            set
            {
                if (idAttributeBuild == value) return;

                idAttributeBuild = value;

                RaisePropertyChanged(NotifyChanges.DataModel_IdAttributeBuild);

            }
        }

        private string idAttributeMinor;
		[DataViewColumn(IsVisible = false)]
        public string IdAttributeMinor
        {
            get { return idAttributeMinor; }
            set
            {
                if (idAttributeMinor == value) return;

                idAttributeMinor = value;

                RaisePropertyChanged(NotifyChanges.DataModel_IdAttributeMinor);

            }
        }

        private string idAttributeMajor;
		[DataViewColumn(IsVisible = false)]
        public string IdAttributeMajor
        {
            get { return idAttributeMajor; }
            set
            {
                if (idAttributeMajor == value) return;

                idAttributeMajor = value;

                RaisePropertyChanged(NotifyChanges.DataModel_IdAttributeMajor);

            }
        }

        private string idVersion;
		[DataViewColumn(IsIdField = true, IsVisible = false)]
        public string IdVersion
        {
            get { return idVersion; }
            set
            {
                if (idVersion == value) return;

                idVersion = value;

                RaisePropertyChanged(NotifyChanges.DataModel_IdVersion);

            }
        }

        private string idLogEntry;
		[DataViewColumn(IsIdField = false, IsVisible = false)]
        public string IdLogEntry
        {
            get { return idLogEntry; }
            set
            {
                if (idLogEntry == value) return;

                idLogEntry = value;

                RaisePropertyChanged(NotifyChanges.DataModel_IdLogEntry);

            }
        }

        private DateTime? creationDate;
		[DataViewColumn(CellType = CellType.Date, DisplayOrder = 0, IsIdField = false, IsVisible = true)]
        public DateTime? CreationDate
        {
            get { return creationDate; }
            set
            {
                if (creationDate == value) return;

                creationDate = value;

                RaisePropertyChanged(NotifyChanges.DataModel_CreationDate);

            }
        }
    }
}
