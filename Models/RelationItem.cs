﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DevelopmentModule.Models
{
    public interface IRelationItem
    {
        string IdRelatedItem { get; }
    }
}
