﻿using DevelopmentModule.Services;
using DevelopmentModule.Translations;
using Newtonsoft.Json.Linq;
using OntologyAppDBConnector;
using OntologyClasses.BaseClasses;
using OntoMsg_Module;
using OntoMsg_Module.Attributes;
using OntoMsg_Module.Base;
using OntoMsg_Module.Models;
using OntoMsg_Module.Notifications;
using OntoMsg_Module.StateMachines;
using OntoMsg_Module.WebSocketServices;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;
using System.Runtime.InteropServices;
using System.Text;
using System.Threading.Tasks;

namespace DevelopmentModule.Controllers
{
    public class DevelopmentVersionController: DevelopmentVersionViewModel, IViewController
    {
        private WebsocketServiceAgent webSocketServiceAgent;

        private TranslationController translationController = new TranslationController();

        private ElasticServiceAgent_Version serviceAgentElastic;

        private clsLocalConfig localConfig;

        private List<clsOntologyItem> dependentList;
        private bool dependentViewLoaded;

        public IControllerStateMachine StateMachine { get; private set; }
        public ControllerStateMachine LocalStateMachine
        {
            get
            {
                if (StateMachine == null)
                {
                    return null;
                }

                return (ControllerStateMachine)StateMachine;
            }
        }

        public DevelopmentVersionController()
        {
            localConfig = (clsLocalConfig)LocalConfigManager.GetLocalConfig(((GuidAttribute)Assembly.GetExecutingAssembly().GetCustomAttributes(true).FirstOrDefault(objAttribute => objAttribute is GuidAttribute)).Value);
            if (localConfig == null)
            {
                localConfig = new clsLocalConfig(new Globals());
                LocalConfigManager.AddLocalConfig(localConfig);
            }

            Initialize();
            PropertyChanged += DevelopmentVersionController_PropertyChanged;
        }

        private void DevelopmentVersionController_PropertyChanged(object sender, System.ComponentModel.PropertyChangedEventArgs e)
        {
            var property = ViewModelProperties.FirstOrDefault(viewProperty => viewProperty.Property.Name == e.PropertyName);

            if (property == null) return;

            property.ViewItem.AddValue(property.Property.GetValue(this));

            webSocketServiceAgent.SendPropertyChange(e.PropertyName);

            
        }

        private void Initialize()
        {
            var stateMachine = new ControllerStateMachine(StateMachineType.BlockSelectingSelected | StateMachineType.BlockLoadingSelected);
            stateMachine.IsControllerListen = true;
            StateMachine = stateMachine;
            stateMachine.PropertyChanged += StateMachine_PropertyChanged;
            stateMachine.loadSelectedItem += StateMachine_loadSelectedItem;
            stateMachine.loginSucceded += StateMachine_loginSucceded;
            stateMachine.openedSocket += StateMachine_openedSocket;
            stateMachine.closedSocket += StateMachine_closedSocket;

            serviceAgentElastic = new ElasticServiceAgent_Version(localConfig);
            serviceAgentElastic.PropertyChanged += ServiceAgentElastic_PropertyChanged;
        }

        private void StateMachine_closedSocket()
        {
            webSocketServiceAgent.RemoveAllResources();
            serviceAgentElastic.StopRead();
            serviceAgentElastic = null;
            translationController = null;
            //viewTreeFactory = null;
        }

        private void StateMachine_openedSocket()
        {
            webSocketServiceAgent.RegisterEndpoint(new ChannelEndpoint
            {
                EndPointId = webSocketServiceAgent.EndpointId,
                EndpointType = EndpointType.Sender,
                SessionId = webSocketServiceAgent.DataText_SessionId
            });


            webSocketServiceAgent.SendModel();

        }

        private void StateMachine_loginSucceded()
        {
            
            webSocketServiceAgent.RegisterEndpoint(new ChannelEndpoint
            {
                ChannelTypeId = Channels.ParameterList,
                EndPointId = webSocketServiceAgent.EndpointId,
                EndpointType = EndpointType.Receiver,
                SessionId = webSocketServiceAgent.DataText_SessionId
            });

            
        }

        private void StateMachine_loadSelectedItem(clsOntologyItem oItemSelected)
        {
            Label_Development = string.Format(translationController.Label_Development, oItemSelected.Name);


            Text_View = oItemSelected.Name;
            var versioningUrl = ModuleDataExchanger.GetViewUrlById("f1e5d921c3ab4e8eb467039a041e6b77", sender: webSocketServiceAgent.EndpointId, objectId: oItemSelected.GUID);

            if (versioningUrl != null)
            {
                Url_VersioningView = versioningUrl;
            }

            var dependencyUrl = ModuleDataExchanger.GetViewUrlById("a01c681fd76a40399d995c4d1e72a75c", sender: webSocketServiceAgent.EndpointId);

            if (dependencyUrl != null)
            {
                Url_DependencyView = dependencyUrl;

            }

            var resultTask = serviceAgentElastic.GetDependentDevelopments(oItemSelected);
        }

        private void StateMachine_PropertyChanged(object sender, System.ComponentModel.PropertyChangedEventArgs e)
        {
            if (e.PropertyName == nameof(OntoMsg_Module.StateMachines.ControllerStateMachine.LoginSuccessful))
            {
                IsSuccessful_Login = LocalStateMachine.LoginSuccessful;
            }
        }

        private void ServiceAgentElastic_PropertyChanged(object sender, System.ComponentModel.PropertyChangedEventArgs e)
        {
            if (e.PropertyName == nameof(ElasticServiceAgent_Version.ResultDepList))
            {
                if (serviceAgentElastic.ResultDepList.GUID == localConfig.Globals.LState_Success.GUID)
                {
                    dependentList = serviceAgentElastic.DependentList;
                    LoadDependentList();

                }
            }
        }

        public void InitializeViewController(WebsocketServiceAgent webSocketServiceAgent)
        {
            this.webSocketServiceAgent = webSocketServiceAgent;
            this.webSocketServiceAgent.PropertyChanged += WebSocketServiceAgent_PropertyChanged;
            this.webSocketServiceAgent.comServerOnMessage += WebSocketServiceAgent_comServerOnMessage;
            this.webSocketServiceAgent.comServerOpened += WebSocketServiceAgent_comServerOpened;

        }

       
        private void WebSocketServiceAgent_PropertyChanged(object sender, System.ComponentModel.PropertyChangedEventArgs e)
        {
            if (e.PropertyName == OntoMsg_Module.Notifications.NotifyChanges.Websocket_DataText_SessionId)
            {


            }
            else if (e.PropertyName == OntoMsg_Module.Notifications.NotifyChanges.Websocket_Command_RequestedCommand)
            {
                


            }
            else if (e.PropertyName == OntoMsg_Module.Notifications.NotifyChanges.Websocket_Event_RequestEvent)
            {

            }
            else if (e.PropertyName == OntoMsg_Module.Notifications.NotifyChanges.Websocket_ChangedProperty)
            {
                
            }
            else if (e.PropertyName == NotifyChanges.Websocket_ChangedViewItems)
            {
                GetChangedViewItems();
            }
            else if (e.PropertyName == NotifyChanges.Websocket_ViewArguments)
            {
                var objectParam = webSocketServiceAgent.ViewArguments.FirstOrDefault(arg => arg.KeyFound("object"));

                if (objectParam != null)
                {
                    
                    var oItemObject = serviceAgentElastic.GetOItem(objectParam.Value, localConfig.Globals.Type_Object);

                    if (oItemObject == null) return;
                    if (oItemObject.GUID_Parent != localConfig.OItem_type_softwaredevelopment.GUID) return;

                    LocalStateMachine.SetItemSelected(oItemObject);
                    return;
                }
            }


        }

        private void GetChangedViewItems()
        {
            webSocketServiceAgent.ChangedViewItems.ForEach(changeItem =>
            {
                

            });

        }

        private void WebSocketServiceAgent_comServerOpened()
        {
            var authenticationRequest = new InterServiceMessage
            {
                ChannelId = Channels.Login,
                SenderId = webSocketServiceAgent.EndpointId
            };

            webSocketServiceAgent.SendInterModMessage(authenticationRequest);

        }

        private void WebSocketServiceAgent_comServerOnMessage(OntoMsg_Module.Notifications.InterServiceMessage message)
        {
            if (message.ReceiverId != null && message.ReceiverId != webSocketServiceAgent.EndpointId) return;
            if (message.ChannelId == Channels.ParameterList)
            {
                dependentList = new List<clsOntologyItem>();
                dependentViewLoaded = false;

                if (message.OItems == null || !message.OItems.Any()) return;
                var oitem = message.OItems.Last();

                if (oitem == null) return;

                if (oitem.GUID_Parent == localConfig.OItem_type_softwaredevelopment.GUID)
                {
                    LocalStateMachine.SetItemSelected(oitem);
                    
                }



                //if (Text_SelectedNodeId == oitem.GUID) return;

                //Text_SelectedNodeId = oitem.GUID;

            }
            else if (message.ChannelId == Channels.AppliedObjects)
            {
                var oitemApplied = message.OItems.FirstOrDefault();
                
            }
            else if (message.ChannelId == Channels.ViewReady && message.ReceiverId == webSocketServiceAgent.EndpointId)
            {
                dependentViewLoaded = true;
                LoadDependentList();
            }
        }


        public List<ViewModelProperty> GetViewModelProperties(bool onlySend = true, ViewItemType viewItemType = ViewItemType.All, ViewItemClass viewItemClass = ViewItemClass.All)
        {
            return ViewModelProperties.Where(viewItemProp => (onlySend ? viewItemProp.ViewModelAttribute.Send : 1 == 1)
                && (viewItemType != ViewItemType.All ? viewItemProp.ViewModelAttribute.ViewItemType == viewItemType : 1 == 1)
                && (viewItemClass != ViewItemClass.All ? viewItemProp.ViewModelAttribute.ViewItemClass == viewItemClass : 1 == 1)).ToList();
        }

        private void LoadDependentList()
        {
            if (dependentList == null || !dependentViewLoaded) return;
            var serviceMessage = new InterServiceMessage
            {
                ChannelId = Channels.OItemList,
                OItems = dependentList
            };

            webSocketServiceAgent.SendInterModMessage(serviceMessage);
        }
    }
}
