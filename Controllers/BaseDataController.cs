﻿using DevelopmentModule.Factories;
using DevelopmentModule.Models;
using DevelopmentModule.Services;
using DevelopmentModule.Translations;
using OntologyAppDBConnector;
using OntologyClasses.BaseClasses;
using OntoMsg_Module;
using OntoMsg_Module.Attributes;
using OntoMsg_Module.Base;
using OntoMsg_Module.Models;
using OntoMsg_Module.Notifications;
using OntoMsg_Module.StateMachines;
using OntoMsg_Module.WebSocketServices;
using OntoWebCore.Factories;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;
using System.Runtime.InteropServices;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using System.ComponentModel;

namespace DevelopmentModule.Controllers
{
    class ChangeItem
    {
        public string PropertyNameEnabled { get; private set; }
        public string PropertyNameIndex { get; private set; }
        public clsObjectRel Relation { get; private set; }
        public List<IRelationItem> RelationItems { get; private set; }
        public clsOntologyItem ClassItem { get; private set; }

        public ChangeItem(string propertyNameEnabled, string propertyNameIndex, clsObjectRel relation, List<IRelationItem> relationItems, clsOntologyItem classItem)
        {
            PropertyNameEnabled = propertyNameEnabled;
            PropertyNameIndex = propertyNameIndex;
            Relation = relation;
            RelationItems = relationItems;
            ClassItem = classItem;
        }
    }

    public class BaseDataController: BaseDataViewModel, IViewController
    {
        private object changeLocker = new object();

        private WebsocketServiceAgent webSocketServiceAgent;

        private System.Timers.Timer timerChangesLanguage;
        private System.Timers.Timer timerChangesPLanguage;
        private System.Timers.Timer timerChangesState;
        private System.Timers.Timer timerChangesUser;

        private ElasticServiceAgent_BaseData serviceAgentElastic;

        private TranslationController translationController = new TranslationController();

        private ComboboxFactory<LanguageItem> languageCombobFactory = new ComboboxFactory<LanguageItem>();
        private ComboboxFactory<PLanguageItem> pLanguageCombobFactory = new ComboboxFactory<PLanguageItem>();
        private ComboboxFactory<UserItem> userCombobFactory = new ComboboxFactory<UserItem>();
        private ComboboxFactory<StateItem> stateCombobFactory = new ComboboxFactory<StateItem>();
        private BaseDataFactory baseDataFactory = new BaseDataFactory();

        private clsOntologyItem emptyItem;
        private List<LanguageItem> languages;
        private List<LanguageItem> additionalLanguages;
        private List<PLanguageItem> pLanguages;
        private List<UserItem> users;
        private List<StateItem> states;

        private clsLocalConfig localConfig;

        private string fileNameLanguagesJson;
        private string fileNameAdditionalLanguagesJson;
        private string fileNamePLanguagesJson;
        private string fileNameUsersJson;
        private string fileNameStatesJson;

        private ChangeItem changeItemLanguage;
        private ChangeItem changeItemPLanguage;
        private ChangeItem changeItemUser;
        private ChangeItem changeItemState;

        private clsOntologyItem oItemDevelopment;

        public IControllerStateMachine StateMachine { get; private set; }
        public ControllerStateMachine LocalStateMachine
        {
            get
            {
                if (StateMachine == null)
                {
                    return null;
                }

                return (ControllerStateMachine)StateMachine;
            }
        }

        public BaseDataController()
        {
            localConfig = (clsLocalConfig)LocalConfigManager.GetLocalConfig(((GuidAttribute)Assembly.GetExecutingAssembly().GetCustomAttributes(true).FirstOrDefault(objAttribute => objAttribute is GuidAttribute)).Value);
            if (localConfig == null)
            {
                localConfig = new clsLocalConfig(new Globals());
                LocalConfigManager.AddLocalConfig(localConfig);
            }

            Initialize();
            PropertyChanged += BaseDataController_PropertyChanged;
        }

        private void BaseDataController_PropertyChanged(object sender, System.ComponentModel.PropertyChangedEventArgs e)
        {
            var property = ViewModelProperties.FirstOrDefault(viewProperty => viewProperty.Property.Name == e.PropertyName);

            if (property == null) return;

            property.ViewItem.AddValue(property.Property.GetValue(this));

            webSocketServiceAgent.SendPropertyChange(e.PropertyName);
        }

        private void Initialize()
        {
            var stateMachine = new ControllerStateMachine(StateMachineType.BlockSelectingSelected | StateMachineType.BlockLoadingSelected);
            stateMachine.IsControllerListen = true;
            StateMachine = stateMachine;
            stateMachine.PropertyChanged += StateMachine_PropertyChanged;
            stateMachine.loadSelectedItem += StateMachine_loadSelectedItem;
            stateMachine.loginSucceded += StateMachine_loginSucceded;
            stateMachine.openedSocket += StateMachine_openedSocket;
            stateMachine.closedSocket += StateMachine_closedSocket;

            emptyItem = new clsOntologyItem
            {
                GUID = localConfig.Globals.NewGUID,
                Name = " "
            };

            serviceAgentElastic = new ElasticServiceAgent_BaseData(localConfig);
            serviceAgentElastic.PropertyChanged += ServiceAgentElastic_PropertyChanged;

            timerChangesLanguage = new System.Timers.Timer();
            timerChangesLanguage.Interval = 500;
            timerChangesLanguage.Elapsed += TimerChangesLanguage_Elapsed;

            timerChangesPLanguage = new System.Timers.Timer();
            timerChangesPLanguage.Interval = 500;
            timerChangesPLanguage.Elapsed += TimerChangesPLanguage_Elapsed;

            timerChangesState = new System.Timers.Timer();
            timerChangesState.Interval = 500;
            timerChangesState.Elapsed += TimerChangesState_Elapsed;

            timerChangesUser = new System.Timers.Timer();
            timerChangesUser.Interval = 500;
            timerChangesUser.Elapsed += TimerChangesUser_Elapsed;
        }

        private void StateMachine_closedSocket()
        {
            webSocketServiceAgent.RemoveAllResources();
            serviceAgentElastic.StopRead();
            serviceAgentElastic = null;
            translationController = null;
            //viewTreeFactory = null;
        }

        private void StateMachine_openedSocket()
        {
            webSocketServiceAgent.RegisterEndpoint(new ChannelEndpoint
            {
                EndPointId = webSocketServiceAgent.EndpointId,
                EndpointType = EndpointType.Sender,
                SessionId = webSocketServiceAgent.DataText_SessionId
            });


            Label_AdditionalLanguages = translationController.Label_AdditionalLanguages;
            Label_Creator = translationController.Label_Creator;
            Label_Development = string.Format(translationController.Label_Development, "");
            Label_Folder = translationController.Label_Folder;
            Label_PhysicalVersion = translationController.Label_PhysicalVersion;
            Label_PLanguage = translationController.Label_PLanguage;
            Label_ProjectFile = translationController.Label_ProjectFile;
            Label_State = translationController.Label_State;
            Label_StdLanguage = translationController.Label_StdLanguage;
            Label_Version = translationController.Label_Version;
            Label_VersionSubPath = translationController.Label_VersionSubPath;
            Label_SoftwareProject = translationController.Label_SoftwareProject;

            IsEnabled_ButtonPhysicalVersion = false;
            IsEnabled_ButtonProjectFile = false;
            IsEnabled_ButtonVersion = false;
            IsEnabled_ButtonVersionSubPath = false;
            IsEnabled_CmboCreator = false;
            IsEnabled_CmboPLanguage = false;
            IsEnabled_CmboState = false;
            IsEnabled_CmboStdLanguage = false;
            IsEnabled_ListBoxAdditionalLanguages = false;
            IsEnabled_ButtonFolder = false;
            IsEnabled_Folder = false;
            IsEnabled_PhysicalVersion = false;
            IsEnabled_ProjectFile = false;
            IsEnabled_Version = false;
            IsEnabled_VersionSubPath = false;
            IsEnabled_SoftwareProject = false;

            PlaceHolder_Folder = translationController.PlaceHoder_Folder;
            PlaceHolder_PhysicalVersion = translationController.PlaceHoder_PhysicalVersion;
            PlaceHolder_ProjectFile = translationController.PlaceHoder_ProjectFile;
            PlaceHolder_Version = translationController.PlaceHoder_Version;
            PlaceHolder_VersionSubPath = translationController.PlaceHoder_VersionSubPath;
            PlaceHolder_SoftwareProject = translationController.PlaceHoder_SoftwareProject;

            webSocketServiceAgent.SendModel();
        }

        private void StateMachine_loginSucceded()
        {
            
            webSocketServiceAgent.RegisterEndpoint(new ChannelEndpoint
            {
                ChannelTypeId = Channels.ParameterList,
                EndPointId = webSocketServiceAgent.EndpointId,
                EndpointType = EndpointType.Receiver,
                SessionId = webSocketServiceAgent.DataText_SessionId
            });

            webSocketServiceAgent.RegisterEndpoint(new ChannelEndpoint
            {
                ChannelTypeId = Channels.AppliedObjects,
                EndPointId = webSocketServiceAgent.EndpointId,
                EndpointType = EndpointType.Receiver,
                SessionId = webSocketServiceAgent.DataText_SessionId
            });

            var result = serviceAgentElastic.GetDataBase();

            
        }

        private void StateMachine_loadSelectedItem(clsOntologyItem oItemSelected)
        {
            IsEnabled_ButtonFolder = false;
            IsEnabled_ButtonPhysicalVersion = false;
            IsEnabled_ButtonProjectFile = false;
            IsEnabled_ButtonSoftwareProject = false;
            IsEnabled_ButtonVersion = false;
            IsEnabled_ButtonVersionSubPath = false;
            IsEnabled_CmboCreator = false;
            IsEnabled_CmboPLanguage = false;
            IsEnabled_CmboState = false;
            IsEnabled_CmboStdLanguage = false;
            IsEnabled_Folder = false;
            IsEnabled_ListBoxAdditionalLanguages = false;
            IsEnabled_PhysicalVersion = false;
            IsEnabled_ProjectFile = false;
            IsEnabled_SoftwareProject = false;
            IsEnabled_Version = false;
            IsEnabled_VersionSubPath = false;

            Index_SelectedCreatorId = 0;
            Index_SelectedLanguageId = 0;
            Index_SelectedPlanguageId = 0;
            Index_SelectedStateId = 0;

            DataText_Folder = "";
            DataText_PhysicalVersion = "";
            DataText_ProjectFile = "";
            DataText_SoftwareProject = "";
            DataText_Version = "";
            DataText_VersionSubPath = "";

            oItemDevelopment = oItemSelected;
            Label_Development = string.Format(translationController.Label_Development, oItemDevelopment.Name);

            serviceAgentElastic.GetDataDevelopment(oItemSelected);
        }

        private void StateMachine_PropertyChanged(object sender, PropertyChangedEventArgs e)
        {
            if (e.PropertyName == nameof(OntoMsg_Module.StateMachines.ControllerStateMachine.LoginSuccessful))
            {
                IsSuccessful_Login = LocalStateMachine.LoginSuccessful;
            }
        }

        private void TimerChangesUser_Elapsed(object sender, System.Timers.ElapsedEventArgs e)
        {
            timerChangesUser.Stop();

            lock(changeLocker)
            {
                if (changeItemUser == null) return;

                RelateToDev(changeItemUser.PropertyNameEnabled,
                    changeItemUser.PropertyNameIndex,
                    changeItemUser.Relation,
                    changeItemUser.RelationItems,
                    changeItemUser.ClassItem);
            }
            
        }

        private void TimerChangesState_Elapsed(object sender, System.Timers.ElapsedEventArgs e)
        {
            timerChangesState.Stop();

            lock (changeLocker)
            {
                if (changeItemState == null) return;

                RelateToDev(changeItemState.PropertyNameEnabled,
                    changeItemState.PropertyNameIndex,
                    changeItemState.Relation,
                    changeItemState.RelationItems,
                    changeItemState.ClassItem);
            }
                
        }

        private void TimerChangesPLanguage_Elapsed(object sender, System.Timers.ElapsedEventArgs e)
        {
            timerChangesPLanguage.Stop();

            lock (changeLocker)
            {
                if (changeItemPLanguage == null) return;

                RelateToDev(changeItemPLanguage.PropertyNameEnabled,
                    changeItemPLanguage.PropertyNameIndex,
                    changeItemPLanguage.Relation,
                    changeItemPLanguage.RelationItems,
                    changeItemPLanguage.ClassItem);
            }
                
        }

        private void TimerChangesLanguage_Elapsed(object sender, System.Timers.ElapsedEventArgs e)
        {
            timerChangesLanguage.Stop();

            lock (changeLocker)
            {
                if (changeItemLanguage == null) return;

                RelateToDev(changeItemLanguage.PropertyNameEnabled,
                    changeItemLanguage.PropertyNameIndex,
                    changeItemLanguage.Relation,
                    changeItemLanguage.RelationItems,
                    changeItemLanguage.ClassItem);
            }
                
        }

        private void ServiceAgentElastic_PropertyChanged(object sender, System.ComponentModel.PropertyChangedEventArgs e)
        {
            if (e.PropertyName == Notifications.NotifyChanges.ElasticSearchAgent_Languages)
            {
                if (serviceAgentElastic.Languages  != null)
                {
                    languages = baseDataFactory.CreateLanguagelist(serviceAgentElastic.Languages.OrderBy(lang => lang.Name).ToList(), emptyItem);
                    additionalLanguages = baseDataFactory.CreateAdditionalLanguagelist(serviceAgentElastic.Languages.OrderBy(lang => lang.Name).ToList());

                    fileNameLanguagesJson = Guid.NewGuid().ToString() + ".json";
                    fileNameAdditionalLanguagesJson = Guid.NewGuid().ToString() + ".json";

                    var sessionFile = webSocketServiceAgent.RequestWriteStream(fileNameLanguagesJson);
                    JqxDataSource_Languages = languageCombobFactory.CreateDataSource(languages.ToList<object>(), sessionFile);

                    sessionFile = webSocketServiceAgent.RequestWriteStream(fileNameAdditionalLanguagesJson);
                    JqxDataSource_AdditionalLanguages = languageCombobFactory.CreateDataSource(additionalLanguages.ToList<object>(),sessionFile);
                }
                
            }
            else if (e.PropertyName == Notifications.NotifyChanges.ElasticSearchAgent_PLanguages)
            {
                if (serviceAgentElastic.PLanguages != null)
                {
                    pLanguages = baseDataFactory.CreatePLanguagelist(serviceAgentElastic.PLanguages.OrderBy(lang => lang.Name).ToList(), emptyItem);

                    fileNamePLanguagesJson = Guid.NewGuid().ToString() + ".json";

                    var sessionFile = webSocketServiceAgent.RequestWriteStream(fileNamePLanguagesJson);
                    JqxDataSource_PLanguages = pLanguageCombobFactory.CreateDataSource(pLanguages.ToList<object>(), sessionFile);
                }

            }
            else if (e.PropertyName == Notifications.NotifyChanges.ElasticSearchAgent_Users)
            {
                if (serviceAgentElastic.Users != null)
                {
                    users = baseDataFactory.CreateUserlist(serviceAgentElastic.Users.OrderBy(lang => lang.Name).ToList(), emptyItem);

                    fileNameUsersJson = Guid.NewGuid().ToString() + ".json";

                    var sessionFile = webSocketServiceAgent.RequestWriteStream(fileNameUsersJson);
                    JqxDataSource_Users = userCombobFactory.CreateDataSource(users.ToList<object>(), sessionFile);
                }

            }
            else if (e.PropertyName == Notifications.NotifyChanges.ElasticSearchAgent_States)
            {
                if (serviceAgentElastic.States != null)
                {
                    states = baseDataFactory.CreateStatelist(serviceAgentElastic.States.OrderBy(lang => lang.Name).ToList(), emptyItem);

                    fileNameStatesJson = Guid.NewGuid().ToString() + ".json";

                    var sessionFile = webSocketServiceAgent.RequestWriteStream(fileNameStatesJson);
                    JqxDataSource_States = stateCombobFactory.CreateDataSource(states.ToList<object>(), sessionFile);
                }

            }
            else if (e.PropertyName == Notifications.NotifyChanges.ElasticSearchAgent_DevData)
            {
                if (serviceAgentElastic.OItemResultDevelopmentData.GUID == localConfig.Globals.LState_Success.GUID)
                {
                    if (serviceAgentElastic.DevToFolder != null)
                    {
                        DataText_Folder = serviceAgentElastic.DevToFolder.Name_Other;
                    }

                    if (serviceAgentElastic.DevToProjectFile != null)
                    {
                        DataText_ProjectFile = serviceAgentElastic.DevToProjectFile.Name_Other;
                    }
                    
                    if (serviceAgentElastic.DevToCreator != null)
                    {
                        var user = users.FirstOrDefault(usr => usr.IdUser == serviceAgentElastic.DevToCreator.ID_Other);
                        var ix = users.IndexOf(user);
                        Index_SelectedCreatorId = ix;
                    }

                    if (serviceAgentElastic.DevToStdLanguage != null)
                    {
                        var language = languages.FirstOrDefault(lng => lng.IdLanguage == serviceAgentElastic.DevToStdLanguage.ID_Other);
                        var ix = languages.IndexOf(language);
                        Index_SelectedLanguageId = ix;
                    }

                    if (serviceAgentElastic.DevToPlanguage != null)
                    {
                        var pLanguage = pLanguages.FirstOrDefault(lng => lng.IdPLanguage == serviceAgentElastic.DevToPlanguage.ID_Other);
                        var ix = pLanguages.IndexOf(pLanguage);
                        Index_SelectedPlanguageId = ix;
                    }

                    if (serviceAgentElastic.DevToState != null)
                    {
                        var state = states.FirstOrDefault(lng => lng.IdState == serviceAgentElastic.DevToState.ID_Other);
                        var ix = states.IndexOf(state);
                        Index_SelectedStateId = ix;

                    }
                    if (serviceAgentElastic.DevToAdditionalLanguages != null)
                    {
                        var additionalLanguageIds = new List<int>();
                        serviceAgentElastic.DevToAdditionalLanguages.ForEach(devToAdditionalLanguage =>
                        {
                            var language = additionalLanguages.FirstOrDefault(lng => lng.IdLanguage == devToAdditionalLanguage.ID_Other);
                            var ix = additionalLanguages.IndexOf(language);

                            additionalLanguageIds.Add(ix);
                        });

                        IsEnabled_ListBoxAdditionalLanguages = true;
                        IdList_SelectedAdditionalLanguageIds = additionalLanguageIds;
                    }

                    if (serviceAgentElastic.ProjectToDev != null)
                    {
                        DataText_SoftwareProject = serviceAgentElastic.ProjectToDev.Name_Object;
                    }

                    if (serviceAgentElastic.DevToVersion != null)
                    {
                        DataText_Version = serviceAgentElastic.DevToVersion.Name_Other;
                    }

                    if (serviceAgentElastic.DevToVersionSubPath != null)
                    {
                        DataText_VersionSubPath = serviceAgentElastic.DevToVersionSubPath.Name_Other;
                    }

                    IsEnabled_ButtonFolder = true;
                    IsEnabled_ButtonPhysicalVersion = true;
                    IsEnabled_ButtonProjectFile = true;
                    IsEnabled_ButtonSoftwareProject = true;
                    IsEnabled_ButtonVersion = true;
                    IsEnabled_ButtonVersionSubPath = true;
                    IsEnabled_CmboCreator = true;
                    IsEnabled_CmboPLanguage = true;
                    IsEnabled_CmboState = true;
                    IsEnabled_CmboStdLanguage = true;
                    IsEnabled_ListBoxAdditionalLanguages = true;


                }
            }

        }

        public void InitializeViewController(WebsocketServiceAgent webSocketServiceAgent)
        {
            this.webSocketServiceAgent = webSocketServiceAgent;
            this.webSocketServiceAgent.PropertyChanged += WebSocketServiceAgent_PropertyChanged;
            this.webSocketServiceAgent.comServerOnMessage += WebSocketServiceAgent_comServerOnMessage;
            this.webSocketServiceAgent.comServerOpened += WebSocketServiceAgent_comServerOpened;

        }


        private void WebSocketServiceAgent_PropertyChanged(object sender, System.ComponentModel.PropertyChangedEventArgs e)
        {
            if (e.PropertyName == OntoMsg_Module.Notifications.NotifyChanges.Websocket_DataText_SessionId)
            {


            }
            else if (e.PropertyName == OntoMsg_Module.Notifications.NotifyChanges.Websocket_Command_RequestedCommand)
            {
                if (webSocketServiceAgent.Command_RequestedCommand == "CheckedAdditionalLanguages" && IsEnabled_ListBoxAdditionalLanguages)
                {
                    IsEnabled_ListBoxAdditionalLanguages = false;
                    var id = webSocketServiceAgent.Request["Id"].ToString();

                    var languageItem = additionalLanguages.FirstOrDefault(lng => lng.IdLanguage == id);

                    var ix = additionalLanguages.IndexOf(languageItem);
                    if (IdList_SelectedAdditionalLanguageIds.Contains(ix))
                    {
                        IsEnabled_ListBoxAdditionalLanguages = true;
                        return;
                    }

                    var result = serviceAgentElastic.AddLanguageToAdditionalList(id, oItemDevelopment);

                    if (result.GUID == localConfig.Globals.LState_Success.GUID)
                    {
                        

                        IdList_SelectedAdditionalLanguageIds.Add(ix);
                        IsEnabled_ListBoxAdditionalLanguages = true;
                    }
                }
                else if (webSocketServiceAgent.Command_RequestedCommand == "UnCheckedAdditionalLanguages" && IsEnabled_ListBoxAdditionalLanguages)
                {
                    IsEnabled_ListBoxAdditionalLanguages = false;
                    var id = webSocketServiceAgent.Request["Id"].ToString();

                    var language = languages.FirstOrDefault(lng => lng.IdLanguage == id);

                    var languageItem = additionalLanguages.FirstOrDefault(lng => lng.IdLanguage == id);
                    var ix = additionalLanguages.IndexOf(languageItem);
                    if (!IdList_SelectedAdditionalLanguageIds.Contains(ix))
                    {
                        IsEnabled_ListBoxAdditionalLanguages = true;
                        return;
                    }


                    var result = serviceAgentElastic.RemoveLanguageFromAdditionalList(id, oItemDevelopment);

                    if (result.GUID == localConfig.Globals.LState_Success.GUID)
                    {
                        

                        additionalLanguages.Remove(languageItem);
                        
                        IsEnabled_ListBoxAdditionalLanguages = true;
                    }
                }
                else if (webSocketServiceAgent.Command_RequestedCommand == "SelectedLanguage")
                {
                    timerChangesLanguage.Stop();

                    lock (changeLocker)
                    {
                        changeItemLanguage = new ChangeItem(Notifications.NotifyChanges.ViewModel_IsEnabled_CmboStdLanguage,
                            Notifications.NotifyChanges.ViewModel_Index_SelectedLanguageId,
                            serviceAgentElastic.DevToStdLanguage,
                            languages.ToList<IRelationItem>(),
                            localConfig.OItem_type_language);
                    }
                        

                    timerChangesLanguage.Start();
                    
                }
                else if (webSocketServiceAgent.Command_RequestedCommand == "SelectedPLanguage")
                {
                    timerChangesPLanguage.Stop();

                    lock (changeLocker)
                    {
                        changeItemPLanguage = new ChangeItem(Notifications.NotifyChanges.ViewModel_IsEnabled_CmboPLanguage,
                           Notifications.NotifyChanges.ViewModel_Index_SelectedPlanguageId,
                           serviceAgentElastic.DevToPlanguage,
                           pLanguages.ToList<IRelationItem>(),
                           localConfig.OItem_type_programinglanguage);
                    }

                    timerChangesPLanguage.Start();
                }
                else if (webSocketServiceAgent.Command_RequestedCommand == "SelectedUser")
                {
                    timerChangesUser.Stop();

                    lock (changeLocker)
                    {
                        changeItemUser = new ChangeItem(Notifications.NotifyChanges.ViewModel_IsEnabled_CmboCreator,
                            Notifications.NotifyChanges.ViewModel_Index_SelectedCreatorId,
                            serviceAgentElastic.DevToCreator,
                            users.ToList<IRelationItem>(),
                            localConfig.OItem_type_user);
                    }

                    timerChangesUser.Start();
                }
                else if (webSocketServiceAgent.Command_RequestedCommand == "SelectedState")
                {
                    timerChangesState.Stop();
                    lock (changeLocker)
                    {
                        changeItemState =new ChangeItem(Notifications.NotifyChanges.ViewModel_IsEnabled_CmboState,
                            Notifications.NotifyChanges.ViewModel_Index_SelectedStateId,
                            serviceAgentElastic.DevToState,
                            states.ToList<IRelationItem>(),
                            localConfig.OItem_type_logstate);
                    }
                        

                    timerChangesState.Start();
                }
                

            }
            else if (e.PropertyName == OntoMsg_Module.Notifications.NotifyChanges.Websocket_Event_RequestEvent)
            {

            }
            else if (e.PropertyName == OntoMsg_Module.Notifications.NotifyChanges.Websocket_ChangedProperty)
            {
                
            }


        }

        private void RelateToDev(string propertyNameEnabled, string propertyNameIndex, clsObjectRel relation, List<IRelationItem> relationItems, clsOntologyItem classItem)
        {

            var propertyEnabled = GetViewModelProperties().FirstOrDefault(prop => prop.Property.Name == propertyNameEnabled);
            var propertyIndex = GetViewModelProperties().FirstOrDefault(prop => prop.Property.Name == propertyNameIndex);

            var isEnabledCombo = (bool)propertyEnabled.Property.GetValue(this);
            var index = (int)propertyIndex.Property.GetValue(this);

            if (!isEnabledCombo) return;
            propertyEnabled.Property.SetValue(this, false);
            
            var id = webSocketServiceAgent.Request["Id"].ToString();

            if (id == emptyItem.GUID)
            {
                propertyIndex.Property.SetValue(this, index);
                propertyEnabled.Property.SetValue(this, true);
            }
            else
            {

                if (relation != null && relation.ID_Other == id)
                {
                    propertyEnabled.Property.SetValue(this, true);
                    return;
                }

                var result = serviceAgentElastic.RelateItem(id, oItemDevelopment, classItem.GUID);

                if (result.GUID == localConfig.Globals.LState_Success.GUID)
                {
                    var pLanguage = relationItems.FirstOrDefault(lng => lng.IdRelatedItem == id);
                    var ix = relationItems.IndexOf(pLanguage);
                    propertyIndex.Property.SetValue(this, ix);
                    propertyEnabled.Property.SetValue(this, true);
                }
                else
                {
                    propertyIndex.Property.SetValue(this, index);
                    propertyEnabled.Property.SetValue(this, true);
                }
            }
        }

        private void WebSocketServiceAgent_comServerOpened()
        {
            var authenticationRequest = new InterServiceMessage
            {
                ChannelId = Channels.Login,
                SenderId = webSocketServiceAgent.EndpointId
            };

            webSocketServiceAgent.SendInterModMessage(authenticationRequest);

        }

        private void WebSocketServiceAgent_comServerOnMessage(OntoMsg_Module.Notifications.InterServiceMessage message)
        {
            if (message.ReceiverId != null && message.ReceiverId != webSocketServiceAgent.EndpointId) return;
            if (message.ChannelId == Channels.ParameterList)
            {
                if (message.OItems == null || !message.OItems.Any()) return;
                var oitem = message.OItems.Last();

                if (oitem == null) return;

                if (oitem.GUID_Parent == localConfig.OItem_type_softwaredevelopment.GUID)
                {
                    LocalStateMachine.SetItemSelected(oitem);
                }


                
                //if (Text_SelectedNodeId == oitem.GUID) return;

                //Text_SelectedNodeId = oitem.GUID;

            }
            else if (message.ChannelId == Channels.AppliedObjects)
            {
                var oitemApplied = message.OItems.FirstOrDefault();
                if (oitemApplied != null && oItemDevelopment != null)
                {
                    if (oitemApplied.GUID_Parent == localConfig.OItem_class_software_project.GUID)
                    {
                        var result = serviceAgentElastic.RelatedSoftwareProject(oItemDevelopment, oitemApplied);
                        DataText_SoftwareProject = oitemApplied.Name;
                    }
                    else if (oitemApplied.GUID_Parent == localConfig.OItem_type_folder.GUID ||
                        oitemApplied.GUID_Parent == localConfig.OItem_class_path.GUID ||
                        oitemApplied.GUID_Parent == localConfig.OItem_type_file.GUID)
                    {
                        var result = serviceAgentElastic.RelatedReference(oItemDevelopment, oitemApplied);

                        if (oitemApplied.GUID_Parent == localConfig.OItem_type_folder.GUID)
                        {
                            DataText_Folder = oitemApplied.Name;
                        }
                        else if (oitemApplied.GUID_Parent == localConfig.OItem_class_path.GUID)
                        {
                            DataText_VersionSubPath = oitemApplied.Name;
                        }
                        else if (oitemApplied.GUID_Parent == localConfig.OItem_type_file.GUID)
                        {
                            DataText_ProjectFile = oitemApplied.Name;
                        }

                    }
                }
            }
        }


        public List<ViewModelProperty> GetViewModelProperties(bool onlySend = true, ViewItemType viewItemType = ViewItemType.All, ViewItemClass viewItemClass = ViewItemClass.All)
        {
            return ViewModelProperties.Where(viewItemProp => (onlySend ? viewItemProp.ViewModelAttribute.Send : 1 == 1)
                && (viewItemType != ViewItemType.All ? viewItemProp.ViewModelAttribute.ViewItemType == viewItemType : 1 == 1)
                && (viewItemClass != ViewItemClass.All ? viewItemProp.ViewModelAttribute.ViewItemClass == viewItemClass : 1 == 1)).ToList();
        }
    }

    
}

