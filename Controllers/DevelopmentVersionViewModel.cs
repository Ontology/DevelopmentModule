﻿using DevelopmentModule.Notifications;
using OntoMsg_Module.Attributes;
using OntoMsg_Module.Base;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DevelopmentModule.Controllers
{
    public class DevelopmentVersionViewModel : ViewModelBase
    {
       
        private bool issuccessful_Login;
        [ViewModel(Send = true, ViewItemClass = ViewItemClass.Other, ViewItemId = "loginSuccess", ViewItemType = ViewItemType.Other)]
        public bool IsSuccessful_Login
        {
            get { return issuccessful_Login; }
            set
            {
                if (issuccessful_Login == value) return;

                issuccessful_Login = value;

                RaisePropertyChanged(NotifyChanges.ViewModel_IsSuccessful_Login);

            }
        }

        private string label_Development;
        [ViewModel(Send = true, ViewItemClass = ViewItemClass.Label, ViewItemId = "lblDevelopment", ViewItemType = ViewItemType.Content)]
        public string Label_Development
        {
            get { return label_Development; }
            set
            {
                if (label_Development == value) return;

                label_Development = value;

                RaisePropertyChanged(NotifyChanges.ViewModel_Label_Development);

            }
        }

        private string url_VersioningView;
        [ViewModel(Send = true, ViewItemClass = ViewItemClass.IFrame, ViewItemId = "versionFrame", ViewItemType = ViewItemType.Content)]
        public string Url_VersioningView
        {
            get { return url_VersioningView; }
            set
            {
                if (url_VersioningView == value) return;

                url_VersioningView = value;

                RaisePropertyChanged(NotifyChanges.ViewModel_Url_VersioningView);

            }
        }

        private string url_DependencyView;
        [ViewModel(Send = true, ViewItemClass = ViewItemClass.IFrame, ViewItemId = "dependencyFrame", ViewItemType = ViewItemType.Content)]
        public string Url_DependencyView
        {
            get { return url_DependencyView; }
            set
            {
                if (url_DependencyView == value) return;

                url_DependencyView = value;

                RaisePropertyChanged(NotifyChanges.ViewModel_Url_DependencyView);

            }
        }

        private string text_View;
        [ViewModel(Send = true, ViewItemClass = ViewItemClass.Other, ViewItemId = "viewText", ViewItemType = ViewItemType.Other)]
		public string Text_View
        {
            get { return text_View; }
            set
            {
                if (text_View == value) return;

                text_View = value;

                RaisePropertyChanged(NotifyChanges.View_Text_View);

            }
        }

    }
}
