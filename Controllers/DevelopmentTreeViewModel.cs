﻿using DevelopmentModule.Notifications;
using OntoMsg_Module.Attributes;
using OntoMsg_Module.Base;
using OntoMsg_Module.Models;
using OntoWebCore.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DevelopmentModule.Controllers
{
    public class DevelopmentTreeViewModel : ViewModelBase
    {
        private bool issuccessful_Login;
        [ViewModel(Send = true)]
        public bool IsSuccessful_Login
        {
            get { return issuccessful_Login; }
            set
            {
                if (issuccessful_Login == value) return;

                issuccessful_Login = value;

                RaisePropertyChanged(NotifyChanges.ViewModel_IsSuccessful_Login);

            }
        }

        private string url_TreeData;
        [ViewModel(Send = true)]
        public string Url_TreeData
        {
            get { return url_TreeData; }
            set
            {
                if (url_TreeData == value) return;

                url_TreeData = value;

                RaisePropertyChanged(NotifyChanges.Navigation_Url_TreeData);

            }
        }
        private string text_SelectedNodeId;
        [ViewModel(Send = true)]
        public string Text_SelectedNodeId
        {
            get { return text_SelectedNodeId; }
            set
            {
                if (text_SelectedNodeId == value) return;

                text_SelectedNodeId = value;

                RaisePropertyChanged(NotifyChanges.Navigation_Text_SelectedNodeId);

            }
        }

        private List<ViewTreeNode> viewtreenodes_PathNodes;
        [ViewModel(Send = true)]
        public List<ViewTreeNode> ViewTreeNodes_PathNodes
        {
            get { return viewtreenodes_PathNodes; }
            set
            {
                if (viewtreenodes_PathNodes == value) return;

                viewtreenodes_PathNodes = value;

                RaisePropertyChanged(NotifyChanges.ViewModel_ViewTreeNodes_PathNodes);

            }
        }

        private string text_Search;
        [ViewModel(Send = true)]
        public string Text_Search
        {
            get { return text_Search; }
            set
            {
                if (text_Search == value) return;

                text_Search = value;

                RaisePropertyChanged(NotifyChanges.ViewModel_Text_Search);

            }
        }
    }
}
