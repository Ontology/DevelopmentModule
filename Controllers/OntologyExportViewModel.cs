﻿using DevelopmentModule.Notifications;
using OntoMsg_Module.Attributes;
using OntoMsg_Module.Base;
using OntoWebCore.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DevelopmentModule.Controllers
{
    public class OntologyExportViewModel : ViewModelBase
    {
        private bool issuccessful_Login;
        [ViewModel(Send = true, ViewItemClass = ViewItemClass.Other, ViewItemId = "loginSuccess", ViewItemType = ViewItemType.Other)]
		public bool IsSuccessful_Login
        {
            get { return issuccessful_Login; }
            set
            {
                if (issuccessful_Login == value) return;

                issuccessful_Login = value;

                RaisePropertyChanged(NotifyChanges.ViewModel_IsSuccessful_Login);

            }
        }

        private bool istoggled_Listen;
        [ViewModel(Send = true, ViewItemClass = ViewItemClass.Other, ViewItemId = "isListen", ViewItemType = ViewItemType.Other)]
		public bool IsToggled_Listen
        {
            get { return istoggled_Listen; }
            set
            {
                if (istoggled_Listen == value) return;

                istoggled_Listen = value;

                RaisePropertyChanged(NotifyChanges.ViewModel_IsToggled_Listen);

            }
        }

        private string text_View;
        [ViewModel(Send = true, ViewItemClass = ViewItemClass.Other, ViewItemId = "viewText", ViewItemType = ViewItemType.Other)]
		public string Text_View
        {
            get { return text_View; }
            set
            {
                if (text_View == value) return;

                text_View = value;

                RaisePropertyChanged(NotifyChanges.View_Text_View);

            }
        }

        private bool isenabled_CreateFiles;
        [ViewModel(Send = true, ViewItemClass = ViewItemClass.Button, ViewItemId = "createFiles", ViewItemType = ViewItemType.Enable)]
		public bool IsEnabled_CreateFiles
        {
            get { return isenabled_CreateFiles; }
            set
            {
                if (isenabled_CreateFiles == value) return;

                isenabled_CreateFiles = value;

                RaisePropertyChanged(NotifyChanges.ViewModel_IsEnabled_CreateFiles);

            }
        }

        private string label_CreateFiles;
        [ViewModel(Send = true, ViewItemClass = ViewItemClass.Button, ViewItemId = "createFiles", ViewItemType = ViewItemType.Content)]
		public string Label_CreateFiles
        {
            get { return label_CreateFiles; }
            set
            {
                if (label_CreateFiles == value) return;

                label_CreateFiles = value;

                RaisePropertyChanged(NotifyChanges.ViewModel_Label_CreateFiles);

            }
        }

        private bool isenabled_SaveButton;
        [ViewModel(Send = true, ViewItemClass = ViewItemClass.Button, ViewItemId = "saveFiles", ViewItemType = ViewItemType.Enable)]
		public bool IsEnabled_SaveButton
        {
            get { return isenabled_SaveButton; }
            set
            {
                if (isenabled_SaveButton == value) return;

                isenabled_SaveButton = value;

                RaisePropertyChanged(NotifyChanges.ViewModel_IsEnabled_SaveButton);

            }
        }

        private string label_SaveButton;
        [ViewModel(Send = true, ViewItemClass = ViewItemClass.Button, ViewItemId = "saveFiles", ViewItemType = ViewItemType.Content)]
		public string Label_SaveButton
        {
            get { return label_SaveButton; }
            set
            {
                if (label_SaveButton == value) return;

                label_SaveButton = value;

                RaisePropertyChanged(NotifyChanges.ViewModel_Label_SaveButton);

            }
        }

        private JqxDataSource jqxdatasource_Grid;
        [ViewModel(Send = true, ViewItemClass = ViewItemClass.Grid, ViewItemId = "grid", ViewItemType = ViewItemType.DataSource)]
		public JqxDataSource JqxDataSource_Grid
        {
            get { return jqxdatasource_Grid; }
            set
            {
                if (jqxdatasource_Grid == value) return;

                jqxdatasource_Grid = value;

                RaisePropertyChanged(NotifyChanges.ViewModel_JqxDataSource_Grid);

            }
        }
    }
}
