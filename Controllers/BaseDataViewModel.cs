﻿using DevelopmentModule.Notifications;
using OntoMsg_Module.Attributes;
using OntoMsg_Module.Base;
using OntoWebCore.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DevelopmentModule.Controllers
{
    public class BaseDataViewModel : ViewModelBase
    {
        private bool issuccessful_Login;
        [ViewModel(Send = true, ViewItemClass = ViewItemClass.Other, ViewItemId = "loginSuccess", ViewItemType = ViewItemType.Other)]
        public bool IsSuccessful_Login
        {
            get { return issuccessful_Login; }
            set
            {
                if (issuccessful_Login == value) return;

                issuccessful_Login = value;

                RaisePropertyChanged(NotifyChanges.ViewModel_IsSuccessful_Login);

            }
        }

        private string label_Development;
        [ViewModel(Send = true, ViewItemClass = ViewItemClass.Label, ViewItemId = "lblDevelopment", ViewItemType = ViewItemType.Content)]
        public string Label_Development
        {
            get { return label_Development; }
            set
            {
                if (label_Development == value) return;

                label_Development = value;

                RaisePropertyChanged(NotifyChanges.ViewModel_Label_Development);

            }
        }

        private bool isvisible_Development;
        [ViewModel(Send = true, ViewItemClass = ViewItemClass.Label, ViewItemId = "lblDevelopment", ViewItemType = ViewItemType.Visible)]
        public bool IsVisible_Development
        {
            get { return isvisible_Development; }
            set
            {
                if (isvisible_Development == value) return;

                isvisible_Development = value;

                RaisePropertyChanged(NotifyChanges.ViewModel_IsVisible_Development);

            }
        }

        private string label_State;
        [ViewModel(Send = true, ViewItemClass = ViewItemClass.Label, ViewItemId = "lblState", ViewItemType = ViewItemType.Content)]
        public string Label_State
        {
            get { return label_State; }
            set
            {
                if (label_State == value) return;

                label_State = value;

                RaisePropertyChanged(NotifyChanges.ViewModel_Label_State);

            }
        }

        private bool isvisible_State;
        [ViewModel(Send = true, ViewItemClass = ViewItemClass.Label, ViewItemId = "lblState", ViewItemType = ViewItemType.Visible)]
        public bool IsVisible_State
        {
            get { return isvisible_State; }
            set
            {
                if (isvisible_State == value) return;

                isvisible_State = value;

                RaisePropertyChanged(NotifyChanges.ViewModel_IsVisible_State);

            }
        }

        private string label_Version;
        [ViewModel(Send = true, ViewItemClass = ViewItemClass.Label, ViewItemId = "lblVersion", ViewItemType = ViewItemType.Content)]
        public string Label_Version
        {
            get { return label_Version; }
            set
            {
                if (label_Version == value) return;

                label_Version = value;

                RaisePropertyChanged(NotifyChanges.ViewModel_Label_Version);

            }
        }

        private bool isvisible_Version;
        [ViewModel(Send = true, ViewItemClass = ViewItemClass.Label, ViewItemId = "lblVersion", ViewItemType = ViewItemType.Visible)]
        public bool IsVisible_Version
        {
            get { return isvisible_Version; }
            set
            {
                if (isvisible_Version == value) return;

                isvisible_Version = value;

                RaisePropertyChanged(NotifyChanges.ViewModel_IsVisible_Version);

            }
        }

        private string label_Creator;
        [ViewModel(Send = true, ViewItemClass = ViewItemClass.Label, ViewItemId = "lblCreator", ViewItemType = ViewItemType.Content)]
        public string Label_Creator
        {
            get { return label_Creator; }
            set
            {
                if (label_Creator == value) return;

                label_Creator = value;

                RaisePropertyChanged(NotifyChanges.ViewModel_Label_Creator);

            }
        }

        private bool isvisible_Creator;
        [ViewModel(Send = true, ViewItemClass = ViewItemClass.Label, ViewItemId = "lblCreator", ViewItemType = ViewItemType.Visible)]
        public bool IsVisible_Creator
        {
            get { return isvisible_Creator; }
            set
            {
                if (isvisible_Creator == value) return;

                isvisible_Creator = value;

                RaisePropertyChanged(NotifyChanges.ViewModel_IsVisible_Creator);

            }
        }

        private string label_PLanguage;
        [ViewModel(Send = true, ViewItemClass = ViewItemClass.Label, ViewItemId = "lblPLanguage", ViewItemType = ViewItemType.Content)]
        public string Label_PLanguage
        {
            get { return label_PLanguage; }
            set
            {
                if (label_PLanguage == value) return;

                label_PLanguage = value;

                RaisePropertyChanged(NotifyChanges.ViewModel_Label_PLanguage);

            }
        }

        private bool isvisible_PLanguage;
        [ViewModel(Send = true, ViewItemClass = ViewItemClass.Label, ViewItemId = "lblPLanguage", ViewItemType = ViewItemType.Visible)]
        public bool IsVisible_PLanguage
        {
            get { return isvisible_PLanguage; }
            set
            {
                if (isvisible_PLanguage == value) return;

                isvisible_PLanguage = value;

                RaisePropertyChanged(NotifyChanges.ViewModel_IsVisible_PLanguage);

            }
        }

        private string label_Folder;
        [ViewModel(Send = true, ViewItemClass = ViewItemClass.Label, ViewItemId = "lblFolder", ViewItemType = ViewItemType.Content)]
        public string Label_Folder
        {
            get { return label_Folder; }
            set
            {
                if (label_Folder == value) return;

                label_Folder = value;

                RaisePropertyChanged(NotifyChanges.ViewModel_Label_Folder);

            }
        }

        private bool isvisible_Folder;
        [ViewModel(Send = true, ViewItemClass = ViewItemClass.Label, ViewItemId = "lblFolder", ViewItemType = ViewItemType.Visible)]
        public bool IsVisible_Folder
        {
            get { return isvisible_Folder; }
            set
            {
                if (isvisible_Folder == value) return;

                isvisible_Folder = value;

                RaisePropertyChanged(NotifyChanges.ViewModel_IsVisible_Folder);

            }
        }

        private string label_VersionSubPath;
        [ViewModel(Send = true, ViewItemClass = ViewItemClass.Label, ViewItemId = "lblVersionSubPath", ViewItemType = ViewItemType.Content)]
        public string Label_VersionSubPath
        {
            get { return label_VersionSubPath; }
            set
            {
                if (label_VersionSubPath == value) return;

                label_VersionSubPath = value;

                RaisePropertyChanged(NotifyChanges.ViewModel_Label_VersionSubPath);

            }
        }

        private bool isvisible_VersionSubPath;
        [ViewModel(Send = true, ViewItemClass = ViewItemClass.Label, ViewItemId = "lblVersionSubPath", ViewItemType = ViewItemType.Visible)]
        public bool IsVisible_VersionSubPath
        {
            get { return isvisible_VersionSubPath; }
            set
            {
                if (isvisible_VersionSubPath == value) return;

                isvisible_VersionSubPath = value;

                RaisePropertyChanged(NotifyChanges.ViewModel_IsVisible_VersionSubPath);

            }
        }

        private string label_PhysicalVersion;
        [ViewModel(Send = true, ViewItemClass = ViewItemClass.Label, ViewItemId = "lblPhysicalVersion", ViewItemType = ViewItemType.Content)]
        public string Label_PhysicalVersion
        {
            get { return label_PhysicalVersion; }
            set
            {
                if (label_PhysicalVersion == value) return;

                label_PhysicalVersion = value;

                RaisePropertyChanged(NotifyChanges.ViewModel_Label_PhysicalVersion);

            }
        }

        private bool isvisible_PhysicalVersion;
        [ViewModel(Send = true, ViewItemClass = ViewItemClass.Label, ViewItemId = "lblPhysicalVersion", ViewItemType = ViewItemType.Visible)]
        public bool IsVisible_PhysicalVersion
        {
            get { return isvisible_PhysicalVersion; }
            set
            {
                if (isvisible_PhysicalVersion == value) return;

                isvisible_PhysicalVersion = value;

                RaisePropertyChanged(NotifyChanges.ViewModel_IsVisible_PhysicalVersion);

            }
        }

        private string label_ProjectFile;
        [ViewModel(Send = true, ViewItemClass = ViewItemClass.Label, ViewItemId = "lblProjectFile", ViewItemType = ViewItemType.Content)]
        public string Label_ProjectFile
        {
            get { return label_ProjectFile; }
            set
            {
                if (label_ProjectFile == value) return;

                label_ProjectFile = value;

                RaisePropertyChanged(NotifyChanges.ViewModel_Label_ProjectFile);

            }
        }

        private bool isvisible_ProjectFile;
        [ViewModel(Send = true, ViewItemClass = ViewItemClass.Label, ViewItemId = "lblProjectFile", ViewItemType = ViewItemType.Visible)]
        public bool IsVisible_ProjectFile
        {
            get { return isvisible_ProjectFile; }
            set
            {
                if (isvisible_ProjectFile == value) return;

                isvisible_ProjectFile = value;

                RaisePropertyChanged(NotifyChanges.ViewModel_IsVisible_ProjectFile);

            }
        }

        private string label_StdLanguage;
        [ViewModel(Send = true, ViewItemClass = ViewItemClass.Label, ViewItemId = "lblStdLanguage", ViewItemType = ViewItemType.Content)]
        public string Label_StdLanguage
        {
            get { return label_StdLanguage; }
            set
            {
                if (label_StdLanguage == value) return;

                label_StdLanguage = value;

                RaisePropertyChanged(NotifyChanges.ViewModel_Label_StdLanguage);

            }
        }

        private bool isvisible_StdLanguage;
        [ViewModel(Send = true, ViewItemClass = ViewItemClass.Label, ViewItemId = "lblStdLanguage", ViewItemType = ViewItemType.Visible)]
        public bool IsVisible_StdLanguage
        {
            get { return isvisible_StdLanguage; }
            set
            {
                if (isvisible_StdLanguage == value) return;

                isvisible_StdLanguage = value;

                RaisePropertyChanged(NotifyChanges.ViewModel_IsVisible_StdLanguage);

            }
        }

        private string label_AdditionalLanguages;
        [ViewModel(Send = true, ViewItemClass = ViewItemClass.Label, ViewItemId = "lblAdditionalLanguages", ViewItemType = ViewItemType.Content)]
        public string Label_AdditionalLanguages
        {
            get { return label_AdditionalLanguages; }
            set
            {
                if (label_AdditionalLanguages == value) return;

                label_AdditionalLanguages = value;

                RaisePropertyChanged(NotifyChanges.ViewModel_Label_AdditionalLanguages);

            }
        }

        private bool isvisible_AdditionalLanguages;
        [ViewModel(Send = true, ViewItemClass = ViewItemClass.Label, ViewItemId = "lblAdditionalLanguages", ViewItemType = ViewItemType.Visible)]
        public bool IsVisible_AdditionalLanguages
        {
            get { return isvisible_AdditionalLanguages; }
            set
            {
                if (isvisible_AdditionalLanguages == value) return;

                isvisible_AdditionalLanguages = value;

                RaisePropertyChanged(NotifyChanges.ViewModel_IsVisible_AdditionalLanguages);

            }
        }

        private JqxDataSource jqxdatasource_Languages;
        [ViewModel(Send = true, ViewItemClass = ViewItemClass.Combobox, ViewItemId = "cmboLanguages", ViewItemType = ViewItemType.DataSource)]
        public JqxDataSource JqxDataSource_Languages
        {
            get { return jqxdatasource_Languages; }
            set
            {
                if (jqxdatasource_Languages == value) return;

                jqxdatasource_Languages = value;

                RaisePropertyChanged(NotifyChanges.ViewModel_JqxDataSource_Languages);

            }
        }

        private JqxDataSource jqxdatasource_PLanguages;
        [ViewModel(Send = true, ViewItemClass = ViewItemClass.Combobox, ViewItemId = "cmboPLanguages", ViewItemType = ViewItemType.DataSource)]
        public JqxDataSource JqxDataSource_PLanguages
        {
            get { return jqxdatasource_PLanguages; }
            set
            {
                if (jqxdatasource_PLanguages == value) return;

                jqxdatasource_PLanguages = value;

                RaisePropertyChanged(NotifyChanges.ViewModel_JqxDataSource_PLanguages);

            }
        }

        private JqxDataSource jqxdatasource_Users;
        [ViewModel(Send = true, ViewItemClass = ViewItemClass.Combobox, ViewItemId = "cmboUsers", ViewItemType = ViewItemType.DataSource)]
        public JqxDataSource JqxDataSource_Users
        {
            get { return jqxdatasource_Users; }
            set
            {
                if (jqxdatasource_Users == value) return;

                jqxdatasource_Users = value;

                RaisePropertyChanged(NotifyChanges.ViewModel_JqxDataSource_Users);

            }
        }

        private JqxDataSource jqxdatasource_States;
        [ViewModel(Send = true, ViewItemClass = ViewItemClass.Combobox, ViewItemId = "cmboStates", ViewItemType = ViewItemType.DataSource)]
        public JqxDataSource JqxDataSource_States
        {
            get { return jqxdatasource_States; }
            set
            {
                if (jqxdatasource_States == value) return;

                jqxdatasource_States = value;

                RaisePropertyChanged(NotifyChanges.ViewModel_JqxDataSource_States);

            }
        }

        private JqxDataSource jqxdatasource_AdditionalLanguages;
        [ViewModel(Send = true, ViewItemClass = ViewItemClass.ListBox, ViewItemId = "lstBoxAdditionalLanguages", ViewItemType = ViewItemType.DataSource)]
        public JqxDataSource JqxDataSource_AdditionalLanguages
        {
            get { return jqxdatasource_AdditionalLanguages; }
            set
            {
                if (jqxdatasource_AdditionalLanguages == value) return;

                jqxdatasource_AdditionalLanguages = value;

                RaisePropertyChanged(NotifyChanges.ViewModel_JqxDataSource_AdditionalLanguages);

            }
        }

        private List<int> idlist_SelectedAdditionalLanguageIds;
        [ViewModel(Send = true, ViewItemClass = ViewItemClass.ListBox, ViewItemId = "lstBoxAdditionalLanguages", ViewItemType = ViewItemType.SelectedIndex)]
        public List<int> IdList_SelectedAdditionalLanguageIds
        {
            get { return idlist_SelectedAdditionalLanguageIds; }
            set
            {
                if (idlist_SelectedAdditionalLanguageIds == value) return;

                idlist_SelectedAdditionalLanguageIds = value;

                RaisePropertyChanged(NotifyChanges.ViewModel_IdList_SelectedAdditionalLanguageIds);

            }
        }

        private int index_SelectedPlanguageId;
        [ViewModel(Send = true, ViewItemClass = ViewItemClass.Combobox, ViewItemId = "cmboPLanguages", ViewItemType = ViewItemType.SelectedIndex)]
        public int Index_SelectedPlanguageId
        {
            get { return index_SelectedPlanguageId; }
            set
            {

                index_SelectedPlanguageId = value;

                RaisePropertyChanged(NotifyChanges.ViewModel_Index_SelectedPlanguageId);

            }
        }

        private int index_SelectedCreatorId;
        [ViewModel(Send = true, ViewItemClass = ViewItemClass.Combobox, ViewItemId = "cmboUsers", ViewItemType = ViewItemType.SelectedIndex)]
        public int Index_SelectedCreatorId
        {
            get { return index_SelectedCreatorId; }
            set
            { 

                index_SelectedCreatorId = value;

                RaisePropertyChanged(NotifyChanges.ViewModel_Index_SelectedCreatorId);

            }
        }

        private int index_SelectedStateId;
        [ViewModel(Send = true, ViewItemClass = ViewItemClass.Combobox, ViewItemId = "cmboStates", ViewItemType = ViewItemType.SelectedIndex)]
        public int Index_SelectedStateId
        {
            get { return index_SelectedStateId; }
            set
            {

                index_SelectedStateId = value;

                RaisePropertyChanged(NotifyChanges.ViewModel_Index_SelectedStateId);

            }
        }

        private int index_SelectedLanguageId;
        [ViewModel(Send = true, ViewItemClass = ViewItemClass.Combobox, ViewItemId = "cmboLanguages", ViewItemType = ViewItemType.SelectedIndex)]
        public int Index_SelectedLanguageId
        {
            get { return index_SelectedLanguageId; }
            set
            {

                index_SelectedLanguageId = value;

                RaisePropertyChanged(NotifyChanges.ViewModel_Index_SelectedLanguageId);

            }
        }

        private bool isenabled_ListBoxAdditionalLanguages;
        [ViewModel(Send = true, ViewItemClass = ViewItemClass.ListBox, ViewItemId = "lstBoxAdditionalLanguages", ViewItemType = ViewItemType.Enable)]
        public bool IsEnabled_ListBoxAdditionalLanguages
        {
            get { return isenabled_ListBoxAdditionalLanguages; }
            set
            {

                isenabled_ListBoxAdditionalLanguages = value;

                RaisePropertyChanged(NotifyChanges.ViewModel_IsEnabled_ListBoxAdditionalLanguages);

            }
        }

        private bool isenabled_CmboStdLanguage;
        [ViewModel(Send = true, ViewItemClass = ViewItemClass.Combobox, ViewItemId = "cmboLanguages", ViewItemType = ViewItemType.Enable)]
        public bool IsEnabled_CmboStdLanguage
        {
            get { return isenabled_CmboStdLanguage; }
            set
            {

                isenabled_CmboStdLanguage = value;

                RaisePropertyChanged(NotifyChanges.ViewModel_IsEnabled_CmboStdLanguage);

            }
        }

        private bool isenabled_ButtonProjectFile;
        [ViewModel(Send = true, ViewItemClass = ViewItemClass.Button, ViewItemId = "btnProjectFile", ViewItemType = ViewItemType.Enable)]
        public bool IsEnabled_ButtonProjectFile
        {
            get { return isenabled_ButtonProjectFile; }
            set
            {

                isenabled_ButtonProjectFile = value;

                RaisePropertyChanged(NotifyChanges.ViewModel_IsEnabled_ButtonProjectFile);

            }
        }

        private bool isenabled_ButtonPhysicalVersion;
        [ViewModel(Send = true, ViewItemClass = ViewItemClass.Button, ViewItemId = "btnPhysicalVersion", ViewItemType = ViewItemType.Enable)]
        public bool IsEnabled_ButtonPhysicalVersion
        {
            get { return isenabled_ButtonPhysicalVersion; }
            set
            {

                isenabled_ButtonPhysicalVersion = value;

                RaisePropertyChanged(NotifyChanges.ViewModel_IsEnabled_ButtonPhysicalVersion);

            }
        }

        private bool isenabled_ButtonVersionSubPath;
        [ViewModel(Send = true, ViewItemClass = ViewItemClass.Button, ViewItemId = "btnVersionSubPath", ViewItemType = ViewItemType.Enable)]
        public bool IsEnabled_ButtonVersionSubPath
        {
            get { return isenabled_ButtonVersionSubPath; }
            set
            {

                isenabled_ButtonVersionSubPath = value;

                RaisePropertyChanged(NotifyChanges.ViewModel_IsEnabled_ButtonVersionSubPath);

            }
        }

        private bool isenabled_CmboPLanguage;
        [ViewModel(Send = true, ViewItemClass = ViewItemClass.Combobox, ViewItemId = "cmboPLanguages", ViewItemType = ViewItemType.Enable)]
        public bool IsEnabled_CmboPLanguage
        {
            get { return isenabled_CmboPLanguage; }
            set
            {

                isenabled_CmboPLanguage = value;

                RaisePropertyChanged(NotifyChanges.ViewModel_IsEnabled_CmboPLanguage);

            }
        }

        private bool isenabled_CmboCreator;
        [ViewModel(Send = true, ViewItemClass = ViewItemClass.Combobox, ViewItemId = "cmboUsers", ViewItemType = ViewItemType.Enable)]
        public bool IsEnabled_CmboCreator
        {
            get { return isenabled_CmboCreator; }
            set
            {

                isenabled_CmboCreator = value;

                RaisePropertyChanged(NotifyChanges.ViewModel_IsEnabled_CmboCreator);

            }
        }

        private bool isenabled_ButtonVersion;
        [ViewModel(Send = true, ViewItemClass = ViewItemClass.Button, ViewItemId = "btnVersion", ViewItemType = ViewItemType.Enable)]
        public bool IsEnabled_ButtonVersion
        {
            get { return isenabled_ButtonVersion; }
            set
            {

                isenabled_ButtonVersion = value;

                RaisePropertyChanged(NotifyChanges.ViewModel_IsEnabled_ButtonVersion);

            }
        }

        private bool isenabled_CmboState;
        [ViewModel(Send = true, ViewItemClass = ViewItemClass.Combobox, ViewItemId = "cmboStates", ViewItemType = ViewItemType.Enable)]
        public bool IsEnabled_CmboState
        {
            get { return isenabled_CmboState; }
            set
            {

                isenabled_CmboState = value;

                RaisePropertyChanged(NotifyChanges.ViewModel_IsEnabled_CmboState);

            }
        }

        private bool isenabled_ButtonFolder;
        [ViewModel(Send = true, ViewItemClass = ViewItemClass.Button, ViewItemId = "btnFolder", ViewItemType = ViewItemType.Enable)]
        public bool IsEnabled_ButtonFolder
        {
            get { return isenabled_ButtonFolder; }
            set
            {

                isenabled_ButtonFolder = value;

                RaisePropertyChanged(NotifyChanges.ViewModel_IsEnabled_ButtonFolder);

            }
        }

        private string datatext_ProjectFile;
        [ViewModel(Send = true, ViewItemClass = ViewItemClass.Input, ViewItemId = "inpProjectFile", ViewItemType = ViewItemType.Content)]
        public string DataText_ProjectFile
        {
            get { return datatext_ProjectFile; }
            set
            {
                if (datatext_ProjectFile == value) return;

                datatext_ProjectFile = value;

                RaisePropertyChanged(NotifyChanges.ViewModel_DataText_ProjectFile);

            }
        }

        private string datatext_PhysicalVersion;
        [ViewModel(Send = true, ViewItemClass = ViewItemClass.Input, ViewItemId = "inpPhysicalVersion", ViewItemType = ViewItemType.Content)]
        public string DataText_PhysicalVersion
        {
            get { return datatext_PhysicalVersion; }
            set
            {
                if (datatext_PhysicalVersion == value) return;

                datatext_PhysicalVersion = value;

                RaisePropertyChanged(NotifyChanges.ViewModel_DataText_PhysicalVersion);

            }
        }

        private string datatext_VersionSubPath;
        [ViewModel(Send = true, ViewItemClass = ViewItemClass.Input, ViewItemId = "inpVersionSubPath", ViewItemType = ViewItemType.Content)]
        public string DataText_VersionSubPath
        {
            get { return datatext_VersionSubPath; }
            set
            {
                if (datatext_VersionSubPath == value) return;

                datatext_VersionSubPath = value;

                RaisePropertyChanged(NotifyChanges.ViewModel_DataText_VersionSubPath);

            }
        }

        private string datatext_Folder;
        [ViewModel(Send = true, ViewItemClass = ViewItemClass.Input, ViewItemId = "inpFolder", ViewItemType = ViewItemType.Content)]
        public string DataText_Folder
        {
            get { return datatext_Folder; }
            set
            {
                if (datatext_Folder == value) return;

                datatext_Folder = value;

                RaisePropertyChanged(NotifyChanges.ViewModel_DataText_Folder);

            }
        }

        private string datatext_Version;
        [ViewModel(Send = true, ViewItemClass = ViewItemClass.Input, ViewItemId = "inpVersion", ViewItemType = ViewItemType.Content)]
        public string DataText_Version
        {
            get { return datatext_Version; }
            set
            {
                if (datatext_Version == value) return;

                datatext_Version = value;

                RaisePropertyChanged(NotifyChanges.ViewModel_DataText_Version);

            }
        }

        private string placeholder_ProjectFile;
        [ViewModel(Send = true, ViewItemClass = ViewItemClass.Input, ViewItemId = "inpProjectFile", ViewItemType = ViewItemType.PlaceHolder)]
        public string PlaceHolder_ProjectFile
        {
            get { return placeholder_ProjectFile; }
            set
            {
                if (placeholder_ProjectFile == value) return;

                placeholder_ProjectFile = value;

                RaisePropertyChanged(NotifyChanges.ViewModel_PlaceHolder_ProjectFile);

            }
        }

        private string placeholder_PhysicalVersion;
        [ViewModel(Send = true, ViewItemClass = ViewItemClass.Input, ViewItemId = "inpPhysicalVersion", ViewItemType = ViewItemType.PlaceHolder)]
        public string PlaceHolder_PhysicalVersion
        {
            get { return placeholder_PhysicalVersion; }
            set
            {
                if (placeholder_PhysicalVersion == value) return;

                placeholder_PhysicalVersion = value;

                RaisePropertyChanged(NotifyChanges.ViewModel_PlaceHolder_PhysicalVersion);

            }
        }

        private string placeholder_VersionSubPath;
        [ViewModel(Send = true, ViewItemClass = ViewItemClass.Input, ViewItemId = "inpVersionSubPath", ViewItemType = ViewItemType.PlaceHolder)]
        public string PlaceHolder_VersionSubPath
        {
            get { return placeholder_VersionSubPath; }
            set
            {
                if (placeholder_VersionSubPath == value) return;

                placeholder_VersionSubPath = value;

                RaisePropertyChanged(NotifyChanges.ViewModel_PlaceHolder_VersionSubPath);

            }
        }

        private string placeholder_Folder;
        [ViewModel(Send = true, ViewItemClass = ViewItemClass.Input, ViewItemId = "inpFolder", ViewItemType = ViewItemType.PlaceHolder)]
        public string PlaceHolder_Folder
        {
            get { return placeholder_Folder; }
            set
            {
                if (placeholder_Folder == value) return;

                placeholder_Folder = value;

                RaisePropertyChanged(NotifyChanges.ViewModel_PlaceHolder_Folder);

            }
        }

        private string placeholder_Version;
        [ViewModel(Send = true, ViewItemClass = ViewItemClass.Input, ViewItemId = "inpVersion", ViewItemType = ViewItemType.PlaceHolder)]
        public string PlaceHolder_Version
        {
            get { return placeholder_Version; }
            set
            {
                if (placeholder_Version == value) return;

                placeholder_Version = value;

                RaisePropertyChanged(NotifyChanges.ViewModel_PlaceHolder_Version);

            }
        }

        private bool isenabled_ProjectFile;
        [ViewModel(Send = true, ViewItemClass = ViewItemClass.Input, ViewItemId = "inpProjectFile", ViewItemType = ViewItemType.Enable)]
        public bool IsEnabled_ProjectFile
        {
            get { return isenabled_ProjectFile; }
            set
            {

                isenabled_ProjectFile = value;

                RaisePropertyChanged(NotifyChanges.ViewModel_IsEnabled_ProjectFile);

            }
        }

        private bool isenabled_PhysicalVersion;
        [ViewModel(Send = true, ViewItemClass = ViewItemClass.Input, ViewItemId = "inpPhysicalVersion", ViewItemType = ViewItemType.Enable)]
        public bool IsEnabled_PhysicalVersion
        {
            get { return isenabled_PhysicalVersion; }
            set
            {

                isenabled_PhysicalVersion = value;

                RaisePropertyChanged(NotifyChanges.ViewModel_IsEnabled_PhysicalVersion);

            }
        }

        private bool isenabled_VersionSubPath;
        [ViewModel(Send = true, ViewItemClass = ViewItemClass.Input, ViewItemId = "inpVersionSubPath", ViewItemType = ViewItemType.Enable)]
        public bool IsEnabled_VersionSubPath
        {
            get { return isenabled_VersionSubPath; }
            set
            {

                isenabled_VersionSubPath = value;

                RaisePropertyChanged(NotifyChanges.ViewModel_IsEnabled_VersionSubPath);

            }
        }

        private bool isenabled_Folder;
        [ViewModel(Send = true, ViewItemClass = ViewItemClass.Input, ViewItemId = "inpFolder", ViewItemType = ViewItemType.Enable)]
        public bool IsEnabled_Folder
        {
            get { return isenabled_Folder; }
            set
            {

                isenabled_Folder = value;

                RaisePropertyChanged(NotifyChanges.ViewModel_IsEnabled_Folder);

            }
        }

        private bool isenabled_Version;
        [ViewModel(Send = true, ViewItemClass = ViewItemClass.Input, ViewItemId = "inpVersion", ViewItemType = ViewItemType.Enable)]
        public bool IsEnabled_Version
        {
            get { return isenabled_Version; }
            set
            {

                isenabled_Version = value;

                RaisePropertyChanged(NotifyChanges.ViewModel_IsEnabled_Version);

            }
        }

        private bool isenabled_ButtonSoftwareProject;
        [ViewModel(Send = true, ViewItemClass = ViewItemClass.Button, ViewItemId = "btnSoftwareProject", ViewItemType = ViewItemType.Enable)]
        public bool IsEnabled_ButtonSoftwareProject
        {
            get { return isenabled_ButtonSoftwareProject; }
            set
            {

                isenabled_ButtonSoftwareProject = value;

                RaisePropertyChanged(NotifyChanges.ViewModel_IsEnabled_ButtonSoftwareProject);

            }
        }

        private string placeholder_SoftwareProject;
        [ViewModel(Send = true, ViewItemClass = ViewItemClass.Input, ViewItemId = "inpSoftwareProject", ViewItemType = ViewItemType.PlaceHolder)]
        public string PlaceHolder_SoftwareProject
        {
            get { return placeholder_SoftwareProject; }
            set
            {
                if (placeholder_SoftwareProject == value) return;

                placeholder_SoftwareProject = value;

                RaisePropertyChanged(NotifyChanges.ViewModel_PlaceHolder_SoftwareProject);

            }
        }

        private bool isenabled_SoftwareProject;
        [ViewModel(Send = true, ViewItemClass = ViewItemClass.Input, ViewItemId = "inpSoftwareProject", ViewItemType = ViewItemType.Enable)]
        public bool IsEnabled_SoftwareProject
        {
            get { return isenabled_SoftwareProject; }
            set
            {

                isenabled_SoftwareProject = value;

                RaisePropertyChanged(NotifyChanges.ViewModel_IsEnabled_SoftwareProject);

            }
        }

        private string datatext_SoftwareProject;
        [ViewModel(Send = true, ViewItemClass = ViewItemClass.Input, ViewItemId = "inpSoftwareProject", ViewItemType = ViewItemType.Content)]
        public string DataText_SoftwareProject
        {
            get { return datatext_SoftwareProject; }
            set
            {
                if (datatext_SoftwareProject == value) return;

                datatext_SoftwareProject = value;

                RaisePropertyChanged(NotifyChanges.ViewModel_DataText_SoftwareProject);

            }
        }

        private string label_SoftwareProject;
        [ViewModel(Send = true, ViewItemClass = ViewItemClass.Label, ViewItemId = "lblSoftwareProject", ViewItemType = ViewItemType.Content)]
        public string Label_SoftwareProject
        {
            get { return label_SoftwareProject; }
            set
            {
                if (label_SoftwareProject == value) return;

                label_SoftwareProject = value;

                RaisePropertyChanged(NotifyChanges.ViewModel_Label_SoftwareProject);

            }
        }

        private int index_AdditionalLanguageRemove;
        [ViewModel(Send = true, ViewItemClass = ViewItemClass.ListBox, ViewItemId = "lstBoxAdditionalLanguages", ViewItemType = ViewItemType.RemoveIndex)]
        public int Index_AdditionalLanguageRemove
        {
            get { return index_AdditionalLanguageRemove; }
            set
            {
                if (index_AdditionalLanguageRemove == value) return;

                index_AdditionalLanguageRemove = value;

                RaisePropertyChanged(NotifyChanges.ViewModel_Index_AdditionalLanguageRemove);

            }
        }

    }
}
