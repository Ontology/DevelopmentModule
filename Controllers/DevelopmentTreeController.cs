﻿using DevelopmentModule.Factories;
using DevelopmentModule.Services;
using DevelopmentModule.Translations;
using OntologyAppDBConnector;
using OntologyClasses.BaseClasses;
using OntoMsg_Module;
using OntoMsg_Module.Attributes;
using OntoMsg_Module.Base;
using OntoMsg_Module.Models;
using OntoMsg_Module.Notifications;
using OntoMsg_Module.StateMachines;
using OntoMsg_Module.WebSocketServices;
using OntoWebCore.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;
using System.Runtime.InteropServices;
using System.Text;
using System.Threading.Tasks;
using System.ComponentModel;

namespace DevelopmentModule.Controllers
{
    public class DevelopmentTreeController : DevelopmentTreeViewModel, IViewController
    {
        private WebsocketServiceAgent webSocketServiceAgent;

        private ElasticServiceAgent_DevTree serviceAgentElastic;
        private ViewTreeFactory viewTreeFactory;

        private TranslationController translationController = new TranslationController();

        private clsLocalConfig localConfig;

        public IControllerStateMachine StateMachine { get; private set; }
        public ControllerStateMachine LocalStateMachine
        {
            get
            {
                if (StateMachine == null)
                {
                    return null;
                }

                return (ControllerStateMachine)StateMachine;
            }
        }

        public DevelopmentTreeController()
        {
            localConfig = (clsLocalConfig)LocalConfigManager.GetLocalConfig(((GuidAttribute)Assembly.GetExecutingAssembly().GetCustomAttributes(true).FirstOrDefault(objAttribute => objAttribute is GuidAttribute)).Value);
            if (localConfig == null)
            {
                localConfig = new clsLocalConfig(new Globals());
                LocalConfigManager.AddLocalConfig(localConfig);
            }

            Initialize();
            PropertyChanged += DevelopmentTreeController_PropertyChanged;


        }

        private void DevelopmentTreeController_PropertyChanged(object sender, System.ComponentModel.PropertyChangedEventArgs e)
        {
            var property = ViewModelProperties.FirstOrDefault(viewProperty => viewProperty.Property.Name == e.PropertyName);

            if (property == null) return;

            property.ViewItem.AddValue(property.Property.GetValue(this));

            webSocketServiceAgent.SendPropertyChange(e.PropertyName);

            if (e.PropertyName == Notifications.NotifyChanges.Navigation_Text_SelectedNodeId)
            {
                SendTreeLevel(Text_SelectedNodeId);

                ViewTreeNodes_PathNodes = viewTreeFactory.CreatePathNodes(serviceAgentElastic.Projects, serviceAgentElastic.ProjectsToDevelopments, serviceAgentElastic.DevelopmentTree, Text_SelectedNodeId);

            }
            else if (e.PropertyName == Notifications.NotifyChanges.Navigation_Text_Search)
            {
                localConfig.SearchNode.Name = "Search: " + Text_Search;
                SendTreeLevel(Text_Search, true);
                ViewTreeNodes_PathNodes = new List<ViewTreeNode> { localConfig.SearchNode };
                Text_SelectedNodeId = "";
            }
        }

        private void Initialize()
        {
            var stateMachine = new ControllerStateMachine(StateMachineType.BlockSelectingSelected | StateMachineType.BlockLoadingSelected);
            stateMachine.IsControllerListen = true;
            StateMachine = stateMachine;
            stateMachine.PropertyChanged += StateMachine_PropertyChanged;
            stateMachine.loadSelectedItem += StateMachine_loadSelectedItem;
            stateMachine.loginSucceded += StateMachine_loginSucceded;
            stateMachine.openedSocket += StateMachine_openedSocket;
            stateMachine.closedSocket += StateMachine_closedSocket;

            viewTreeFactory = new ViewTreeFactory(localConfig);
            serviceAgentElastic = new ElasticServiceAgent_DevTree(localConfig);
            serviceAgentElastic.PropertyChanged += ServiceAgentElastic_PropertyChanged;
            localConfig.SearchNode = viewTreeFactory.CrateSearchNode();
        }

        private void StateMachine_closedSocket()
        {
            webSocketServiceAgent.RemoveAllResources();
            serviceAgentElastic = null;
            translationController = null;
            viewTreeFactory = null;
        }

        private void StateMachine_openedSocket()
        {
            webSocketServiceAgent.RegisterEndpoint(new ChannelEndpoint
            {
                EndPointId = webSocketServiceAgent.EndpointId,
                EndpointType = EndpointType.Sender,
                SessionId = webSocketServiceAgent.DataText_SessionId
            });
        }

        private void StateMachine_loginSucceded()
        {
            webSocketServiceAgent.RegisterEndpoint(new ChannelEndpoint
            {
                ChannelTypeId = Channels.ParameterList,
                EndPointId = webSocketServiceAgent.EndpointId,
                EndpointType = EndpointType.Receiver,
                SessionId = webSocketServiceAgent.DataText_SessionId
            });

            var result = serviceAgentElastic.GetSoftwareProjects();

            
        }

        private void StateMachine_loadSelectedItem(clsOntologyItem oItemSelected)
        {
            
        }

        private void StateMachine_PropertyChanged(object sender, PropertyChangedEventArgs e)
        {
            if (e.PropertyName == nameof(OntoMsg_Module.StateMachines.ControllerStateMachine.LoginSuccessful))
            {
                IsSuccessful_Login = LocalStateMachine.LoginSuccessful;
            }

        }

        public void InitializeViewController(WebsocketServiceAgent webSocketServiceAgent)
        {
            this.webSocketServiceAgent = webSocketServiceAgent;
            this.webSocketServiceAgent.PropertyChanged += WebSocketServiceAgent_PropertyChanged;
            this.webSocketServiceAgent.comServerOnMessage += WebSocketServiceAgent_comServerOnMessage;
            this.webSocketServiceAgent.comServerOpened += WebSocketServiceAgent_comServerOpened;

        }

        private void WebSocketServiceAgent_PropertyChanged(object sender, System.ComponentModel.PropertyChangedEventArgs e)
        {
            if (e.PropertyName == OntoMsg_Module.Notifications.NotifyChanges.Websocket_DataText_SessionId)
            {


            }
            else if (e.PropertyName == OntoMsg_Module.Notifications.NotifyChanges.Websocket_Command_RequestedCommand)
            {


            }
            else if (e.PropertyName == OntoMsg_Module.Notifications.NotifyChanges.Websocket_Event_RequestEvent)
            {

            }
            else if (e.PropertyName == OntoMsg_Module.Notifications.NotifyChanges.Websocket_ChangedProperty)
            {
                if (webSocketServiceAgent.ChangedProperty.Key == Notifications.NotifyChanges.Navigation_Text_SelectedNodeId)
                {
                    if (Text_SelectedNodeId == webSocketServiceAgent.ChangedProperty.Value.ToString()) return;
                    if (!localConfig.Globals.is_GUID(webSocketServiceAgent.ChangedProperty.Value.ToString())) return;
                    Text_SelectedNodeId = webSocketServiceAgent.ChangedProperty.Value.ToString();
                    

                }
                else if (webSocketServiceAgent.ChangedProperty.Key == Notifications.NotifyChanges.Navigation_Text_Search)
                {
                    Text_Search = webSocketServiceAgent.ChangedProperty.Value.ToString();

                }
            }


        }

        private void WebSocketServiceAgent_comServerOpened()
        {
            var authenticationRequest = new InterServiceMessage
            {
                ChannelId = Channels.Login,
                SenderId = webSocketServiceAgent.EndpointId
            };

            webSocketServiceAgent.SendInterModMessage(authenticationRequest);

        }

        private void SendTreeLevel(string parent, bool doSearch = false)
        {

            if (doSearch)
            {
                var treeFileName = Guid.NewGuid().ToString() + ".json";
                var sessionFile = webSocketServiceAgent.RequestWriteStream(treeFileName);
                if (sessionFile.FileUri == null)
                {
                    return;
                }

                if (sessionFile.StreamWriter == null)
                {

                    return;
                }

                var result = viewTreeFactory.WriteJson(serviceAgentElastic.Projects, serviceAgentElastic.DevelopmentTree, serviceAgentElastic.ProjectsToDevelopments, sessionFile, null, parent);


                Url_TreeData = sessionFile.FileUri.AbsoluteUri;
            }
            else
            {
                var treeFileName = Guid.NewGuid().ToString() + ".json";
                var sessionFile = webSocketServiceAgent.RequestWriteStream(treeFileName);
                if (sessionFile.FileUri == null)
                {
                    return;
                }

                if (sessionFile.StreamWriter == null)
                {

                    return;
                }

                var treeNode = viewTreeFactory.GetSelectedNode(parent);

                if (treeNode != null)
                {
                    var interComMessage = new InterServiceMessage
                    {
                        ChannelId = Channels.ParameterList,
                        OItems = new List<clsOntologyItem> { treeNode }
                    };

                    webSocketServiceAgent.SendInterModMessage(interComMessage);
                }
                var result = viewTreeFactory.WriteJson(serviceAgentElastic.Projects, serviceAgentElastic.DevelopmentTree, serviceAgentElastic.ProjectsToDevelopments, sessionFile, parent);


                Url_TreeData = sessionFile.FileUri.AbsoluteUri;
            }
            
        }

        private void WebSocketServiceAgent_comServerOnMessage(OntoMsg_Module.Notifications.InterServiceMessage message)
        {
            if (message.ReceiverId != null && message.ReceiverId != webSocketServiceAgent.EndpointId) return;
            if (message.ChannelId == Channels.ParameterList)
            {
                if (message.OItems == null || !message.OItems.Any()) return;
                var oitem = message.OItems.Last();
                if (oitem.GUID_Parent != localConfig.OItem_type_softwaredevelopment.GUID && oitem.GUID_Parent != localConfig.OItem_class_software_project.GUID) return;
                if (Text_SelectedNodeId == oitem.GUID) return;

                Text_SelectedNodeId = oitem.GUID;

            }
        }

        private void ServiceAgentElastic_PropertyChanged(object sender, System.ComponentModel.PropertyChangedEventArgs e)
        {
            if (e.PropertyName == Notifications.NotifyChanges.Elastic_ResultData_Projects)
            {


                if (serviceAgentElastic.ResultData_Projects.GUID == localConfig.Globals.LState_Error.GUID)
                {
                    return;


                }

                viewTreeFactory = new ViewTreeFactory(localConfig);

                

                var treeFileName = Guid.NewGuid().ToString() + ".json";

                var sessionFile = webSocketServiceAgent.RequestWriteStream(treeFileName);
                if (sessionFile.FileUri == null)
                {
                    return;
                }

                if (sessionFile.StreamWriter == null)
                {

                    return;
                }

                var result = viewTreeFactory.WriteJson(serviceAgentElastic.Projects, serviceAgentElastic.DevelopmentTree, serviceAgentElastic.ProjectsToDevelopments, sessionFile);

                if (result.GUID == localConfig.Globals.LState_Error.GUID)
                {

                    return;
                }


                Url_TreeData = sessionFile.FileUri.AbsoluteUri;
                ViewTreeNodes_PathNodes = viewTreeFactory.CreatePathNodes(serviceAgentElastic.Projects, serviceAgentElastic.DevelopmentTree, serviceAgentElastic.ProjectsToDevelopments, null);
            }

            
        }
        public List<ViewModelProperty> GetViewModelProperties(bool onlySend = true, ViewItemType viewItemType = ViewItemType.All, ViewItemClass viewItemClass = ViewItemClass.All)
        {
            return ViewModelProperties.Where(viewItemProp => (onlySend ? viewItemProp.ViewModelAttribute.Send : 1 == 1)
                && (viewItemType != ViewItemType.All ? viewItemProp.ViewModelAttribute.ViewItemType == viewItemType : 1 == 1)
                && (viewItemClass != ViewItemClass.All ? viewItemProp.ViewModelAttribute.ViewItemClass == viewItemClass : 1 == 1)).ToList();
        }
    }
}
