﻿using OntologyAppDBConnector;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;
using System.Text;
using System.Threading.Tasks;

namespace DevelopmentModule.Translations
{
    public class TranslationController
    {

        
        public string Label_Url
        {
            get
            {
                return GetValue("244d7e5cd2e64401bd99a91ba98fda34", "Datei-Fehler!");
            }

        }

        public string Text_FileStreamError
        {
            get
            {
                return GetValue("5682b2ddf0be4d1babeac59245d25a85", "Datei für Datenaustausch konnte  nicht agespeichert werden!");
            }

        }

        public string Title_WebConfigError
        {
            get
            {
                return GetValue("78634024801646ceba093bb8c62f7f0a", "Webserver-Fehler");
            }

        }

        public string Text_WebConfigError
        {
            get
            {
                return GetValue("7d1199b177ba435b8c2bcb917cec913b", "Der Webserver hat einen Fehler verursacht!");
            }

        }

        public string Label_Development
        {
            get
            {
                return GetValue("Label_Development", "Development: {0}");
            }
        }

        public string Label_State
        {
            get
            {
                return GetValue("Label_State", "State:");
            }
        }

        public string Label_Version
        {
            get
            {
                return GetValue("Label_Version", "Version:");
            }
        }

        public string Label_Creator
        {
            get
            {
                return GetValue("Label_Creator", "Author:");
            }
        }

        public string Label_PLanguage
        {
            get
            {
                return GetValue("Label_PLanguage", "P.-Sprache:");
            }
        }

        public string Label_Folder
        {
            get
            {
                return GetValue("Label_Folder", "Pfad:");
            }
        }

        public string Label_VersionSubPath
        {
            get
            {
                return GetValue("Label_VersionSubPath", "Version-Pfad:");
            }
        }

        public string Label_PhysicalVersion
        {
            get
            {
                return GetValue("Label_PhysicalVersion", "Dateiversion:");
            }
        }

        public string Label_ProjectFile
        {
            get
            {
                return GetValue("Label_ProjectFile", "Projektdatei:");
            }
        }

        public string Label_StdLanguage
        {
            get
            {
                return GetValue("Label_StdLanguage", "Str.-Sprache:");
            }
        }

        public string Label_AdditionalLanguages
        {
            get
            {
                return GetValue("Label_AdditionalLanguages", "Zusatzsprachen:");
            }
        }

        public string Label_SoftwareProject
        {
            get
            {
                return GetValue("Label_SoftwareProject", "Projekt:");
            }
        }

        public string PlaceHoder_Version
        {
            get
            {
                return GetValue("PlaceHoder_Version", "Wählen Sie eine Version");
            }
        }

        public string PlaceHoder_Folder
        {
            get
            {
                return GetValue("PlaceHoder_Folder", "Wählen Sie einen Projekt-Ordner");
            }
        }

        public string PlaceHoder_VersionSubPath
        {
            get
            {
                return GetValue("PlaceHoder_Folder", "Wählen Sie den relativen Pfad der Version-Assmbly");
            }
        }

        public string PlaceHoder_PhysicalVersion
        {
            get
            {
                return GetValue("PlaceHoder_PhysicalVersion", "Aktualisieren Sie die physikalische Version");
            }
        }

        public string PlaceHoder_ProjectFile
        {
            get
            {
                return GetValue("PlaceHoder_ProjectFile", "Wählen Sie die Projekt-Datei");
            }
        }

        public string PlaceHoder_SoftwareProject
        {
            get
            {
                return GetValue("PlaceHoder_SoftwareProject", "Wählen Sie die Software Projekt");
            }
        }

        public string GetTranslatedByPropertyName(string propertyName)
        {
            var property = this.GetType().GetProperties().Cast<PropertyInfo>().FirstOrDefault(propItem => propItem.Name == propertyName);

            if (property != null)
            {
                var value = property.GetValue(this);
                return value != null ? value.ToString() : "xxx_Error_xxx";
            }
            else
            {
                return "xxx_Error_xxx";
            }
        }


        private string GetValue(string IdReference, string defaultValue)
        {
            return defaultValue;
        }

    }
}
