﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using OntologyAppDBConnector;
using ImportExport_Module;
using OntologyClasses.BaseClasses;
using System.Reflection;
using System.Runtime.InteropServices;
using OntologyClasses.Interfaces;
using OntoWebCore.Models;

namespace DevelopmentModule
{
    public class clsLocalConfig : ILocalConfig
{
    private const string cstrID_Ontology = "d453931eab534a3eb0dcb3498e35c841";
    private ImportWorker objImport;

    public Globals Globals { get; set; }

    private clsOntologyItem objOItem_DevConfig = new clsOntologyItem();
    public clsOntologyItem OItem_BaseConfig { get; set; }

    private OntologyModDBConnector objDBLevel_Config1;
    private OntologyModDBConnector objDBLevel_Config2;

    public clsOntologyItem OItem_attribute_build { get; set; }
    public clsOntologyItem OItem_attribute_caption { get; set; }
    public clsOntologyItem OItem_attribute_datetimestamp { get; set; }
    public clsOntologyItem OItem_attribute_dbpostfix { get; set; }
    public clsOntologyItem OItem_attribute_grant { get; set; }
    public clsOntologyItem OItem_attribute_imageid { get; set; }
    public clsOntologyItem OItem_attribute_major { get; set; }
    public clsOntologyItem OItem_attribute_message { get; set; }
    public clsOntologyItem OItem_attribute_minor { get; set; }
    public clsOntologyItem OItem_attribute_revision { get; set; }
    public clsOntologyItem OItem_attribute_xml_text { get; set; }
    public clsOntologyItem OItem_attributetype_regex { get; set; }
    public clsOntologyItem OItem_attributetype_remove_from_source { get; set; }
    public clsOntologyItem OItem_attributetype_standard { get; set; }
    public clsOntologyItem OItem_attributetype_useorderid { get; set; }
    public clsOntologyItem OItem_class_code_templates { get; set; }
    public clsOntologyItem OItem_class_path { get; set; }
    public clsOntologyItem OItem_class_software_project { get; set; }
    public clsOntologyItem OItem_class_syntax_highlighting__scintillanet_ { get; set; }
    public clsOntologyItem OItem_class_xml { get; set; }
    public clsOntologyItem OItem_development_management { get; set; }
    public clsOntologyItem OItem_object____ { get; set; }
    public clsOntologyItem OItem_object__assembly__assemblyversion___ { get; set; }
    public clsOntologyItem OItem_object__point { get; set; }
    public clsOntologyItem OItem_object_assembly_version { get; set; }
    public clsOntologyItem OItem_object_c_ { get; set; }
    public clsOntologyItem OItem_object_string { get; set; }
    public clsOntologyItem OItem_object_vb_net { get; set; }
    public clsOntologyItem OItem_object_version { get; set; }
    public clsOntologyItem OItem_object_visual_studio_version_parser { get; set; }
    public clsOntologyItem OItem_relationtype_access_by { get; set; }
    public clsOntologyItem OItem_relationtype_additional { get; set; }
    public clsOntologyItem OItem_relationtype_alternative_for { get; set; }
    public clsOntologyItem OItem_relationtype_attribute { get; set; }
    public clsOntologyItem OItem_relationtype_belonging_paramter { get; set; }
    public clsOntologyItem OItem_relationtype_belonging_resource { get; set; }
    public clsOntologyItem OItem_relationtype_belongsto { get; set; }
    public clsOntologyItem OItem_relationtype_class { get; set; }
    public clsOntologyItem OItem_relationtype_contains { get; set; }
    public clsOntologyItem OItem_relationtype_declaration { get; set; }
    public clsOntologyItem OItem_relationtype_describes { get; set; }
    public clsOntologyItem OItem_relationtype_directed_by { get; set; }
    public clsOntologyItem OItem_relationtype_document_container { get; set; }
    public clsOntologyItem OItem_relationtype_entry { get; set; }
    public clsOntologyItem OItem_relationtype_error_message { get; set; }
    public clsOntologyItem OItem_relationtype_export_to { get; set; }
    public clsOntologyItem OItem_relationtype_handles { get; set; }
    public clsOntologyItem OItem_relationtype_happened { get; set; }
    public clsOntologyItem OItem_relationtype_ignore { get; set; }
    public clsOntologyItem OItem_relationtype_initializing { get; set; }
    public clsOntologyItem OItem_relationtype_input_message { get; set; }
    public clsOntologyItem OItem_relationtype_invoked_actor { get; set; }
    public clsOntologyItem OItem_relationtype_invoking_actor { get; set; }
    public clsOntologyItem OItem_relationtype_is_defined_by { get; set; }
    public clsOntologyItem OItem_relationtype_is_instance_of { get; set; }
    public clsOntologyItem OItem_relationtype_is_of_type { get; set; }
    public clsOntologyItem OItem_relationtype_isdescribedby { get; set; }
    public clsOntologyItem OItem_relationtype_isinstate { get; set; }
    public clsOntologyItem OItem_relationtype_issubordinated { get; set; }
    public clsOntologyItem OItem_relationtype_iswrittenin { get; set; }
    public clsOntologyItem OItem_relationtype_located_in { get; set; }
    public clsOntologyItem OItem_relationtype_main { get; set; }
    public clsOntologyItem OItem_relationtype_needs { get; set; }
    public clsOntologyItem OItem_relationtype_object { get; set; }
    public clsOntologyItem OItem_relationtype_offered_by { get; set; }
    public clsOntologyItem OItem_relationtype_offers { get; set; }
    public clsOntologyItem OItem_relationtype_possible { get; set; }
    public clsOntologyItem OItem_relationtype_posts { get; set; }
    public clsOntologyItem OItem_relationtype_pre { get; set; }
    public clsOntologyItem OItem_relationtype_project_file { get; set; }
    public clsOntologyItem OItem_relationtype_property { get; set; }
    public clsOntologyItem OItem_relationtype_provides { get; set; }
    public clsOntologyItem OItem_relationtype_relationtype { get; set; }
    public clsOntologyItem OItem_relationtype_request { get; set; }
    public clsOntologyItem OItem_relationtype_sourceslocatedin { get; set; }
    public clsOntologyItem OItem_relationtype_standard { get; set; }
    public clsOntologyItem OItem_relationtype_subpath_versionfile { get; set; }
    public clsOntologyItem OItem_relationtype_superordinate { get; set; }
    public clsOntologyItem OItem_relationtype_user_message { get; set; }
    public clsOntologyItem OItem_relationtype_value_type { get; set; }
    public clsOntologyItem OItem_relationtype_wascreatedby { get; set; }
    public clsOntologyItem OItem_relationtype_wasdevelopedby { get; set; }
    public clsOntologyItem OItem_relationtype_works_off { get; set; }
    public clsOntologyItem OItem_token_clslocalconfig_xml_xml { get; set; }
    public clsOntologyItem OItem_token_db_schema_type_config { get; set; }
    public clsOntologyItem OItem_token_db_schema_type_module { get; set; }
    public clsOntologyItem OItem_token_db_schema_type_user { get; set; }
    public clsOntologyItem OItem_token_declaration_configitems_xml { get; set; }
    public clsOntologyItem OItem_token_dev_structure_database { get; set; }
    public clsOntologyItem OItem_token_dev_structure_return { get; set; }
    public clsOntologyItem OItem_token_dev_structure_schleife { get; set; }
    public clsOntologyItem OItem_token_directions_in { get; set; }
    public clsOntologyItem OItem_token_directions_out { get; set; }
    public clsOntologyItem OItem_token_export_mode_deny_item_with_children__type___token_ { get; set; }
    public clsOntologyItem OItem_token_export_mode_deny_only_children__token_of_type_ { get; set; }
    public clsOntologyItem OItem_token_export_mode_grant_children_of_item__token_of_type_ { get; set; }
    public clsOntologyItem OItem_token_export_mode_normal { get; set; }
    public clsOntologyItem OItem_token_initialize_relationtype__configitem__xml { get; set; }
    public clsOntologyItem OItem_token_initialize_token__configitem__xml { get; set; }
    public clsOntologyItem OItem_token_initilize_attribute__configitem__xml { get; set; }
    public clsOntologyItem OItem_token_initilize_type__configitem__xml { get; set; }
    public clsOntologyItem OItem_token_logstate_active { get; set; }
    public clsOntologyItem OItem_token_logstate_changed { get; set; }
    public clsOntologyItem OItem_token_logstate_configitemadded { get; set; }
    public clsOntologyItem OItem_token_logstate_create { get; set; }
    public clsOntologyItem OItem_token_logstate_inactive { get; set; }
    public clsOntologyItem OItem_token_logstate_information { get; set; }
    public clsOntologyItem OItem_token_logstate_obsolete { get; set; }
    public clsOntologyItem OItem_token_logstate_open { get; set; }
    public clsOntologyItem OItem_token_logstate_request { get; set; }
    public clsOntologyItem OItem_token_logstate_versionchanged { get; set; }
    public clsOntologyItem OItem_token_module_development_management { get; set; }
    public clsOntologyItem OItem_token_possiblestates_developmentstandard { get; set; }
    public clsOntologyItem OItem_token_property_configitem_xml { get; set; }
    public clsOntologyItem OItem_token_variable_list_declaration_configitems { get; set; }
    public clsOntologyItem OItem_token_variable_list_initialize_configitems_attributes { get; set; }
    public clsOntologyItem OItem_token_variable_list_initialize_configitems_relationtypes { get; set; }
    public clsOntologyItem OItem_token_variable_list_initialize_configitems_token { get; set; }
    public clsOntologyItem OItem_token_variable_list_initialize_configitems_types { get; set; }
    public clsOntologyItem OItem_token_variable_list_properties { get; set; }
    public clsOntologyItem OItem_token_variable_name_configitem { get; set; }
    public clsOntologyItem OItem_token_xml_clslocalconfig_ontology_xml { get; set; }
    public clsOntologyItem OItem_token_xml_declaration_ontology_configitems { get; set; }
    public clsOntologyItem OItem_token_xml_initialize_relationtype__configitem_ontology_ { get; set; }
    public clsOntologyItem OItem_token_xml_initialize_token__configitem_ontology_ { get; set; }
    public clsOntologyItem OItem_token_xml_initilize_attribute__configitem_ontology_ { get; set; }
    public clsOntologyItem OItem_token_xml_initilize_type__configitem_ontology_ { get; set; }
    public clsOntologyItem OItem_token_xml_property_ontology_configitem { get; set; }
    public clsOntologyItem OItem_type_database { get; set; }
    public clsOntologyItem OItem_type_database_on_server { get; set; }
    public clsOntologyItem OItem_type_database_schema { get; set; }
    public clsOntologyItem OItem_type_db_schema_type { get; set; }
    public clsOntologyItem OItem_type_dbschema_of_application { get; set; }
    public clsOntologyItem OItem_type_dev_structure { get; set; }
    public clsOntologyItem OItem_type_dev_structure__process_type_to_process_ { get; set; }
    public clsOntologyItem OItem_type_dev_structure_invoke { get; set; }
    public clsOntologyItem OItem_type_development_libraries { get; set; }
    public clsOntologyItem OItem_type_development_module { get; set; }
    public clsOntologyItem OItem_type_development_structure { get; set; }
    public clsOntologyItem OItem_type_developmentconfig { get; set; }
    public clsOntologyItem OItem_type_developmentconfigitem { get; set; }
    public clsOntologyItem OItem_type_developmentversion { get; set; }
    public clsOntologyItem OItem_type_directions { get; set; }
    public clsOntologyItem OItem_type_export_mode { get; set; }
    public clsOntologyItem OItem_type_file { get; set; }
    public clsOntologyItem OItem_type_folder { get; set; }
    public clsOntologyItem OItem_type_gui_caption { get; set; }
    public clsOntologyItem OItem_type_gui_entires { get; set; }
    public clsOntologyItem OItem_type_image_video_management { get; set; }
    public clsOntologyItem OItem_type_language { get; set; }
    public clsOntologyItem OItem_type_localized_message { get; set; }
    public clsOntologyItem OItem_type_localized_names { get; set; }
    public clsOntologyItem OItem_type_localizeddescription { get; set; }
    public clsOntologyItem OItem_type_logentry { get; set; }
    public clsOntologyItem OItem_type_logstate { get; set; }
    public clsOntologyItem OItem_type_messages { get; set; }
    public clsOntologyItem OItem_type_module { get; set; }
    public clsOntologyItem OItem_type_objects { get; set; }
    public clsOntologyItem OItem_type_ontology_export { get; set; }
    public clsOntologyItem OItem_type_parameter_dev_structure { get; set; }
    public clsOntologyItem OItem_type_process { get; set; }
    public clsOntologyItem OItem_type_programinglanguage { get; set; }
    public clsOntologyItem OItem_type_scene { get; set; }
    public clsOntologyItem OItem_type_sem_items_to_expot_with_children { get; set; }
    public clsOntologyItem OItem_type_server { get; set; }
    public clsOntologyItem OItem_type_softwaredevelopment { get; set; }
    public clsOntologyItem OItem_type_structure_type { get; set; }
    public clsOntologyItem OItem_type_structure_type_with_aspects { get; set; }
    public clsOntologyItem OItem_type_structure_validity { get; set; }
    public clsOntologyItem OItem_type_tooltip_messages { get; set; }
    public clsOntologyItem OItem_type_user { get; set; }
    public clsOntologyItem OItem_type_wiki_implementation { get; set; }

        public ViewTreeNode SearchNode { get; set; }

        private void get_Data_DevelopmentConfig()
    {
        var objORL_Ontology_To_OntolgyItems = new List<clsObjectRel> {new clsObjectRel {ID_Object = cstrID_Ontology,
                                                                                             ID_RelationType = Globals.RelationType_contains.GUID,
                                                                                             ID_Parent_Other = Globals.Class_OntologyItems.GUID}};

        var objOItem_Result = objDBLevel_Config1.GetDataObjectRel(objORL_Ontology_To_OntolgyItems, doIds: false);
        if (objOItem_Result.GUID == Globals.LState_Success.GUID)
        {
            if (objDBLevel_Config1.ObjectRels.Any())
            {

                objORL_Ontology_To_OntolgyItems = objDBLevel_Config1.ObjectRels.Select(oi => new clsObjectRel
                {
                    ID_Object = oi.ID_Other,
                    ID_RelationType = Globals.RelationType_belongingAttribute.GUID
                }).ToList();

                objORL_Ontology_To_OntolgyItems.AddRange(objDBLevel_Config1.ObjectRels.Select(oi => new clsObjectRel
                {
                    ID_Object = oi.ID_Other,
                    ID_RelationType = Globals.RelationType_belongingClass.GUID
                }));
                objORL_Ontology_To_OntolgyItems.AddRange(objDBLevel_Config1.ObjectRels.Select(oi => new clsObjectRel
                {
                    ID_Object = oi.ID_Other,
                    ID_RelationType = Globals.RelationType_belongingObject.GUID
                }));
                objORL_Ontology_To_OntolgyItems.AddRange(objDBLevel_Config1.ObjectRels.Select(oi => new clsObjectRel
                {
                    ID_Object = oi.ID_Other,
                    ID_RelationType = Globals.RelationType_belongingRelationType.GUID
                }));

                objOItem_Result = objDBLevel_Config2.GetDataObjectRel(objORL_Ontology_To_OntolgyItems, doIds: false);
                if (objOItem_Result.GUID == Globals.LState_Success.GUID)
                {
                    if (!objDBLevel_Config2.ObjectRels.Any())
                    {
                        throw new Exception("Config-Error");
                    }
                }
                else
                {
                    throw new Exception("Config-Error");
                }
            }
            else
            {
                throw new Exception("Config-Error");
            }

        }

    }

    public clsLocalConfig()
    {
        Globals = new Globals();
        set_DBConnection();
        get_Config();
    }

    public clsLocalConfig(Globals Globals)
    {
        this.Globals = Globals;
        set_DBConnection();
        get_Config();
    }

    private void set_DBConnection()
    {
        objDBLevel_Config1 = new OntologyModDBConnector(Globals);
        objDBLevel_Config2 = new OntologyModDBConnector(Globals);
        objImport = new ImportWorker(Globals);
    }

    private void get_Config()
    {
        try
        {
            get_Data_DevelopmentConfig();
            get_Config_AttributeTypes();
            get_Config_RelationTypes();
            get_Config_Classes();
            get_Config_Objects();
        }
        catch (Exception ex)
        {
            var objAssembly = Assembly.GetExecutingAssembly();
            AssemblyTitleAttribute[] objCustomAttributes = (AssemblyTitleAttribute[])objAssembly.GetCustomAttributes(typeof(AssemblyTitleAttribute), false);
            var strTitle = "Unbekannt";
            if (objCustomAttributes.Length == 1)
            {
                strTitle = objCustomAttributes.First().Title;
            }
            
            var objOItem_Result = objImport.ImportTemplates(objAssembly);
            if (objOItem_Result.GUID != Globals.LState_Error.GUID)
            {
                get_Data_DevelopmentConfig();
                get_Config_AttributeTypes();
                get_Config_RelationTypes();
                get_Config_Classes();
                get_Config_Objects();
            }
            else
            {
                throw new Exception("Config not importable");
            }
            
        }
    }

    private void get_Config_AttributeTypes()
    {
        var objOList_attribute_build = (from objOItem in objDBLevel_Config1.ObjectRels
                                        where objOItem.ID_Object == cstrID_Ontology
                                        join objRef in objDBLevel_Config2.ObjectRels on objOItem.ID_Other equals objRef.ID_Object
                                        where objRef.Name_Object.ToLower() == "attribute_build".ToLower() && objRef.Ontology == Globals.Type_AttributeType
                                        select objRef).ToList();

        if (objOList_attribute_build.Any())
        {
            OItem_attribute_build = new clsOntologyItem()
            {
                GUID = objOList_attribute_build.First().ID_Other,
                Name = objOList_attribute_build.First().Name_Other,
                GUID_Parent = objOList_attribute_build.First().ID_Parent_Other,
                Type = Globals.Type_AttributeType
            };
        }
        else
        {
            throw new Exception("config err");
        }

        var objOList_attribute_caption = (from objOItem in objDBLevel_Config1.ObjectRels
                                          where objOItem.ID_Object == cstrID_Ontology
                                          join objRef in objDBLevel_Config2.ObjectRels on objOItem.ID_Other equals objRef.ID_Object
                                          where objRef.Name_Object.ToLower() == "attribute_caption".ToLower() && objRef.Ontology == Globals.Type_AttributeType
                                          select objRef).ToList();

        if (objOList_attribute_caption.Any())
        {
            OItem_attribute_caption = new clsOntologyItem()
            {
                GUID = objOList_attribute_caption.First().ID_Other,
                Name = objOList_attribute_caption.First().Name_Other,
                GUID_Parent = objOList_attribute_caption.First().ID_Parent_Other,
                Type = Globals.Type_AttributeType
            };
        }
        else
        {
            throw new Exception("config err");
        }

        var objOList_attribute_datetimestamp = (from objOItem in objDBLevel_Config1.ObjectRels
                                                where objOItem.ID_Object == cstrID_Ontology
                                                join objRef in objDBLevel_Config2.ObjectRels on objOItem.ID_Other equals objRef.ID_Object
                                                where objRef.Name_Object.ToLower() == "attribute_datetimestamp".ToLower() && objRef.Ontology == Globals.Type_AttributeType
                                                select objRef).ToList();

        if (objOList_attribute_datetimestamp.Any())
        {
            OItem_attribute_datetimestamp = new clsOntologyItem()
            {
                GUID = objOList_attribute_datetimestamp.First().ID_Other,
                Name = objOList_attribute_datetimestamp.First().Name_Other,
                GUID_Parent = objOList_attribute_datetimestamp.First().ID_Parent_Other,
                Type = Globals.Type_AttributeType
            };
        }
        else
        {
            throw new Exception("config err");
        }

        var objOList_attribute_dbpostfix = (from objOItem in objDBLevel_Config1.ObjectRels
                                            where objOItem.ID_Object == cstrID_Ontology
                                            join objRef in objDBLevel_Config2.ObjectRels on objOItem.ID_Other equals objRef.ID_Object
                                            where objRef.Name_Object.ToLower() == "attribute_dbpostfix".ToLower() && objRef.Ontology == Globals.Type_AttributeType
                                            select objRef).ToList();

        if (objOList_attribute_dbpostfix.Any())
        {
            OItem_attribute_dbpostfix = new clsOntologyItem()
            {
                GUID = objOList_attribute_dbpostfix.First().ID_Other,
                Name = objOList_attribute_dbpostfix.First().Name_Other,
                GUID_Parent = objOList_attribute_dbpostfix.First().ID_Parent_Other,
                Type = Globals.Type_AttributeType
            };
        }
        else
        {
            throw new Exception("config err");
        }

        var objOList_attribute_grant = (from objOItem in objDBLevel_Config1.ObjectRels
                                        where objOItem.ID_Object == cstrID_Ontology
                                        join objRef in objDBLevel_Config2.ObjectRels on objOItem.ID_Other equals objRef.ID_Object
                                        where objRef.Name_Object.ToLower() == "attribute_grant".ToLower() && objRef.Ontology == Globals.Type_AttributeType
                                        select objRef).ToList();

        if (objOList_attribute_grant.Any())
        {
            OItem_attribute_grant = new clsOntologyItem()
            {
                GUID = objOList_attribute_grant.First().ID_Other,
                Name = objOList_attribute_grant.First().Name_Other,
                GUID_Parent = objOList_attribute_grant.First().ID_Parent_Other,
                Type = Globals.Type_AttributeType
            };
        }
        else
        {
            throw new Exception("config err");
        }

        var objOList_attribute_imageid = (from objOItem in objDBLevel_Config1.ObjectRels
                                          where objOItem.ID_Object == cstrID_Ontology
                                          join objRef in objDBLevel_Config2.ObjectRels on objOItem.ID_Other equals objRef.ID_Object
                                          where objRef.Name_Object.ToLower() == "attribute_imageid".ToLower() && objRef.Ontology == Globals.Type_AttributeType
                                          select objRef).ToList();

        if (objOList_attribute_imageid.Any())
        {
            OItem_attribute_imageid = new clsOntologyItem()
            {
                GUID = objOList_attribute_imageid.First().ID_Other,
                Name = objOList_attribute_imageid.First().Name_Other,
                GUID_Parent = objOList_attribute_imageid.First().ID_Parent_Other,
                Type = Globals.Type_AttributeType
            };
        }
        else
        {
            throw new Exception("config err");
        }

        var objOList_attribute_major = (from objOItem in objDBLevel_Config1.ObjectRels
                                        where objOItem.ID_Object == cstrID_Ontology
                                        join objRef in objDBLevel_Config2.ObjectRels on objOItem.ID_Other equals objRef.ID_Object
                                        where objRef.Name_Object.ToLower() == "attribute_major".ToLower() && objRef.Ontology == Globals.Type_AttributeType
                                        select objRef).ToList();

        if (objOList_attribute_major.Any())
        {
            OItem_attribute_major = new clsOntologyItem()
            {
                GUID = objOList_attribute_major.First().ID_Other,
                Name = objOList_attribute_major.First().Name_Other,
                GUID_Parent = objOList_attribute_major.First().ID_Parent_Other,
                Type = Globals.Type_AttributeType
            };
        }
        else
        {
            throw new Exception("config err");
        }

        var objOList_attribute_message = (from objOItem in objDBLevel_Config1.ObjectRels
                                          where objOItem.ID_Object == cstrID_Ontology
                                          join objRef in objDBLevel_Config2.ObjectRels on objOItem.ID_Other equals objRef.ID_Object
                                          where objRef.Name_Object.ToLower() == "attribute_message".ToLower() && objRef.Ontology == Globals.Type_AttributeType
                                          select objRef).ToList();

        if (objOList_attribute_message.Any())
        {
            OItem_attribute_message = new clsOntologyItem()
            {
                GUID = objOList_attribute_message.First().ID_Other,
                Name = objOList_attribute_message.First().Name_Other,
                GUID_Parent = objOList_attribute_message.First().ID_Parent_Other,
                Type = Globals.Type_AttributeType
            };
        }
        else
        {
            throw new Exception("config err");
        }

        var objOList_attribute_minor = (from objOItem in objDBLevel_Config1.ObjectRels
                                        where objOItem.ID_Object == cstrID_Ontology
                                        join objRef in objDBLevel_Config2.ObjectRels on objOItem.ID_Other equals objRef.ID_Object
                                        where objRef.Name_Object.ToLower() == "attribute_minor".ToLower() && objRef.Ontology == Globals.Type_AttributeType
                                        select objRef).ToList();

        if (objOList_attribute_minor.Any())
        {
            OItem_attribute_minor = new clsOntologyItem()
            {
                GUID = objOList_attribute_minor.First().ID_Other,
                Name = objOList_attribute_minor.First().Name_Other,
                GUID_Parent = objOList_attribute_minor.First().ID_Parent_Other,
                Type = Globals.Type_AttributeType
            };
        }
        else
        {
            throw new Exception("config err");
        }

        var objOList_attribute_revision = (from objOItem in objDBLevel_Config1.ObjectRels
                                           where objOItem.ID_Object == cstrID_Ontology
                                           join objRef in objDBLevel_Config2.ObjectRels on objOItem.ID_Other equals objRef.ID_Object
                                           where objRef.Name_Object.ToLower() == "attribute_revision".ToLower() && objRef.Ontology == Globals.Type_AttributeType
                                           select objRef).ToList();

        if (objOList_attribute_revision.Any())
        {
            OItem_attribute_revision = new clsOntologyItem()
            {
                GUID = objOList_attribute_revision.First().ID_Other,
                Name = objOList_attribute_revision.First().Name_Other,
                GUID_Parent = objOList_attribute_revision.First().ID_Parent_Other,
                Type = Globals.Type_AttributeType
            };
        }
        else
        {
            throw new Exception("config err");
        }

        var objOList_attribute_xml_text = (from objOItem in objDBLevel_Config1.ObjectRels
                                           where objOItem.ID_Object == cstrID_Ontology
                                           join objRef in objDBLevel_Config2.ObjectRels on objOItem.ID_Other equals objRef.ID_Object
                                           where objRef.Name_Object.ToLower() == "attribute_xml_text".ToLower() && objRef.Ontology == Globals.Type_AttributeType
                                           select objRef).ToList();

        if (objOList_attribute_xml_text.Any())
        {
            OItem_attribute_xml_text = new clsOntologyItem()
            {
                GUID = objOList_attribute_xml_text.First().ID_Other,
                Name = objOList_attribute_xml_text.First().Name_Other,
                GUID_Parent = objOList_attribute_xml_text.First().ID_Parent_Other,
                Type = Globals.Type_AttributeType
            };
        }
        else
        {
            throw new Exception("config err");
        }

        var objOList_attributetype_regex = (from objOItem in objDBLevel_Config1.ObjectRels
                                            where objOItem.ID_Object == cstrID_Ontology
                                            join objRef in objDBLevel_Config2.ObjectRels on objOItem.ID_Other equals objRef.ID_Object
                                            where objRef.Name_Object.ToLower() == "attributetype_regex".ToLower() && objRef.Ontology == Globals.Type_AttributeType
                                            select objRef).ToList();

        if (objOList_attributetype_regex.Any())
        {
            OItem_attributetype_regex = new clsOntologyItem()
            {
                GUID = objOList_attributetype_regex.First().ID_Other,
                Name = objOList_attributetype_regex.First().Name_Other,
                GUID_Parent = objOList_attributetype_regex.First().ID_Parent_Other,
                Type = Globals.Type_AttributeType
            };
        }
        else
        {
            throw new Exception("config err");
        }

        var objOList_attributetype_remove_from_source = (from objOItem in objDBLevel_Config1.ObjectRels
                                                         where objOItem.ID_Object == cstrID_Ontology
                                                         join objRef in objDBLevel_Config2.ObjectRels on objOItem.ID_Other equals objRef.ID_Object
                                                         where objRef.Name_Object.ToLower() == "attributetype_remove_from_source".ToLower() && objRef.Ontology == Globals.Type_AttributeType
                                                         select objRef).ToList();

        if (objOList_attributetype_remove_from_source.Any())
        {
            OItem_attributetype_remove_from_source = new clsOntologyItem()
            {
                GUID = objOList_attributetype_remove_from_source.First().ID_Other,
                Name = objOList_attributetype_remove_from_source.First().Name_Other,
                GUID_Parent = objOList_attributetype_remove_from_source.First().ID_Parent_Other,
                Type = Globals.Type_AttributeType
            };
        }
        else
        {
            throw new Exception("config err");
        }

        var objOList_attributetype_standard = (from objOItem in objDBLevel_Config1.ObjectRels
                                               where objOItem.ID_Object == cstrID_Ontology
                                               join objRef in objDBLevel_Config2.ObjectRels on objOItem.ID_Other equals objRef.ID_Object
                                               where objRef.Name_Object.ToLower() == "attributetype_standard".ToLower() && objRef.Ontology == Globals.Type_AttributeType
                                               select objRef).ToList();

        if (objOList_attributetype_standard.Any())
        {
            OItem_attributetype_standard = new clsOntologyItem()
            {
                GUID = objOList_attributetype_standard.First().ID_Other,
                Name = objOList_attributetype_standard.First().Name_Other,
                GUID_Parent = objOList_attributetype_standard.First().ID_Parent_Other,
                Type = Globals.Type_AttributeType
            };
        }
        else
        {
            throw new Exception("config err");
        }

        var objOList_attributetype_useorderid = (from objOItem in objDBLevel_Config1.ObjectRels
                                                 where objOItem.ID_Object == cstrID_Ontology
                                                 join objRef in objDBLevel_Config2.ObjectRels on objOItem.ID_Other equals objRef.ID_Object
                                                 where objRef.Name_Object.ToLower() == "attributetype_useorderid".ToLower() && objRef.Ontology == Globals.Type_AttributeType
                                                 select objRef).ToList();

        if (objOList_attributetype_useorderid.Any())
        {
            OItem_attributetype_useorderid = new clsOntologyItem()
            {
                GUID = objOList_attributetype_useorderid.First().ID_Other,
                Name = objOList_attributetype_useorderid.First().Name_Other,
                GUID_Parent = objOList_attributetype_useorderid.First().ID_Parent_Other,
                Type = Globals.Type_AttributeType
            };
        }
        else
        {
            throw new Exception("config err");
        }


    }

    private void get_Config_RelationTypes()
    {
        var objOList_relationtype_access_by = (from objOItem in objDBLevel_Config1.ObjectRels
                                               where objOItem.ID_Object == cstrID_Ontology
                                               join objRef in objDBLevel_Config2.ObjectRels on objOItem.ID_Other equals objRef.ID_Object
                                               where objRef.Name_Object.ToLower() == "relationtype_access_by".ToLower() && objRef.Ontology == Globals.Type_RelationType
                                               select objRef).ToList();

        if (objOList_relationtype_access_by.Any())
        {
            OItem_relationtype_access_by = new clsOntologyItem()
            {
                GUID = objOList_relationtype_access_by.First().ID_Other,
                Name = objOList_relationtype_access_by.First().Name_Other,
                GUID_Parent = objOList_relationtype_access_by.First().ID_Parent_Other,
                Type = Globals.Type_RelationType
            };
        }
        else
        {
            throw new Exception("config err");
        }

        var objOList_relationtype_additional = (from objOItem in objDBLevel_Config1.ObjectRels
                                                where objOItem.ID_Object == cstrID_Ontology
                                                join objRef in objDBLevel_Config2.ObjectRels on objOItem.ID_Other equals objRef.ID_Object
                                                where objRef.Name_Object.ToLower() == "relationtype_additional".ToLower() && objRef.Ontology == Globals.Type_RelationType
                                                select objRef).ToList();

        if (objOList_relationtype_additional.Any())
        {
            OItem_relationtype_additional = new clsOntologyItem()
            {
                GUID = objOList_relationtype_additional.First().ID_Other,
                Name = objOList_relationtype_additional.First().Name_Other,
                GUID_Parent = objOList_relationtype_additional.First().ID_Parent_Other,
                Type = Globals.Type_RelationType
            };
        }
        else
        {
            throw new Exception("config err");
        }

        var objOList_relationtype_alternative_for = (from objOItem in objDBLevel_Config1.ObjectRels
                                                     where objOItem.ID_Object == cstrID_Ontology
                                                     join objRef in objDBLevel_Config2.ObjectRels on objOItem.ID_Other equals objRef.ID_Object
                                                     where objRef.Name_Object.ToLower() == "relationtype_alternative_for".ToLower() && objRef.Ontology == Globals.Type_RelationType
                                                     select objRef).ToList();

        if (objOList_relationtype_alternative_for.Any())
        {
            OItem_relationtype_alternative_for = new clsOntologyItem()
            {
                GUID = objOList_relationtype_alternative_for.First().ID_Other,
                Name = objOList_relationtype_alternative_for.First().Name_Other,
                GUID_Parent = objOList_relationtype_alternative_for.First().ID_Parent_Other,
                Type = Globals.Type_RelationType
            };
        }
        else
        {
            throw new Exception("config err");
        }

        var objOList_relationtype_attribute = (from objOItem in objDBLevel_Config1.ObjectRels
                                               where objOItem.ID_Object == cstrID_Ontology
                                               join objRef in objDBLevel_Config2.ObjectRels on objOItem.ID_Other equals objRef.ID_Object
                                               where objRef.Name_Object.ToLower() == "relationtype_attribute".ToLower() && objRef.Ontology == Globals.Type_RelationType
                                               select objRef).ToList();

        if (objOList_relationtype_attribute.Any())
        {
            OItem_relationtype_attribute = new clsOntologyItem()
            {
                GUID = objOList_relationtype_attribute.First().ID_Other,
                Name = objOList_relationtype_attribute.First().Name_Other,
                GUID_Parent = objOList_relationtype_attribute.First().ID_Parent_Other,
                Type = Globals.Type_RelationType
            };
        }
        else
        {
            throw new Exception("config err");
        }

        var objOList_relationtype_belonging_paramter = (from objOItem in objDBLevel_Config1.ObjectRels
                                                        where objOItem.ID_Object == cstrID_Ontology
                                                        join objRef in objDBLevel_Config2.ObjectRels on objOItem.ID_Other equals objRef.ID_Object
                                                        where objRef.Name_Object.ToLower() == "relationtype_belonging_paramter".ToLower() && objRef.Ontology == Globals.Type_RelationType
                                                        select objRef).ToList();

        if (objOList_relationtype_belonging_paramter.Any())
        {
            OItem_relationtype_belonging_paramter = new clsOntologyItem()
            {
                GUID = objOList_relationtype_belonging_paramter.First().ID_Other,
                Name = objOList_relationtype_belonging_paramter.First().Name_Other,
                GUID_Parent = objOList_relationtype_belonging_paramter.First().ID_Parent_Other,
                Type = Globals.Type_RelationType
            };
        }
        else
        {
            throw new Exception("config err");
        }

        var objOList_relationtype_belonging_resource = (from objOItem in objDBLevel_Config1.ObjectRels
                                                        where objOItem.ID_Object == cstrID_Ontology
                                                        join objRef in objDBLevel_Config2.ObjectRels on objOItem.ID_Other equals objRef.ID_Object
                                                        where objRef.Name_Object.ToLower() == "relationtype_belonging_resource".ToLower() && objRef.Ontology == Globals.Type_RelationType
                                                        select objRef).ToList();

        if (objOList_relationtype_belonging_resource.Any())
        {
            OItem_relationtype_belonging_resource = new clsOntologyItem()
            {
                GUID = objOList_relationtype_belonging_resource.First().ID_Other,
                Name = objOList_relationtype_belonging_resource.First().Name_Other,
                GUID_Parent = objOList_relationtype_belonging_resource.First().ID_Parent_Other,
                Type = Globals.Type_RelationType
            };
        }
        else
        {
            throw new Exception("config err");
        }

        var objOList_relationtype_belongsto = (from objOItem in objDBLevel_Config1.ObjectRels
                                               where objOItem.ID_Object == cstrID_Ontology
                                               join objRef in objDBLevel_Config2.ObjectRels on objOItem.ID_Other equals objRef.ID_Object
                                               where objRef.Name_Object.ToLower() == "relationtype_belongsto".ToLower() && objRef.Ontology == Globals.Type_RelationType
                                               select objRef).ToList();

        if (objOList_relationtype_belongsto.Any())
        {
            OItem_relationtype_belongsto = new clsOntologyItem()
            {
                GUID = objOList_relationtype_belongsto.First().ID_Other,
                Name = objOList_relationtype_belongsto.First().Name_Other,
                GUID_Parent = objOList_relationtype_belongsto.First().ID_Parent_Other,
                Type = Globals.Type_RelationType
            };
        }
        else
        {
            throw new Exception("config err");
        }

        var objOList_relationtype_class = (from objOItem in objDBLevel_Config1.ObjectRels
                                           where objOItem.ID_Object == cstrID_Ontology
                                           join objRef in objDBLevel_Config2.ObjectRels on objOItem.ID_Other equals objRef.ID_Object
                                           where objRef.Name_Object.ToLower() == "relationtype_class".ToLower() && objRef.Ontology == Globals.Type_RelationType
                                           select objRef).ToList();

        if (objOList_relationtype_class.Any())
        {
            OItem_relationtype_class = new clsOntologyItem()
            {
                GUID = objOList_relationtype_class.First().ID_Other,
                Name = objOList_relationtype_class.First().Name_Other,
                GUID_Parent = objOList_relationtype_class.First().ID_Parent_Other,
                Type = Globals.Type_RelationType
            };
        }
        else
        {
            throw new Exception("config err");
        }

        var objOList_relationtype_contains = (from objOItem in objDBLevel_Config1.ObjectRels
                                              where objOItem.ID_Object == cstrID_Ontology
                                              join objRef in objDBLevel_Config2.ObjectRels on objOItem.ID_Other equals objRef.ID_Object
                                              where objRef.Name_Object.ToLower() == "relationtype_contains".ToLower() && objRef.Ontology == Globals.Type_RelationType
                                              select objRef).ToList();

        if (objOList_relationtype_contains.Any())
        {
            OItem_relationtype_contains = new clsOntologyItem()
            {
                GUID = objOList_relationtype_contains.First().ID_Other,
                Name = objOList_relationtype_contains.First().Name_Other,
                GUID_Parent = objOList_relationtype_contains.First().ID_Parent_Other,
                Type = Globals.Type_RelationType
            };
        }
        else
        {
            throw new Exception("config err");
        }

        var objOList_relationtype_declaration = (from objOItem in objDBLevel_Config1.ObjectRels
                                                 where objOItem.ID_Object == cstrID_Ontology
                                                 join objRef in objDBLevel_Config2.ObjectRels on objOItem.ID_Other equals objRef.ID_Object
                                                 where objRef.Name_Object.ToLower() == "relationtype_declaration".ToLower() && objRef.Ontology == Globals.Type_RelationType
                                                 select objRef).ToList();

        if (objOList_relationtype_declaration.Any())
        {
            OItem_relationtype_declaration = new clsOntologyItem()
            {
                GUID = objOList_relationtype_declaration.First().ID_Other,
                Name = objOList_relationtype_declaration.First().Name_Other,
                GUID_Parent = objOList_relationtype_declaration.First().ID_Parent_Other,
                Type = Globals.Type_RelationType
            };
        }
        else
        {
            throw new Exception("config err");
        }

        var objOList_relationtype_describes = (from objOItem in objDBLevel_Config1.ObjectRels
                                               where objOItem.ID_Object == cstrID_Ontology
                                               join objRef in objDBLevel_Config2.ObjectRels on objOItem.ID_Other equals objRef.ID_Object
                                               where objRef.Name_Object.ToLower() == "relationtype_describes".ToLower() && objRef.Ontology == Globals.Type_RelationType
                                               select objRef).ToList();

        if (objOList_relationtype_describes.Any())
        {
            OItem_relationtype_describes = new clsOntologyItem()
            {
                GUID = objOList_relationtype_describes.First().ID_Other,
                Name = objOList_relationtype_describes.First().Name_Other,
                GUID_Parent = objOList_relationtype_describes.First().ID_Parent_Other,
                Type = Globals.Type_RelationType
            };
        }
        else
        {
            throw new Exception("config err");
        }

        var objOList_relationtype_directed_by = (from objOItem in objDBLevel_Config1.ObjectRels
                                                 where objOItem.ID_Object == cstrID_Ontology
                                                 join objRef in objDBLevel_Config2.ObjectRels on objOItem.ID_Other equals objRef.ID_Object
                                                 where objRef.Name_Object.ToLower() == "relationtype_directed_by".ToLower() && objRef.Ontology == Globals.Type_RelationType
                                                 select objRef).ToList();

        if (objOList_relationtype_directed_by.Any())
        {
            OItem_relationtype_directed_by = new clsOntologyItem()
            {
                GUID = objOList_relationtype_directed_by.First().ID_Other,
                Name = objOList_relationtype_directed_by.First().Name_Other,
                GUID_Parent = objOList_relationtype_directed_by.First().ID_Parent_Other,
                Type = Globals.Type_RelationType
            };
        }
        else
        {
            throw new Exception("config err");
        }

        var objOList_relationtype_document_container = (from objOItem in objDBLevel_Config1.ObjectRels
                                                        where objOItem.ID_Object == cstrID_Ontology
                                                        join objRef in objDBLevel_Config2.ObjectRels on objOItem.ID_Other equals objRef.ID_Object
                                                        where objRef.Name_Object.ToLower() == "relationtype_document_container".ToLower() && objRef.Ontology == Globals.Type_RelationType
                                                        select objRef).ToList();

        if (objOList_relationtype_document_container.Any())
        {
            OItem_relationtype_document_container = new clsOntologyItem()
            {
                GUID = objOList_relationtype_document_container.First().ID_Other,
                Name = objOList_relationtype_document_container.First().Name_Other,
                GUID_Parent = objOList_relationtype_document_container.First().ID_Parent_Other,
                Type = Globals.Type_RelationType
            };
        }
        else
        {
            throw new Exception("config err");
        }

        var objOList_relationtype_entry = (from objOItem in objDBLevel_Config1.ObjectRels
                                           where objOItem.ID_Object == cstrID_Ontology
                                           join objRef in objDBLevel_Config2.ObjectRels on objOItem.ID_Other equals objRef.ID_Object
                                           where objRef.Name_Object.ToLower() == "relationtype_entry".ToLower() && objRef.Ontology == Globals.Type_RelationType
                                           select objRef).ToList();

        if (objOList_relationtype_entry.Any())
        {
            OItem_relationtype_entry = new clsOntologyItem()
            {
                GUID = objOList_relationtype_entry.First().ID_Other,
                Name = objOList_relationtype_entry.First().Name_Other,
                GUID_Parent = objOList_relationtype_entry.First().ID_Parent_Other,
                Type = Globals.Type_RelationType
            };
        }
        else
        {
            throw new Exception("config err");
        }

        var objOList_relationtype_error_message = (from objOItem in objDBLevel_Config1.ObjectRels
                                                   where objOItem.ID_Object == cstrID_Ontology
                                                   join objRef in objDBLevel_Config2.ObjectRels on objOItem.ID_Other equals objRef.ID_Object
                                                   where objRef.Name_Object.ToLower() == "relationtype_error_message".ToLower() && objRef.Ontology == Globals.Type_RelationType
                                                   select objRef).ToList();

        if (objOList_relationtype_error_message.Any())
        {
            OItem_relationtype_error_message = new clsOntologyItem()
            {
                GUID = objOList_relationtype_error_message.First().ID_Other,
                Name = objOList_relationtype_error_message.First().Name_Other,
                GUID_Parent = objOList_relationtype_error_message.First().ID_Parent_Other,
                Type = Globals.Type_RelationType
            };
        }
        else
        {
            throw new Exception("config err");
        }

        var objOList_relationtype_export_to = (from objOItem in objDBLevel_Config1.ObjectRels
                                               where objOItem.ID_Object == cstrID_Ontology
                                               join objRef in objDBLevel_Config2.ObjectRels on objOItem.ID_Other equals objRef.ID_Object
                                               where objRef.Name_Object.ToLower() == "relationtype_export_to".ToLower() && objRef.Ontology == Globals.Type_RelationType
                                               select objRef).ToList();

        if (objOList_relationtype_export_to.Any())
        {
            OItem_relationtype_export_to = new clsOntologyItem()
            {
                GUID = objOList_relationtype_export_to.First().ID_Other,
                Name = objOList_relationtype_export_to.First().Name_Other,
                GUID_Parent = objOList_relationtype_export_to.First().ID_Parent_Other,
                Type = Globals.Type_RelationType
            };
        }
        else
        {
            throw new Exception("config err");
        }

        var objOList_relationtype_handles = (from objOItem in objDBLevel_Config1.ObjectRels
                                             where objOItem.ID_Object == cstrID_Ontology
                                             join objRef in objDBLevel_Config2.ObjectRels on objOItem.ID_Other equals objRef.ID_Object
                                             where objRef.Name_Object.ToLower() == "relationtype_handles".ToLower() && objRef.Ontology == Globals.Type_RelationType
                                             select objRef).ToList();

        if (objOList_relationtype_handles.Any())
        {
            OItem_relationtype_handles = new clsOntologyItem()
            {
                GUID = objOList_relationtype_handles.First().ID_Other,
                Name = objOList_relationtype_handles.First().Name_Other,
                GUID_Parent = objOList_relationtype_handles.First().ID_Parent_Other,
                Type = Globals.Type_RelationType
            };
        }
        else
        {
            throw new Exception("config err");
        }

        var objOList_relationtype_happened = (from objOItem in objDBLevel_Config1.ObjectRels
                                              where objOItem.ID_Object == cstrID_Ontology
                                              join objRef in objDBLevel_Config2.ObjectRels on objOItem.ID_Other equals objRef.ID_Object
                                              where objRef.Name_Object.ToLower() == "relationtype_happened".ToLower() && objRef.Ontology == Globals.Type_RelationType
                                              select objRef).ToList();

        if (objOList_relationtype_happened.Any())
        {
            OItem_relationtype_happened = new clsOntologyItem()
            {
                GUID = objOList_relationtype_happened.First().ID_Other,
                Name = objOList_relationtype_happened.First().Name_Other,
                GUID_Parent = objOList_relationtype_happened.First().ID_Parent_Other,
                Type = Globals.Type_RelationType
            };
        }
        else
        {
            throw new Exception("config err");
        }

        var objOList_relationtype_ignore = (from objOItem in objDBLevel_Config1.ObjectRels
                                            where objOItem.ID_Object == cstrID_Ontology
                                            join objRef in objDBLevel_Config2.ObjectRels on objOItem.ID_Other equals objRef.ID_Object
                                            where objRef.Name_Object.ToLower() == "relationtype_ignore".ToLower() && objRef.Ontology == Globals.Type_RelationType
                                            select objRef).ToList();

        if (objOList_relationtype_ignore.Any())
        {
            OItem_relationtype_ignore = new clsOntologyItem()
            {
                GUID = objOList_relationtype_ignore.First().ID_Other,
                Name = objOList_relationtype_ignore.First().Name_Other,
                GUID_Parent = objOList_relationtype_ignore.First().ID_Parent_Other,
                Type = Globals.Type_RelationType
            };
        }
        else
        {
            throw new Exception("config err");
        }

        var objOList_relationtype_initializing = (from objOItem in objDBLevel_Config1.ObjectRels
                                                  where objOItem.ID_Object == cstrID_Ontology
                                                  join objRef in objDBLevel_Config2.ObjectRels on objOItem.ID_Other equals objRef.ID_Object
                                                  where objRef.Name_Object.ToLower() == "relationtype_initializing".ToLower() && objRef.Ontology == Globals.Type_RelationType
                                                  select objRef).ToList();

        if (objOList_relationtype_initializing.Any())
        {
            OItem_relationtype_initializing = new clsOntologyItem()
            {
                GUID = objOList_relationtype_initializing.First().ID_Other,
                Name = objOList_relationtype_initializing.First().Name_Other,
                GUID_Parent = objOList_relationtype_initializing.First().ID_Parent_Other,
                Type = Globals.Type_RelationType
            };
        }
        else
        {
            throw new Exception("config err");
        }

        var objOList_relationtype_input_message = (from objOItem in objDBLevel_Config1.ObjectRels
                                                   where objOItem.ID_Object == cstrID_Ontology
                                                   join objRef in objDBLevel_Config2.ObjectRels on objOItem.ID_Other equals objRef.ID_Object
                                                   where objRef.Name_Object.ToLower() == "relationtype_input_message".ToLower() && objRef.Ontology == Globals.Type_RelationType
                                                   select objRef).ToList();

        if (objOList_relationtype_input_message.Any())
        {
            OItem_relationtype_input_message = new clsOntologyItem()
            {
                GUID = objOList_relationtype_input_message.First().ID_Other,
                Name = objOList_relationtype_input_message.First().Name_Other,
                GUID_Parent = objOList_relationtype_input_message.First().ID_Parent_Other,
                Type = Globals.Type_RelationType
            };
        }
        else
        {
            throw new Exception("config err");
        }

        var objOList_relationtype_invoked_actor = (from objOItem in objDBLevel_Config1.ObjectRels
                                                   where objOItem.ID_Object == cstrID_Ontology
                                                   join objRef in objDBLevel_Config2.ObjectRels on objOItem.ID_Other equals objRef.ID_Object
                                                   where objRef.Name_Object.ToLower() == "relationtype_invoked_actor".ToLower() && objRef.Ontology == Globals.Type_RelationType
                                                   select objRef).ToList();

        if (objOList_relationtype_invoked_actor.Any())
        {
            OItem_relationtype_invoked_actor = new clsOntologyItem()
            {
                GUID = objOList_relationtype_invoked_actor.First().ID_Other,
                Name = objOList_relationtype_invoked_actor.First().Name_Other,
                GUID_Parent = objOList_relationtype_invoked_actor.First().ID_Parent_Other,
                Type = Globals.Type_RelationType
            };
        }
        else
        {
            throw new Exception("config err");
        }

        var objOList_relationtype_invoking_actor = (from objOItem in objDBLevel_Config1.ObjectRels
                                                    where objOItem.ID_Object == cstrID_Ontology
                                                    join objRef in objDBLevel_Config2.ObjectRels on objOItem.ID_Other equals objRef.ID_Object
                                                    where objRef.Name_Object.ToLower() == "relationtype_invoking_actor".ToLower() && objRef.Ontology == Globals.Type_RelationType
                                                    select objRef).ToList();

        if (objOList_relationtype_invoking_actor.Any())
        {
            OItem_relationtype_invoking_actor = new clsOntologyItem()
            {
                GUID = objOList_relationtype_invoking_actor.First().ID_Other,
                Name = objOList_relationtype_invoking_actor.First().Name_Other,
                GUID_Parent = objOList_relationtype_invoking_actor.First().ID_Parent_Other,
                Type = Globals.Type_RelationType
            };
        }
        else
        {
            throw new Exception("config err");
        }

        var objOList_relationtype_is_defined_by = (from objOItem in objDBLevel_Config1.ObjectRels
                                                   where objOItem.ID_Object == cstrID_Ontology
                                                   join objRef in objDBLevel_Config2.ObjectRels on objOItem.ID_Other equals objRef.ID_Object
                                                   where objRef.Name_Object.ToLower() == "relationtype_is_defined_by".ToLower() && objRef.Ontology == Globals.Type_RelationType
                                                   select objRef).ToList();

        if (objOList_relationtype_is_defined_by.Any())
        {
            OItem_relationtype_is_defined_by = new clsOntologyItem()
            {
                GUID = objOList_relationtype_is_defined_by.First().ID_Other,
                Name = objOList_relationtype_is_defined_by.First().Name_Other,
                GUID_Parent = objOList_relationtype_is_defined_by.First().ID_Parent_Other,
                Type = Globals.Type_RelationType
            };
        }
        else
        {
            throw new Exception("config err");
        }

        var objOList_relationtype_is_instance_of = (from objOItem in objDBLevel_Config1.ObjectRels
                                                    where objOItem.ID_Object == cstrID_Ontology
                                                    join objRef in objDBLevel_Config2.ObjectRels on objOItem.ID_Other equals objRef.ID_Object
                                                    where objRef.Name_Object.ToLower() == "relationtype_is_instance_of".ToLower() && objRef.Ontology == Globals.Type_RelationType
                                                    select objRef).ToList();

        if (objOList_relationtype_is_instance_of.Any())
        {
            OItem_relationtype_is_instance_of = new clsOntologyItem()
            {
                GUID = objOList_relationtype_is_instance_of.First().ID_Other,
                Name = objOList_relationtype_is_instance_of.First().Name_Other,
                GUID_Parent = objOList_relationtype_is_instance_of.First().ID_Parent_Other,
                Type = Globals.Type_RelationType
            };
        }
        else
        {
            throw new Exception("config err");
        }

        var objOList_relationtype_is_of_type = (from objOItem in objDBLevel_Config1.ObjectRels
                                                where objOItem.ID_Object == cstrID_Ontology
                                                join objRef in objDBLevel_Config2.ObjectRels on objOItem.ID_Other equals objRef.ID_Object
                                                where objRef.Name_Object.ToLower() == "relationtype_is_of_type".ToLower() && objRef.Ontology == Globals.Type_RelationType
                                                select objRef).ToList();

        if (objOList_relationtype_is_of_type.Any())
        {
            OItem_relationtype_is_of_type = new clsOntologyItem()
            {
                GUID = objOList_relationtype_is_of_type.First().ID_Other,
                Name = objOList_relationtype_is_of_type.First().Name_Other,
                GUID_Parent = objOList_relationtype_is_of_type.First().ID_Parent_Other,
                Type = Globals.Type_RelationType
            };
        }
        else
        {
            throw new Exception("config err");
        }

        var objOList_relationtype_isdescribedby = (from objOItem in objDBLevel_Config1.ObjectRels
                                                   where objOItem.ID_Object == cstrID_Ontology
                                                   join objRef in objDBLevel_Config2.ObjectRels on objOItem.ID_Other equals objRef.ID_Object
                                                   where objRef.Name_Object.ToLower() == "relationtype_isdescribedby".ToLower() && objRef.Ontology == Globals.Type_RelationType
                                                   select objRef).ToList();

        if (objOList_relationtype_isdescribedby.Any())
        {
            OItem_relationtype_isdescribedby = new clsOntologyItem()
            {
                GUID = objOList_relationtype_isdescribedby.First().ID_Other,
                Name = objOList_relationtype_isdescribedby.First().Name_Other,
                GUID_Parent = objOList_relationtype_isdescribedby.First().ID_Parent_Other,
                Type = Globals.Type_RelationType
            };
        }
        else
        {
            throw new Exception("config err");
        }

        var objOList_relationtype_isinstate = (from objOItem in objDBLevel_Config1.ObjectRels
                                               where objOItem.ID_Object == cstrID_Ontology
                                               join objRef in objDBLevel_Config2.ObjectRels on objOItem.ID_Other equals objRef.ID_Object
                                               where objRef.Name_Object.ToLower() == "relationtype_isinstate".ToLower() && objRef.Ontology == Globals.Type_RelationType
                                               select objRef).ToList();

        if (objOList_relationtype_isinstate.Any())
        {
            OItem_relationtype_isinstate = new clsOntologyItem()
            {
                GUID = objOList_relationtype_isinstate.First().ID_Other,
                Name = objOList_relationtype_isinstate.First().Name_Other,
                GUID_Parent = objOList_relationtype_isinstate.First().ID_Parent_Other,
                Type = Globals.Type_RelationType
            };
        }
        else
        {
            throw new Exception("config err");
        }

        var objOList_relationtype_issubordinated = (from objOItem in objDBLevel_Config1.ObjectRels
                                                    where objOItem.ID_Object == cstrID_Ontology
                                                    join objRef in objDBLevel_Config2.ObjectRels on objOItem.ID_Other equals objRef.ID_Object
                                                    where objRef.Name_Object.ToLower() == "relationtype_issubordinated".ToLower() && objRef.Ontology == Globals.Type_RelationType
                                                    select objRef).ToList();

        if (objOList_relationtype_issubordinated.Any())
        {
            OItem_relationtype_issubordinated = new clsOntologyItem()
            {
                GUID = objOList_relationtype_issubordinated.First().ID_Other,
                Name = objOList_relationtype_issubordinated.First().Name_Other,
                GUID_Parent = objOList_relationtype_issubordinated.First().ID_Parent_Other,
                Type = Globals.Type_RelationType
            };
        }
        else
        {
            throw new Exception("config err");
        }

        var objOList_relationtype_iswrittenin = (from objOItem in objDBLevel_Config1.ObjectRels
                                                 where objOItem.ID_Object == cstrID_Ontology
                                                 join objRef in objDBLevel_Config2.ObjectRels on objOItem.ID_Other equals objRef.ID_Object
                                                 where objRef.Name_Object.ToLower() == "relationtype_iswrittenin".ToLower() && objRef.Ontology == Globals.Type_RelationType
                                                 select objRef).ToList();

        if (objOList_relationtype_iswrittenin.Any())
        {
            OItem_relationtype_iswrittenin = new clsOntologyItem()
            {
                GUID = objOList_relationtype_iswrittenin.First().ID_Other,
                Name = objOList_relationtype_iswrittenin.First().Name_Other,
                GUID_Parent = objOList_relationtype_iswrittenin.First().ID_Parent_Other,
                Type = Globals.Type_RelationType
            };
        }
        else
        {
            throw new Exception("config err");
        }

        var objOList_relationtype_located_in = (from objOItem in objDBLevel_Config1.ObjectRels
                                                where objOItem.ID_Object == cstrID_Ontology
                                                join objRef in objDBLevel_Config2.ObjectRels on objOItem.ID_Other equals objRef.ID_Object
                                                where objRef.Name_Object.ToLower() == "relationtype_located_in".ToLower() && objRef.Ontology == Globals.Type_RelationType
                                                select objRef).ToList();

        if (objOList_relationtype_located_in.Any())
        {
            OItem_relationtype_located_in = new clsOntologyItem()
            {
                GUID = objOList_relationtype_located_in.First().ID_Other,
                Name = objOList_relationtype_located_in.First().Name_Other,
                GUID_Parent = objOList_relationtype_located_in.First().ID_Parent_Other,
                Type = Globals.Type_RelationType
            };
        }
        else
        {
            throw new Exception("config err");
        }

        var objOList_relationtype_main = (from objOItem in objDBLevel_Config1.ObjectRels
                                          where objOItem.ID_Object == cstrID_Ontology
                                          join objRef in objDBLevel_Config2.ObjectRels on objOItem.ID_Other equals objRef.ID_Object
                                          where objRef.Name_Object.ToLower() == "relationtype_main".ToLower() && objRef.Ontology == Globals.Type_RelationType
                                          select objRef).ToList();

        if (objOList_relationtype_main.Any())
        {
            OItem_relationtype_main = new clsOntologyItem()
            {
                GUID = objOList_relationtype_main.First().ID_Other,
                Name = objOList_relationtype_main.First().Name_Other,
                GUID_Parent = objOList_relationtype_main.First().ID_Parent_Other,
                Type = Globals.Type_RelationType
            };
        }
        else
        {
            throw new Exception("config err");
        }

        var objOList_relationtype_needs = (from objOItem in objDBLevel_Config1.ObjectRels
                                           where objOItem.ID_Object == cstrID_Ontology
                                           join objRef in objDBLevel_Config2.ObjectRels on objOItem.ID_Other equals objRef.ID_Object
                                           where objRef.Name_Object.ToLower() == "relationtype_needs".ToLower() && objRef.Ontology == Globals.Type_RelationType
                                           select objRef).ToList();

        if (objOList_relationtype_needs.Any())
        {
            OItem_relationtype_needs = new clsOntologyItem()
            {
                GUID = objOList_relationtype_needs.First().ID_Other,
                Name = objOList_relationtype_needs.First().Name_Other,
                GUID_Parent = objOList_relationtype_needs.First().ID_Parent_Other,
                Type = Globals.Type_RelationType
            };
        }
        else
        {
            throw new Exception("config err");
        }

        var objOList_relationtype_object = (from objOItem in objDBLevel_Config1.ObjectRels
                                            where objOItem.ID_Object == cstrID_Ontology
                                            join objRef in objDBLevel_Config2.ObjectRels on objOItem.ID_Other equals objRef.ID_Object
                                            where objRef.Name_Object.ToLower() == "relationtype_object".ToLower() && objRef.Ontology == Globals.Type_RelationType
                                            select objRef).ToList();

        if (objOList_relationtype_object.Any())
        {
            OItem_relationtype_object = new clsOntologyItem()
            {
                GUID = objOList_relationtype_object.First().ID_Other,
                Name = objOList_relationtype_object.First().Name_Other,
                GUID_Parent = objOList_relationtype_object.First().ID_Parent_Other,
                Type = Globals.Type_RelationType
            };
        }
        else
        {
            throw new Exception("config err");
        }

        var objOList_relationtype_offered_by = (from objOItem in objDBLevel_Config1.ObjectRels
                                                where objOItem.ID_Object == cstrID_Ontology
                                                join objRef in objDBLevel_Config2.ObjectRels on objOItem.ID_Other equals objRef.ID_Object
                                                where objRef.Name_Object.ToLower() == "relationtype_offered_by".ToLower() && objRef.Ontology == Globals.Type_RelationType
                                                select objRef).ToList();

        if (objOList_relationtype_offered_by.Any())
        {
            OItem_relationtype_offered_by = new clsOntologyItem()
            {
                GUID = objOList_relationtype_offered_by.First().ID_Other,
                Name = objOList_relationtype_offered_by.First().Name_Other,
                GUID_Parent = objOList_relationtype_offered_by.First().ID_Parent_Other,
                Type = Globals.Type_RelationType
            };
        }
        else
        {
            throw new Exception("config err");
        }

        var objOList_relationtype_offers = (from objOItem in objDBLevel_Config1.ObjectRels
                                            where objOItem.ID_Object == cstrID_Ontology
                                            join objRef in objDBLevel_Config2.ObjectRels on objOItem.ID_Other equals objRef.ID_Object
                                            where objRef.Name_Object.ToLower() == "relationtype_offers".ToLower() && objRef.Ontology == Globals.Type_RelationType
                                            select objRef).ToList();

        if (objOList_relationtype_offers.Any())
        {
            OItem_relationtype_offers = new clsOntologyItem()
            {
                GUID = objOList_relationtype_offers.First().ID_Other,
                Name = objOList_relationtype_offers.First().Name_Other,
                GUID_Parent = objOList_relationtype_offers.First().ID_Parent_Other,
                Type = Globals.Type_RelationType
            };
        }
        else
        {
            throw new Exception("config err");
        }

        var objOList_relationtype_possible = (from objOItem in objDBLevel_Config1.ObjectRels
                                              where objOItem.ID_Object == cstrID_Ontology
                                              join objRef in objDBLevel_Config2.ObjectRels on objOItem.ID_Other equals objRef.ID_Object
                                              where objRef.Name_Object.ToLower() == "relationtype_possible".ToLower() && objRef.Ontology == Globals.Type_RelationType
                                              select objRef).ToList();

        if (objOList_relationtype_possible.Any())
        {
            OItem_relationtype_possible = new clsOntologyItem()
            {
                GUID = objOList_relationtype_possible.First().ID_Other,
                Name = objOList_relationtype_possible.First().Name_Other,
                GUID_Parent = objOList_relationtype_possible.First().ID_Parent_Other,
                Type = Globals.Type_RelationType
            };
        }
        else
        {
            throw new Exception("config err");
        }

        var objOList_relationtype_posts = (from objOItem in objDBLevel_Config1.ObjectRels
                                           where objOItem.ID_Object == cstrID_Ontology
                                           join objRef in objDBLevel_Config2.ObjectRels on objOItem.ID_Other equals objRef.ID_Object
                                           where objRef.Name_Object.ToLower() == "relationtype_posts".ToLower() && objRef.Ontology == Globals.Type_RelationType
                                           select objRef).ToList();

        if (objOList_relationtype_posts.Any())
        {
            OItem_relationtype_posts = new clsOntologyItem()
            {
                GUID = objOList_relationtype_posts.First().ID_Other,
                Name = objOList_relationtype_posts.First().Name_Other,
                GUID_Parent = objOList_relationtype_posts.First().ID_Parent_Other,
                Type = Globals.Type_RelationType
            };
        }
        else
        {
            throw new Exception("config err");
        }

        var objOList_relationtype_pre = (from objOItem in objDBLevel_Config1.ObjectRels
                                         where objOItem.ID_Object == cstrID_Ontology
                                         join objRef in objDBLevel_Config2.ObjectRels on objOItem.ID_Other equals objRef.ID_Object
                                         where objRef.Name_Object.ToLower() == "relationtype_pre".ToLower() && objRef.Ontology == Globals.Type_RelationType
                                         select objRef).ToList();

        if (objOList_relationtype_pre.Any())
        {
            OItem_relationtype_pre = new clsOntologyItem()
            {
                GUID = objOList_relationtype_pre.First().ID_Other,
                Name = objOList_relationtype_pre.First().Name_Other,
                GUID_Parent = objOList_relationtype_pre.First().ID_Parent_Other,
                Type = Globals.Type_RelationType
            };
        }
        else
        {
            throw new Exception("config err");
        }

        var objOList_relationtype_project_file = (from objOItem in objDBLevel_Config1.ObjectRels
                                                  where objOItem.ID_Object == cstrID_Ontology
                                                  join objRef in objDBLevel_Config2.ObjectRels on objOItem.ID_Other equals objRef.ID_Object
                                                  where objRef.Name_Object.ToLower() == "relationtype_project_file".ToLower() && objRef.Ontology == Globals.Type_RelationType
                                                  select objRef).ToList();

        if (objOList_relationtype_project_file.Any())
        {
            OItem_relationtype_project_file = new clsOntologyItem()
            {
                GUID = objOList_relationtype_project_file.First().ID_Other,
                Name = objOList_relationtype_project_file.First().Name_Other,
                GUID_Parent = objOList_relationtype_project_file.First().ID_Parent_Other,
                Type = Globals.Type_RelationType
            };
        }
        else
        {
            throw new Exception("config err");
        }

        var objOList_relationtype_property = (from objOItem in objDBLevel_Config1.ObjectRels
                                              where objOItem.ID_Object == cstrID_Ontology
                                              join objRef in objDBLevel_Config2.ObjectRels on objOItem.ID_Other equals objRef.ID_Object
                                              where objRef.Name_Object.ToLower() == "relationtype_property".ToLower() && objRef.Ontology == Globals.Type_RelationType
                                              select objRef).ToList();

        if (objOList_relationtype_property.Any())
        {
            OItem_relationtype_property = new clsOntologyItem()
            {
                GUID = objOList_relationtype_property.First().ID_Other,
                Name = objOList_relationtype_property.First().Name_Other,
                GUID_Parent = objOList_relationtype_property.First().ID_Parent_Other,
                Type = Globals.Type_RelationType
            };
        }
        else
        {
            throw new Exception("config err");
        }

        var objOList_relationtype_provides = (from objOItem in objDBLevel_Config1.ObjectRels
                                              where objOItem.ID_Object == cstrID_Ontology
                                              join objRef in objDBLevel_Config2.ObjectRels on objOItem.ID_Other equals objRef.ID_Object
                                              where objRef.Name_Object.ToLower() == "relationtype_provides".ToLower() && objRef.Ontology == Globals.Type_RelationType
                                              select objRef).ToList();

        if (objOList_relationtype_provides.Any())
        {
            OItem_relationtype_provides = new clsOntologyItem()
            {
                GUID = objOList_relationtype_provides.First().ID_Other,
                Name = objOList_relationtype_provides.First().Name_Other,
                GUID_Parent = objOList_relationtype_provides.First().ID_Parent_Other,
                Type = Globals.Type_RelationType
            };
        }
        else
        {
            throw new Exception("config err");
        }

        var objOList_relationtype_relationtype = (from objOItem in objDBLevel_Config1.ObjectRels
                                                  where objOItem.ID_Object == cstrID_Ontology
                                                  join objRef in objDBLevel_Config2.ObjectRels on objOItem.ID_Other equals objRef.ID_Object
                                                  where objRef.Name_Object.ToLower() == "relationtype_relationtype".ToLower() && objRef.Ontology == Globals.Type_RelationType
                                                  select objRef).ToList();

        if (objOList_relationtype_relationtype.Any())
        {
            OItem_relationtype_relationtype = new clsOntologyItem()
            {
                GUID = objOList_relationtype_relationtype.First().ID_Other,
                Name = objOList_relationtype_relationtype.First().Name_Other,
                GUID_Parent = objOList_relationtype_relationtype.First().ID_Parent_Other,
                Type = Globals.Type_RelationType
            };
        }
        else
        {
            throw new Exception("config err");
        }

        var objOList_relationtype_request = (from objOItem in objDBLevel_Config1.ObjectRels
                                             where objOItem.ID_Object == cstrID_Ontology
                                             join objRef in objDBLevel_Config2.ObjectRels on objOItem.ID_Other equals objRef.ID_Object
                                             where objRef.Name_Object.ToLower() == "relationtype_request".ToLower() && objRef.Ontology == Globals.Type_RelationType
                                             select objRef).ToList();

        if (objOList_relationtype_request.Any())
        {
            OItem_relationtype_request = new clsOntologyItem()
            {
                GUID = objOList_relationtype_request.First().ID_Other,
                Name = objOList_relationtype_request.First().Name_Other,
                GUID_Parent = objOList_relationtype_request.First().ID_Parent_Other,
                Type = Globals.Type_RelationType
            };
        }
        else
        {
            throw new Exception("config err");
        }

        var objOList_relationtype_sourceslocatedin = (from objOItem in objDBLevel_Config1.ObjectRels
                                                      where objOItem.ID_Object == cstrID_Ontology
                                                      join objRef in objDBLevel_Config2.ObjectRels on objOItem.ID_Other equals objRef.ID_Object
                                                      where objRef.Name_Object.ToLower() == "relationtype_sourceslocatedin".ToLower() && objRef.Ontology == Globals.Type_RelationType
                                                      select objRef).ToList();

        if (objOList_relationtype_sourceslocatedin.Any())
        {
            OItem_relationtype_sourceslocatedin = new clsOntologyItem()
            {
                GUID = objOList_relationtype_sourceslocatedin.First().ID_Other,
                Name = objOList_relationtype_sourceslocatedin.First().Name_Other,
                GUID_Parent = objOList_relationtype_sourceslocatedin.First().ID_Parent_Other,
                Type = Globals.Type_RelationType
            };
        }
        else
        {
            throw new Exception("config err");
        }

        var objOList_relationtype_standard = (from objOItem in objDBLevel_Config1.ObjectRels
                                              where objOItem.ID_Object == cstrID_Ontology
                                              join objRef in objDBLevel_Config2.ObjectRels on objOItem.ID_Other equals objRef.ID_Object
                                              where objRef.Name_Object.ToLower() == "relationtype_standard".ToLower() && objRef.Ontology == Globals.Type_RelationType
                                              select objRef).ToList();

        if (objOList_relationtype_standard.Any())
        {
            OItem_relationtype_standard = new clsOntologyItem()
            {
                GUID = objOList_relationtype_standard.First().ID_Other,
                Name = objOList_relationtype_standard.First().Name_Other,
                GUID_Parent = objOList_relationtype_standard.First().ID_Parent_Other,
                Type = Globals.Type_RelationType
            };
        }
        else
        {
            throw new Exception("config err");
        }

        var objOList_relationtype_subpath_versionfile = (from objOItem in objDBLevel_Config1.ObjectRels
                                                         where objOItem.ID_Object == cstrID_Ontology
                                                         join objRef in objDBLevel_Config2.ObjectRels on objOItem.ID_Other equals objRef.ID_Object
                                                         where objRef.Name_Object.ToLower() == "relationtype_subpath_versionfile".ToLower() && objRef.Ontology == Globals.Type_RelationType
                                                         select objRef).ToList();

        if (objOList_relationtype_subpath_versionfile.Any())
        {
            OItem_relationtype_subpath_versionfile = new clsOntologyItem()
            {
                GUID = objOList_relationtype_subpath_versionfile.First().ID_Other,
                Name = objOList_relationtype_subpath_versionfile.First().Name_Other,
                GUID_Parent = objOList_relationtype_subpath_versionfile.First().ID_Parent_Other,
                Type = Globals.Type_RelationType
            };
        }
        else
        {
            throw new Exception("config err");
        }

        var objOList_relationtype_superordinate = (from objOItem in objDBLevel_Config1.ObjectRels
                                                   where objOItem.ID_Object == cstrID_Ontology
                                                   join objRef in objDBLevel_Config2.ObjectRels on objOItem.ID_Other equals objRef.ID_Object
                                                   where objRef.Name_Object.ToLower() == "relationtype_superordinate".ToLower() && objRef.Ontology == Globals.Type_RelationType
                                                   select objRef).ToList();

        if (objOList_relationtype_superordinate.Any())
        {
            OItem_relationtype_superordinate = new clsOntologyItem()
            {
                GUID = objOList_relationtype_superordinate.First().ID_Other,
                Name = objOList_relationtype_superordinate.First().Name_Other,
                GUID_Parent = objOList_relationtype_superordinate.First().ID_Parent_Other,
                Type = Globals.Type_RelationType
            };
        }
        else
        {
            throw new Exception("config err");
        }

        var objOList_relationtype_user_message = (from objOItem in objDBLevel_Config1.ObjectRels
                                                  where objOItem.ID_Object == cstrID_Ontology
                                                  join objRef in objDBLevel_Config2.ObjectRels on objOItem.ID_Other equals objRef.ID_Object
                                                  where objRef.Name_Object.ToLower() == "relationtype_user_message".ToLower() && objRef.Ontology == Globals.Type_RelationType
                                                  select objRef).ToList();

        if (objOList_relationtype_user_message.Any())
        {
            OItem_relationtype_user_message = new clsOntologyItem()
            {
                GUID = objOList_relationtype_user_message.First().ID_Other,
                Name = objOList_relationtype_user_message.First().Name_Other,
                GUID_Parent = objOList_relationtype_user_message.First().ID_Parent_Other,
                Type = Globals.Type_RelationType
            };
        }
        else
        {
            throw new Exception("config err");
        }

        var objOList_relationtype_value_type = (from objOItem in objDBLevel_Config1.ObjectRels
                                                where objOItem.ID_Object == cstrID_Ontology
                                                join objRef in objDBLevel_Config2.ObjectRels on objOItem.ID_Other equals objRef.ID_Object
                                                where objRef.Name_Object.ToLower() == "relationtype_value_type".ToLower() && objRef.Ontology == Globals.Type_RelationType
                                                select objRef).ToList();

        if (objOList_relationtype_value_type.Any())
        {
            OItem_relationtype_value_type = new clsOntologyItem()
            {
                GUID = objOList_relationtype_value_type.First().ID_Other,
                Name = objOList_relationtype_value_type.First().Name_Other,
                GUID_Parent = objOList_relationtype_value_type.First().ID_Parent_Other,
                Type = Globals.Type_RelationType
            };
        }
        else
        {
            throw new Exception("config err");
        }

        var objOList_relationtype_wascreatedby = (from objOItem in objDBLevel_Config1.ObjectRels
                                                  where objOItem.ID_Object == cstrID_Ontology
                                                  join objRef in objDBLevel_Config2.ObjectRels on objOItem.ID_Other equals objRef.ID_Object
                                                  where objRef.Name_Object.ToLower() == "relationtype_wascreatedby".ToLower() && objRef.Ontology == Globals.Type_RelationType
                                                  select objRef).ToList();

        if (objOList_relationtype_wascreatedby.Any())
        {
            OItem_relationtype_wascreatedby = new clsOntologyItem()
            {
                GUID = objOList_relationtype_wascreatedby.First().ID_Other,
                Name = objOList_relationtype_wascreatedby.First().Name_Other,
                GUID_Parent = objOList_relationtype_wascreatedby.First().ID_Parent_Other,
                Type = Globals.Type_RelationType
            };
        }
        else
        {
            throw new Exception("config err");
        }

        var objOList_relationtype_wasdevelopedby = (from objOItem in objDBLevel_Config1.ObjectRels
                                                    where objOItem.ID_Object == cstrID_Ontology
                                                    join objRef in objDBLevel_Config2.ObjectRels on objOItem.ID_Other equals objRef.ID_Object
                                                    where objRef.Name_Object.ToLower() == "relationtype_wasdevelopedby".ToLower() && objRef.Ontology == Globals.Type_RelationType
                                                    select objRef).ToList();

        if (objOList_relationtype_wasdevelopedby.Any())
        {
            OItem_relationtype_wasdevelopedby = new clsOntologyItem()
            {
                GUID = objOList_relationtype_wasdevelopedby.First().ID_Other,
                Name = objOList_relationtype_wasdevelopedby.First().Name_Other,
                GUID_Parent = objOList_relationtype_wasdevelopedby.First().ID_Parent_Other,
                Type = Globals.Type_RelationType
            };
        }
        else
        {
            throw new Exception("config err");
        }

        var objOList_relationtype_works_off = (from objOItem in objDBLevel_Config1.ObjectRels
                                               where objOItem.ID_Object == cstrID_Ontology
                                               join objRef in objDBLevel_Config2.ObjectRels on objOItem.ID_Other equals objRef.ID_Object
                                               where objRef.Name_Object.ToLower() == "relationtype_works_off".ToLower() && objRef.Ontology == Globals.Type_RelationType
                                               select objRef).ToList();

        if (objOList_relationtype_works_off.Any())
        {
            OItem_relationtype_works_off = new clsOntologyItem()
            {
                GUID = objOList_relationtype_works_off.First().ID_Other,
                Name = objOList_relationtype_works_off.First().Name_Other,
                GUID_Parent = objOList_relationtype_works_off.First().ID_Parent_Other,
                Type = Globals.Type_RelationType
            };
        }
        else
        {
            throw new Exception("config err");
        }


    }

    private void get_Config_Objects()
    {
        var objOList_development_management = (from objOItem in objDBLevel_Config1.ObjectRels
                                               where objOItem.ID_Object == cstrID_Ontology
                                               join objRef in objDBLevel_Config2.ObjectRels on objOItem.ID_Other equals objRef.ID_Object
                                               where objRef.Name_Object.ToLower() == "development_management".ToLower() && objRef.Ontology == Globals.Type_Object
                                               select objRef).ToList();

        if (objOList_development_management.Any())
        {
            OItem_development_management = new clsOntologyItem()
            {
                GUID = objOList_development_management.First().ID_Other,
                Name = objOList_development_management.First().Name_Other,
                GUID_Parent = objOList_development_management.First().ID_Parent_Other,
                Type = Globals.Type_Object
            };
        }
        else
        {
            throw new Exception("config err");
        }

        var objOList_object____ = (from objOItem in objDBLevel_Config1.ObjectRels
                                   where objOItem.ID_Object == cstrID_Ontology
                                   join objRef in objDBLevel_Config2.ObjectRels on objOItem.ID_Other equals objRef.ID_Object
                                   where objRef.Name_Object.ToLower() == "object____".ToLower() && objRef.Ontology == Globals.Type_Object
                                   select objRef).ToList();

        if (objOList_object____.Any())
        {
            OItem_object____ = new clsOntologyItem()
            {
                GUID = objOList_object____.First().ID_Other,
                Name = objOList_object____.First().Name_Other,
                GUID_Parent = objOList_object____.First().ID_Parent_Other,
                Type = Globals.Type_Object
            };
        }
        else
        {
            throw new Exception("config err");
        }

        var objOList_object__assembly__assemblyversion___ = (from objOItem in objDBLevel_Config1.ObjectRels
                                                             where objOItem.ID_Object == cstrID_Ontology
                                                             join objRef in objDBLevel_Config2.ObjectRels on objOItem.ID_Other equals objRef.ID_Object
                                                             where objRef.Name_Object.ToLower() == "object__assembly__assemblyversion___".ToLower() && objRef.Ontology == Globals.Type_Object
                                                             select objRef).ToList();

        if (objOList_object__assembly__assemblyversion___.Any())
        {
            OItem_object__assembly__assemblyversion___ = new clsOntologyItem()
            {
                GUID = objOList_object__assembly__assemblyversion___.First().ID_Other,
                Name = objOList_object__assembly__assemblyversion___.First().Name_Other,
                GUID_Parent = objOList_object__assembly__assemblyversion___.First().ID_Parent_Other,
                Type = Globals.Type_Object
            };
        }
        else
        {
            throw new Exception("config err");
        }

        var objOList_object__point = (from objOItem in objDBLevel_Config1.ObjectRels
                                      where objOItem.ID_Object == cstrID_Ontology
                                      join objRef in objDBLevel_Config2.ObjectRels on objOItem.ID_Other equals objRef.ID_Object
                                      where objRef.Name_Object.ToLower() == "object__point".ToLower() && objRef.Ontology == Globals.Type_Object
                                      select objRef).ToList();

        if (objOList_object__point.Any())
        {
            OItem_object__point = new clsOntologyItem()
            {
                GUID = objOList_object__point.First().ID_Other,
                Name = objOList_object__point.First().Name_Other,
                GUID_Parent = objOList_object__point.First().ID_Parent_Other,
                Type = Globals.Type_Object
            };
        }
        else
        {
            throw new Exception("config err");
        }

        var objOList_object_assembly_version = (from objOItem in objDBLevel_Config1.ObjectRels
                                                where objOItem.ID_Object == cstrID_Ontology
                                                join objRef in objDBLevel_Config2.ObjectRels on objOItem.ID_Other equals objRef.ID_Object
                                                where objRef.Name_Object.ToLower() == "object_assembly_version".ToLower() && objRef.Ontology == Globals.Type_Object
                                                select objRef).ToList();

        if (objOList_object_assembly_version.Any())
        {
            OItem_object_assembly_version = new clsOntologyItem()
            {
                GUID = objOList_object_assembly_version.First().ID_Other,
                Name = objOList_object_assembly_version.First().Name_Other,
                GUID_Parent = objOList_object_assembly_version.First().ID_Parent_Other,
                Type = Globals.Type_Object
            };
        }
        else
        {
            throw new Exception("config err");
        }

        var objOList_object_c_ = (from objOItem in objDBLevel_Config1.ObjectRels
                                  where objOItem.ID_Object == cstrID_Ontology
                                  join objRef in objDBLevel_Config2.ObjectRels on objOItem.ID_Other equals objRef.ID_Object
                                  where objRef.Name_Object.ToLower() == "object_c_".ToLower() && objRef.Ontology == Globals.Type_Object
                                  select objRef).ToList();

        if (objOList_object_c_.Any())
        {
            OItem_object_c_ = new clsOntologyItem()
            {
                GUID = objOList_object_c_.First().ID_Other,
                Name = objOList_object_c_.First().Name_Other,
                GUID_Parent = objOList_object_c_.First().ID_Parent_Other,
                Type = Globals.Type_Object
            };
        }
        else
        {
            throw new Exception("config err");
        }

        var objOList_object_string = (from objOItem in objDBLevel_Config1.ObjectRels
                                      where objOItem.ID_Object == cstrID_Ontology
                                      join objRef in objDBLevel_Config2.ObjectRels on objOItem.ID_Other equals objRef.ID_Object
                                      where objRef.Name_Object.ToLower() == "object_string".ToLower() && objRef.Ontology == Globals.Type_Object
                                      select objRef).ToList();

        if (objOList_object_string.Any())
        {
            OItem_object_string = new clsOntologyItem()
            {
                GUID = objOList_object_string.First().ID_Other,
                Name = objOList_object_string.First().Name_Other,
                GUID_Parent = objOList_object_string.First().ID_Parent_Other,
                Type = Globals.Type_Object
            };
        }
        else
        {
            throw new Exception("config err");
        }

        var objOList_object_vb_net = (from objOItem in objDBLevel_Config1.ObjectRels
                                      where objOItem.ID_Object == cstrID_Ontology
                                      join objRef in objDBLevel_Config2.ObjectRels on objOItem.ID_Other equals objRef.ID_Object
                                      where objRef.Name_Object.ToLower() == "object_vb_net".ToLower() && objRef.Ontology == Globals.Type_Object
                                      select objRef).ToList();

        if (objOList_object_vb_net.Any())
        {
            OItem_object_vb_net = new clsOntologyItem()
            {
                GUID = objOList_object_vb_net.First().ID_Other,
                Name = objOList_object_vb_net.First().Name_Other,
                GUID_Parent = objOList_object_vb_net.First().ID_Parent_Other,
                Type = Globals.Type_Object
            };
        }
        else
        {
            throw new Exception("config err");
        }

        var objOList_object_version = (from objOItem in objDBLevel_Config1.ObjectRels
                                       where objOItem.ID_Object == cstrID_Ontology
                                       join objRef in objDBLevel_Config2.ObjectRels on objOItem.ID_Other equals objRef.ID_Object
                                       where objRef.Name_Object.ToLower() == "object_version".ToLower() && objRef.Ontology == Globals.Type_Object
                                       select objRef).ToList();

        if (objOList_object_version.Any())
        {
            OItem_object_version = new clsOntologyItem()
            {
                GUID = objOList_object_version.First().ID_Other,
                Name = objOList_object_version.First().Name_Other,
                GUID_Parent = objOList_object_version.First().ID_Parent_Other,
                Type = Globals.Type_Object
            };
        }
        else
        {
            throw new Exception("config err");
        }

        var objOList_object_visual_studio_version_parser = (from objOItem in objDBLevel_Config1.ObjectRels
                                                            where objOItem.ID_Object == cstrID_Ontology
                                                            join objRef in objDBLevel_Config2.ObjectRels on objOItem.ID_Other equals objRef.ID_Object
                                                            where objRef.Name_Object.ToLower() == "object_visual_studio_version_parser".ToLower() && objRef.Ontology == Globals.Type_Object
                                                            select objRef).ToList();

        if (objOList_object_visual_studio_version_parser.Any())
        {
            OItem_object_visual_studio_version_parser = new clsOntologyItem()
            {
                GUID = objOList_object_visual_studio_version_parser.First().ID_Other,
                Name = objOList_object_visual_studio_version_parser.First().Name_Other,
                GUID_Parent = objOList_object_visual_studio_version_parser.First().ID_Parent_Other,
                Type = Globals.Type_Object
            };
        }
        else
        {
            throw new Exception("config err");
        }

        var objOList_token_clslocalconfig_xml_xml = (from objOItem in objDBLevel_Config1.ObjectRels
                                                     where objOItem.ID_Object == cstrID_Ontology
                                                     join objRef in objDBLevel_Config2.ObjectRels on objOItem.ID_Other equals objRef.ID_Object
                                                     where objRef.Name_Object.ToLower() == "token_clslocalconfig_xml_xml".ToLower() && objRef.Ontology == Globals.Type_Object
                                                     select objRef).ToList();

        if (objOList_token_clslocalconfig_xml_xml.Any())
        {
            OItem_token_clslocalconfig_xml_xml = new clsOntologyItem()
            {
                GUID = objOList_token_clslocalconfig_xml_xml.First().ID_Other,
                Name = objOList_token_clslocalconfig_xml_xml.First().Name_Other,
                GUID_Parent = objOList_token_clslocalconfig_xml_xml.First().ID_Parent_Other,
                Type = Globals.Type_Object
            };
        }
        else
        {
            throw new Exception("config err");
        }

        var objOList_token_db_schema_type_config = (from objOItem in objDBLevel_Config1.ObjectRels
                                                    where objOItem.ID_Object == cstrID_Ontology
                                                    join objRef in objDBLevel_Config2.ObjectRels on objOItem.ID_Other equals objRef.ID_Object
                                                    where objRef.Name_Object.ToLower() == "token_db_schema_type_config".ToLower() && objRef.Ontology == Globals.Type_Object
                                                    select objRef).ToList();

        if (objOList_token_db_schema_type_config.Any())
        {
            OItem_token_db_schema_type_config = new clsOntologyItem()
            {
                GUID = objOList_token_db_schema_type_config.First().ID_Other,
                Name = objOList_token_db_schema_type_config.First().Name_Other,
                GUID_Parent = objOList_token_db_schema_type_config.First().ID_Parent_Other,
                Type = Globals.Type_Object
            };
        }
        else
        {
            throw new Exception("config err");
        }

        var objOList_token_db_schema_type_module = (from objOItem in objDBLevel_Config1.ObjectRels
                                                    where objOItem.ID_Object == cstrID_Ontology
                                                    join objRef in objDBLevel_Config2.ObjectRels on objOItem.ID_Other equals objRef.ID_Object
                                                    where objRef.Name_Object.ToLower() == "token_db_schema_type_module".ToLower() && objRef.Ontology == Globals.Type_Object
                                                    select objRef).ToList();

        if (objOList_token_db_schema_type_module.Any())
        {
            OItem_token_db_schema_type_module = new clsOntologyItem()
            {
                GUID = objOList_token_db_schema_type_module.First().ID_Other,
                Name = objOList_token_db_schema_type_module.First().Name_Other,
                GUID_Parent = objOList_token_db_schema_type_module.First().ID_Parent_Other,
                Type = Globals.Type_Object
            };
        }
        else
        {
            throw new Exception("config err");
        }

        var objOList_token_db_schema_type_user = (from objOItem in objDBLevel_Config1.ObjectRels
                                                  where objOItem.ID_Object == cstrID_Ontology
                                                  join objRef in objDBLevel_Config2.ObjectRels on objOItem.ID_Other equals objRef.ID_Object
                                                  where objRef.Name_Object.ToLower() == "token_db_schema_type_user".ToLower() && objRef.Ontology == Globals.Type_Object
                                                  select objRef).ToList();

        if (objOList_token_db_schema_type_user.Any())
        {
            OItem_token_db_schema_type_user = new clsOntologyItem()
            {
                GUID = objOList_token_db_schema_type_user.First().ID_Other,
                Name = objOList_token_db_schema_type_user.First().Name_Other,
                GUID_Parent = objOList_token_db_schema_type_user.First().ID_Parent_Other,
                Type = Globals.Type_Object
            };
        }
        else
        {
            throw new Exception("config err");
        }

        var objOList_token_declaration_configitems_xml = (from objOItem in objDBLevel_Config1.ObjectRels
                                                          where objOItem.ID_Object == cstrID_Ontology
                                                          join objRef in objDBLevel_Config2.ObjectRels on objOItem.ID_Other equals objRef.ID_Object
                                                          where objRef.Name_Object.ToLower() == "token_declaration_configitems_xml".ToLower() && objRef.Ontology == Globals.Type_Object
                                                          select objRef).ToList();

        if (objOList_token_declaration_configitems_xml.Any())
        {
            OItem_token_declaration_configitems_xml = new clsOntologyItem()
            {
                GUID = objOList_token_declaration_configitems_xml.First().ID_Other,
                Name = objOList_token_declaration_configitems_xml.First().Name_Other,
                GUID_Parent = objOList_token_declaration_configitems_xml.First().ID_Parent_Other,
                Type = Globals.Type_Object
            };
        }
        else
        {
            throw new Exception("config err");
        }

        var objOList_token_dev_structure_database = (from objOItem in objDBLevel_Config1.ObjectRels
                                                     where objOItem.ID_Object == cstrID_Ontology
                                                     join objRef in objDBLevel_Config2.ObjectRels on objOItem.ID_Other equals objRef.ID_Object
                                                     where objRef.Name_Object.ToLower() == "token_dev_structure_database".ToLower() && objRef.Ontology == Globals.Type_Object
                                                     select objRef).ToList();

        if (objOList_token_dev_structure_database.Any())
        {
            OItem_token_dev_structure_database = new clsOntologyItem()
            {
                GUID = objOList_token_dev_structure_database.First().ID_Other,
                Name = objOList_token_dev_structure_database.First().Name_Other,
                GUID_Parent = objOList_token_dev_structure_database.First().ID_Parent_Other,
                Type = Globals.Type_Object
            };
        }
        else
        {
            throw new Exception("config err");
        }

        var objOList_token_dev_structure_return = (from objOItem in objDBLevel_Config1.ObjectRels
                                                   where objOItem.ID_Object == cstrID_Ontology
                                                   join objRef in objDBLevel_Config2.ObjectRels on objOItem.ID_Other equals objRef.ID_Object
                                                   where objRef.Name_Object.ToLower() == "token_dev_structure_return".ToLower() && objRef.Ontology == Globals.Type_Object
                                                   select objRef).ToList();

        if (objOList_token_dev_structure_return.Any())
        {
            OItem_token_dev_structure_return = new clsOntologyItem()
            {
                GUID = objOList_token_dev_structure_return.First().ID_Other,
                Name = objOList_token_dev_structure_return.First().Name_Other,
                GUID_Parent = objOList_token_dev_structure_return.First().ID_Parent_Other,
                Type = Globals.Type_Object
            };
        }
        else
        {
            throw new Exception("config err");
        }

        var objOList_token_dev_structure_schleife = (from objOItem in objDBLevel_Config1.ObjectRels
                                                     where objOItem.ID_Object == cstrID_Ontology
                                                     join objRef in objDBLevel_Config2.ObjectRels on objOItem.ID_Other equals objRef.ID_Object
                                                     where objRef.Name_Object.ToLower() == "token_dev_structure_schleife".ToLower() && objRef.Ontology == Globals.Type_Object
                                                     select objRef).ToList();

        if (objOList_token_dev_structure_schleife.Any())
        {
            OItem_token_dev_structure_schleife = new clsOntologyItem()
            {
                GUID = objOList_token_dev_structure_schleife.First().ID_Other,
                Name = objOList_token_dev_structure_schleife.First().Name_Other,
                GUID_Parent = objOList_token_dev_structure_schleife.First().ID_Parent_Other,
                Type = Globals.Type_Object
            };
        }
        else
        {
            throw new Exception("config err");
        }

        var objOList_token_directions_in = (from objOItem in objDBLevel_Config1.ObjectRels
                                            where objOItem.ID_Object == cstrID_Ontology
                                            join objRef in objDBLevel_Config2.ObjectRels on objOItem.ID_Other equals objRef.ID_Object
                                            where objRef.Name_Object.ToLower() == "token_directions_in".ToLower() && objRef.Ontology == Globals.Type_Object
                                            select objRef).ToList();

        if (objOList_token_directions_in.Any())
        {
            OItem_token_directions_in = new clsOntologyItem()
            {
                GUID = objOList_token_directions_in.First().ID_Other,
                Name = objOList_token_directions_in.First().Name_Other,
                GUID_Parent = objOList_token_directions_in.First().ID_Parent_Other,
                Type = Globals.Type_Object
            };
        }
        else
        {
            throw new Exception("config err");
        }

        var objOList_token_directions_out = (from objOItem in objDBLevel_Config1.ObjectRels
                                             where objOItem.ID_Object == cstrID_Ontology
                                             join objRef in objDBLevel_Config2.ObjectRels on objOItem.ID_Other equals objRef.ID_Object
                                             where objRef.Name_Object.ToLower() == "token_directions_out".ToLower() && objRef.Ontology == Globals.Type_Object
                                             select objRef).ToList();

        if (objOList_token_directions_out.Any())
        {
            OItem_token_directions_out = new clsOntologyItem()
            {
                GUID = objOList_token_directions_out.First().ID_Other,
                Name = objOList_token_directions_out.First().Name_Other,
                GUID_Parent = objOList_token_directions_out.First().ID_Parent_Other,
                Type = Globals.Type_Object
            };
        }
        else
        {
            throw new Exception("config err");
        }

        var objOList_token_export_mode_deny_item_with_children__type___token_ = (from objOItem in objDBLevel_Config1.ObjectRels
                                                                                 where objOItem.ID_Object == cstrID_Ontology
                                                                                 join objRef in objDBLevel_Config2.ObjectRels on objOItem.ID_Other equals objRef.ID_Object
                                                                                 where objRef.Name_Object.ToLower() == "token_export_mode_deny_item_with_children__type___token_".ToLower() && objRef.Ontology == Globals.Type_Object
                                                                                 select objRef).ToList();

        if (objOList_token_export_mode_deny_item_with_children__type___token_.Any())
        {
            OItem_token_export_mode_deny_item_with_children__type___token_ = new clsOntologyItem()
            {
                GUID = objOList_token_export_mode_deny_item_with_children__type___token_.First().ID_Other,
                Name = objOList_token_export_mode_deny_item_with_children__type___token_.First().Name_Other,
                GUID_Parent = objOList_token_export_mode_deny_item_with_children__type___token_.First().ID_Parent_Other,
                Type = Globals.Type_Object
            };
        }
        else
        {
            throw new Exception("config err");
        }

        var objOList_token_export_mode_deny_only_children__token_of_type_ = (from objOItem in objDBLevel_Config1.ObjectRels
                                                                             where objOItem.ID_Object == cstrID_Ontology
                                                                             join objRef in objDBLevel_Config2.ObjectRels on objOItem.ID_Other equals objRef.ID_Object
                                                                             where objRef.Name_Object.ToLower() == "token_export_mode_deny_only_children__token_of_type_".ToLower() && objRef.Ontology == Globals.Type_Object
                                                                             select objRef).ToList();

        if (objOList_token_export_mode_deny_only_children__token_of_type_.Any())
        {
            OItem_token_export_mode_deny_only_children__token_of_type_ = new clsOntologyItem()
            {
                GUID = objOList_token_export_mode_deny_only_children__token_of_type_.First().ID_Other,
                Name = objOList_token_export_mode_deny_only_children__token_of_type_.First().Name_Other,
                GUID_Parent = objOList_token_export_mode_deny_only_children__token_of_type_.First().ID_Parent_Other,
                Type = Globals.Type_Object
            };
        }
        else
        {
            throw new Exception("config err");
        }

        var objOList_token_export_mode_grant_children_of_item__token_of_type_ = (from objOItem in objDBLevel_Config1.ObjectRels
                                                                                 where objOItem.ID_Object == cstrID_Ontology
                                                                                 join objRef in objDBLevel_Config2.ObjectRels on objOItem.ID_Other equals objRef.ID_Object
                                                                                 where objRef.Name_Object.ToLower() == "token_export_mode_grant_children_of_item__token_of_type_".ToLower() && objRef.Ontology == Globals.Type_Object
                                                                                 select objRef).ToList();

        if (objOList_token_export_mode_grant_children_of_item__token_of_type_.Any())
        {
            OItem_token_export_mode_grant_children_of_item__token_of_type_ = new clsOntologyItem()
            {
                GUID = objOList_token_export_mode_grant_children_of_item__token_of_type_.First().ID_Other,
                Name = objOList_token_export_mode_grant_children_of_item__token_of_type_.First().Name_Other,
                GUID_Parent = objOList_token_export_mode_grant_children_of_item__token_of_type_.First().ID_Parent_Other,
                Type = Globals.Type_Object
            };
        }
        else
        {
            throw new Exception("config err");
        }

        var objOList_token_export_mode_normal = (from objOItem in objDBLevel_Config1.ObjectRels
                                                 where objOItem.ID_Object == cstrID_Ontology
                                                 join objRef in objDBLevel_Config2.ObjectRels on objOItem.ID_Other equals objRef.ID_Object
                                                 where objRef.Name_Object.ToLower() == "token_export_mode_normal".ToLower() && objRef.Ontology == Globals.Type_Object
                                                 select objRef).ToList();

        if (objOList_token_export_mode_normal.Any())
        {
            OItem_token_export_mode_normal = new clsOntologyItem()
            {
                GUID = objOList_token_export_mode_normal.First().ID_Other,
                Name = objOList_token_export_mode_normal.First().Name_Other,
                GUID_Parent = objOList_token_export_mode_normal.First().ID_Parent_Other,
                Type = Globals.Type_Object
            };
        }
        else
        {
            throw new Exception("config err");
        }

        var objOList_token_initialize_relationtype__configitem__xml = (from objOItem in objDBLevel_Config1.ObjectRels
                                                                       where objOItem.ID_Object == cstrID_Ontology
                                                                       join objRef in objDBLevel_Config2.ObjectRels on objOItem.ID_Other equals objRef.ID_Object
                                                                       where objRef.Name_Object.ToLower() == "token_initialize_relationtype__configitem__xml".ToLower() && objRef.Ontology == Globals.Type_Object
                                                                       select objRef).ToList();

        if (objOList_token_initialize_relationtype__configitem__xml.Any())
        {
            OItem_token_initialize_relationtype__configitem__xml = new clsOntologyItem()
            {
                GUID = objOList_token_initialize_relationtype__configitem__xml.First().ID_Other,
                Name = objOList_token_initialize_relationtype__configitem__xml.First().Name_Other,
                GUID_Parent = objOList_token_initialize_relationtype__configitem__xml.First().ID_Parent_Other,
                Type = Globals.Type_Object
            };
        }
        else
        {
            throw new Exception("config err");
        }

        var objOList_token_initialize_token__configitem__xml = (from objOItem in objDBLevel_Config1.ObjectRels
                                                                where objOItem.ID_Object == cstrID_Ontology
                                                                join objRef in objDBLevel_Config2.ObjectRels on objOItem.ID_Other equals objRef.ID_Object
                                                                where objRef.Name_Object.ToLower() == "token_initialize_token__configitem__xml".ToLower() && objRef.Ontology == Globals.Type_Object
                                                                select objRef).ToList();

        if (objOList_token_initialize_token__configitem__xml.Any())
        {
            OItem_token_initialize_token__configitem__xml = new clsOntologyItem()
            {
                GUID = objOList_token_initialize_token__configitem__xml.First().ID_Other,
                Name = objOList_token_initialize_token__configitem__xml.First().Name_Other,
                GUID_Parent = objOList_token_initialize_token__configitem__xml.First().ID_Parent_Other,
                Type = Globals.Type_Object
            };
        }
        else
        {
            throw new Exception("config err");
        }

        var objOList_token_initilize_attribute__configitem__xml = (from objOItem in objDBLevel_Config1.ObjectRels
                                                                   where objOItem.ID_Object == cstrID_Ontology
                                                                   join objRef in objDBLevel_Config2.ObjectRels on objOItem.ID_Other equals objRef.ID_Object
                                                                   where objRef.Name_Object.ToLower() == "token_initilize_attribute__configitem__xml".ToLower() && objRef.Ontology == Globals.Type_Object
                                                                   select objRef).ToList();

        if (objOList_token_initilize_attribute__configitem__xml.Any())
        {
            OItem_token_initilize_attribute__configitem__xml = new clsOntologyItem()
            {
                GUID = objOList_token_initilize_attribute__configitem__xml.First().ID_Other,
                Name = objOList_token_initilize_attribute__configitem__xml.First().Name_Other,
                GUID_Parent = objOList_token_initilize_attribute__configitem__xml.First().ID_Parent_Other,
                Type = Globals.Type_Object
            };
        }
        else
        {
            throw new Exception("config err");
        }

        var objOList_token_initilize_type__configitem__xml = (from objOItem in objDBLevel_Config1.ObjectRels
                                                              where objOItem.ID_Object == cstrID_Ontology
                                                              join objRef in objDBLevel_Config2.ObjectRels on objOItem.ID_Other equals objRef.ID_Object
                                                              where objRef.Name_Object.ToLower() == "token_initilize_type__configitem__xml".ToLower() && objRef.Ontology == Globals.Type_Object
                                                              select objRef).ToList();

        if (objOList_token_initilize_type__configitem__xml.Any())
        {
            OItem_token_initilize_type__configitem__xml = new clsOntologyItem()
            {
                GUID = objOList_token_initilize_type__configitem__xml.First().ID_Other,
                Name = objOList_token_initilize_type__configitem__xml.First().Name_Other,
                GUID_Parent = objOList_token_initilize_type__configitem__xml.First().ID_Parent_Other,
                Type = Globals.Type_Object
            };
        }
        else
        {
            throw new Exception("config err");
        }

        var objOList_token_logstate_active = (from objOItem in objDBLevel_Config1.ObjectRels
                                              where objOItem.ID_Object == cstrID_Ontology
                                              join objRef in objDBLevel_Config2.ObjectRels on objOItem.ID_Other equals objRef.ID_Object
                                              where objRef.Name_Object.ToLower() == "token_logstate_active".ToLower() && objRef.Ontology == Globals.Type_Object
                                              select objRef).ToList();

        if (objOList_token_logstate_active.Any())
        {
            OItem_token_logstate_active = new clsOntologyItem()
            {
                GUID = objOList_token_logstate_active.First().ID_Other,
                Name = objOList_token_logstate_active.First().Name_Other,
                GUID_Parent = objOList_token_logstate_active.First().ID_Parent_Other,
                Type = Globals.Type_Object
            };
        }
        else
        {
            throw new Exception("config err");
        }

        var objOList_token_logstate_changed = (from objOItem in objDBLevel_Config1.ObjectRels
                                               where objOItem.ID_Object == cstrID_Ontology
                                               join objRef in objDBLevel_Config2.ObjectRels on objOItem.ID_Other equals objRef.ID_Object
                                               where objRef.Name_Object.ToLower() == "token_logstate_changed".ToLower() && objRef.Ontology == Globals.Type_Object
                                               select objRef).ToList();

        if (objOList_token_logstate_changed.Any())
        {
            OItem_token_logstate_changed = new clsOntologyItem()
            {
                GUID = objOList_token_logstate_changed.First().ID_Other,
                Name = objOList_token_logstate_changed.First().Name_Other,
                GUID_Parent = objOList_token_logstate_changed.First().ID_Parent_Other,
                Type = Globals.Type_Object
            };
        }
        else
        {
            throw new Exception("config err");
        }

        var objOList_token_logstate_configitemadded = (from objOItem in objDBLevel_Config1.ObjectRels
                                                       where objOItem.ID_Object == cstrID_Ontology
                                                       join objRef in objDBLevel_Config2.ObjectRels on objOItem.ID_Other equals objRef.ID_Object
                                                       where objRef.Name_Object.ToLower() == "token_logstate_configitemadded".ToLower() && objRef.Ontology == Globals.Type_Object
                                                       select objRef).ToList();

        if (objOList_token_logstate_configitemadded.Any())
        {
            OItem_token_logstate_configitemadded = new clsOntologyItem()
            {
                GUID = objOList_token_logstate_configitemadded.First().ID_Other,
                Name = objOList_token_logstate_configitemadded.First().Name_Other,
                GUID_Parent = objOList_token_logstate_configitemadded.First().ID_Parent_Other,
                Type = Globals.Type_Object
            };
        }
        else
        {
            throw new Exception("config err");
        }

        var objOList_token_logstate_create = (from objOItem in objDBLevel_Config1.ObjectRels
                                              where objOItem.ID_Object == cstrID_Ontology
                                              join objRef in objDBLevel_Config2.ObjectRels on objOItem.ID_Other equals objRef.ID_Object
                                              where objRef.Name_Object.ToLower() == "token_logstate_create".ToLower() && objRef.Ontology == Globals.Type_Object
                                              select objRef).ToList();

        if (objOList_token_logstate_create.Any())
        {
            OItem_token_logstate_create = new clsOntologyItem()
            {
                GUID = objOList_token_logstate_create.First().ID_Other,
                Name = objOList_token_logstate_create.First().Name_Other,
                GUID_Parent = objOList_token_logstate_create.First().ID_Parent_Other,
                Type = Globals.Type_Object
            };
        }
        else
        {
            throw new Exception("config err");
        }

        var objOList_token_logstate_inactive = (from objOItem in objDBLevel_Config1.ObjectRels
                                                where objOItem.ID_Object == cstrID_Ontology
                                                join objRef in objDBLevel_Config2.ObjectRels on objOItem.ID_Other equals objRef.ID_Object
                                                where objRef.Name_Object.ToLower() == "token_logstate_inactive".ToLower() && objRef.Ontology == Globals.Type_Object
                                                select objRef).ToList();

        if (objOList_token_logstate_inactive.Any())
        {
            OItem_token_logstate_inactive = new clsOntologyItem()
            {
                GUID = objOList_token_logstate_inactive.First().ID_Other,
                Name = objOList_token_logstate_inactive.First().Name_Other,
                GUID_Parent = objOList_token_logstate_inactive.First().ID_Parent_Other,
                Type = Globals.Type_Object
            };
        }
        else
        {
            throw new Exception("config err");
        }

        var objOList_token_logstate_information = (from objOItem in objDBLevel_Config1.ObjectRels
                                                   where objOItem.ID_Object == cstrID_Ontology
                                                   join objRef in objDBLevel_Config2.ObjectRels on objOItem.ID_Other equals objRef.ID_Object
                                                   where objRef.Name_Object.ToLower() == "token_logstate_information".ToLower() && objRef.Ontology == Globals.Type_Object
                                                   select objRef).ToList();

        if (objOList_token_logstate_information.Any())
        {
            OItem_token_logstate_information = new clsOntologyItem()
            {
                GUID = objOList_token_logstate_information.First().ID_Other,
                Name = objOList_token_logstate_information.First().Name_Other,
                GUID_Parent = objOList_token_logstate_information.First().ID_Parent_Other,
                Type = Globals.Type_Object
            };
        }
        else
        {
            throw new Exception("config err");
        }

        var objOList_token_logstate_obsolete = (from objOItem in objDBLevel_Config1.ObjectRels
                                                where objOItem.ID_Object == cstrID_Ontology
                                                join objRef in objDBLevel_Config2.ObjectRels on objOItem.ID_Other equals objRef.ID_Object
                                                where objRef.Name_Object.ToLower() == "token_logstate_obsolete".ToLower() && objRef.Ontology == Globals.Type_Object
                                                select objRef).ToList();

        if (objOList_token_logstate_obsolete.Any())
        {
            OItem_token_logstate_obsolete = new clsOntologyItem()
            {
                GUID = objOList_token_logstate_obsolete.First().ID_Other,
                Name = objOList_token_logstate_obsolete.First().Name_Other,
                GUID_Parent = objOList_token_logstate_obsolete.First().ID_Parent_Other,
                Type = Globals.Type_Object
            };
        }
        else
        {
            throw new Exception("config err");
        }

        var objOList_token_logstate_open = (from objOItem in objDBLevel_Config1.ObjectRels
                                            where objOItem.ID_Object == cstrID_Ontology
                                            join objRef in objDBLevel_Config2.ObjectRels on objOItem.ID_Other equals objRef.ID_Object
                                            where objRef.Name_Object.ToLower() == "token_logstate_open".ToLower() && objRef.Ontology == Globals.Type_Object
                                            select objRef).ToList();

        if (objOList_token_logstate_open.Any())
        {
            OItem_token_logstate_open = new clsOntologyItem()
            {
                GUID = objOList_token_logstate_open.First().ID_Other,
                Name = objOList_token_logstate_open.First().Name_Other,
                GUID_Parent = objOList_token_logstate_open.First().ID_Parent_Other,
                Type = Globals.Type_Object
            };
        }
        else
        {
            throw new Exception("config err");
        }

        var objOList_token_logstate_request = (from objOItem in objDBLevel_Config1.ObjectRels
                                               where objOItem.ID_Object == cstrID_Ontology
                                               join objRef in objDBLevel_Config2.ObjectRels on objOItem.ID_Other equals objRef.ID_Object
                                               where objRef.Name_Object.ToLower() == "token_logstate_request".ToLower() && objRef.Ontology == Globals.Type_Object
                                               select objRef).ToList();

        if (objOList_token_logstate_request.Any())
        {
            OItem_token_logstate_request = new clsOntologyItem()
            {
                GUID = objOList_token_logstate_request.First().ID_Other,
                Name = objOList_token_logstate_request.First().Name_Other,
                GUID_Parent = objOList_token_logstate_request.First().ID_Parent_Other,
                Type = Globals.Type_Object
            };
        }
        else
        {
            throw new Exception("config err");
        }

        var objOList_token_logstate_versionchanged = (from objOItem in objDBLevel_Config1.ObjectRels
                                                      where objOItem.ID_Object == cstrID_Ontology
                                                      join objRef in objDBLevel_Config2.ObjectRels on objOItem.ID_Other equals objRef.ID_Object
                                                      where objRef.Name_Object.ToLower() == "token_logstate_versionchanged".ToLower() && objRef.Ontology == Globals.Type_Object
                                                      select objRef).ToList();

        if (objOList_token_logstate_versionchanged.Any())
        {
            OItem_token_logstate_versionchanged = new clsOntologyItem()
            {
                GUID = objOList_token_logstate_versionchanged.First().ID_Other,
                Name = objOList_token_logstate_versionchanged.First().Name_Other,
                GUID_Parent = objOList_token_logstate_versionchanged.First().ID_Parent_Other,
                Type = Globals.Type_Object
            };
        }
        else
        {
            throw new Exception("config err");
        }

        var objOList_token_module_development_management = (from objOItem in objDBLevel_Config1.ObjectRels
                                                            where objOItem.ID_Object == cstrID_Ontology
                                                            join objRef in objDBLevel_Config2.ObjectRels on objOItem.ID_Other equals objRef.ID_Object
                                                            where objRef.Name_Object.ToLower() == "token_module_development_management".ToLower() && objRef.Ontology == Globals.Type_Object
                                                            select objRef).ToList();

        if (objOList_token_module_development_management.Any())
        {
            OItem_token_module_development_management = new clsOntologyItem()
            {
                GUID = objOList_token_module_development_management.First().ID_Other,
                Name = objOList_token_module_development_management.First().Name_Other,
                GUID_Parent = objOList_token_module_development_management.First().ID_Parent_Other,
                Type = Globals.Type_Object
            };
        }
        else
        {
            throw new Exception("config err");
        }

        var objOList_token_possiblestates_developmentstandard = (from objOItem in objDBLevel_Config1.ObjectRels
                                                                 where objOItem.ID_Object == cstrID_Ontology
                                                                 join objRef in objDBLevel_Config2.ObjectRels on objOItem.ID_Other equals objRef.ID_Object
                                                                 where objRef.Name_Object.ToLower() == "token_possiblestates_developmentstandard".ToLower() && objRef.Ontology == Globals.Type_Object
                                                                 select objRef).ToList();

        if (objOList_token_possiblestates_developmentstandard.Any())
        {
            OItem_token_possiblestates_developmentstandard = new clsOntologyItem()
            {
                GUID = objOList_token_possiblestates_developmentstandard.First().ID_Other,
                Name = objOList_token_possiblestates_developmentstandard.First().Name_Other,
                GUID_Parent = objOList_token_possiblestates_developmentstandard.First().ID_Parent_Other,
                Type = Globals.Type_Object
            };
        }
        else
        {
            throw new Exception("config err");
        }

        var objOList_token_property_configitem_xml = (from objOItem in objDBLevel_Config1.ObjectRels
                                                      where objOItem.ID_Object == cstrID_Ontology
                                                      join objRef in objDBLevel_Config2.ObjectRels on objOItem.ID_Other equals objRef.ID_Object
                                                      where objRef.Name_Object.ToLower() == "token_property_configitem_xml".ToLower() && objRef.Ontology == Globals.Type_Object
                                                      select objRef).ToList();

        if (objOList_token_property_configitem_xml.Any())
        {
            OItem_token_property_configitem_xml = new clsOntologyItem()
            {
                GUID = objOList_token_property_configitem_xml.First().ID_Other,
                Name = objOList_token_property_configitem_xml.First().Name_Other,
                GUID_Parent = objOList_token_property_configitem_xml.First().ID_Parent_Other,
                Type = Globals.Type_Object
            };
        }
        else
        {
            throw new Exception("config err");
        }

        var objOList_token_variable_list_declaration_configitems = (from objOItem in objDBLevel_Config1.ObjectRels
                                                                    where objOItem.ID_Object == cstrID_Ontology
                                                                    join objRef in objDBLevel_Config2.ObjectRels on objOItem.ID_Other equals objRef.ID_Object
                                                                    where objRef.Name_Object.ToLower() == "token_variable_list_declaration_configitems".ToLower() && objRef.Ontology == Globals.Type_Object
                                                                    select objRef).ToList();

        if (objOList_token_variable_list_declaration_configitems.Any())
        {
            OItem_token_variable_list_declaration_configitems = new clsOntologyItem()
            {
                GUID = objOList_token_variable_list_declaration_configitems.First().ID_Other,
                Name = objOList_token_variable_list_declaration_configitems.First().Name_Other,
                GUID_Parent = objOList_token_variable_list_declaration_configitems.First().ID_Parent_Other,
                Type = Globals.Type_Object
            };
        }
        else
        {
            throw new Exception("config err");
        }

        var objOList_token_variable_list_initialize_configitems_attributes = (from objOItem in objDBLevel_Config1.ObjectRels
                                                                              where objOItem.ID_Object == cstrID_Ontology
                                                                              join objRef in objDBLevel_Config2.ObjectRels on objOItem.ID_Other equals objRef.ID_Object
                                                                              where objRef.Name_Object.ToLower() == "token_variable_list_initialize_configitems_attributes".ToLower() && objRef.Ontology == Globals.Type_Object
                                                                              select objRef).ToList();

        if (objOList_token_variable_list_initialize_configitems_attributes.Any())
        {
            OItem_token_variable_list_initialize_configitems_attributes = new clsOntologyItem()
            {
                GUID = objOList_token_variable_list_initialize_configitems_attributes.First().ID_Other,
                Name = objOList_token_variable_list_initialize_configitems_attributes.First().Name_Other,
                GUID_Parent = objOList_token_variable_list_initialize_configitems_attributes.First().ID_Parent_Other,
                Type = Globals.Type_Object
            };
        }
        else
        {
            throw new Exception("config err");
        }

        var objOList_token_variable_list_initialize_configitems_relationtypes = (from objOItem in objDBLevel_Config1.ObjectRels
                                                                                 where objOItem.ID_Object == cstrID_Ontology
                                                                                 join objRef in objDBLevel_Config2.ObjectRels on objOItem.ID_Other equals objRef.ID_Object
                                                                                 where objRef.Name_Object.ToLower() == "token_variable_list_initialize_configitems_relationtypes".ToLower() && objRef.Ontology == Globals.Type_Object
                                                                                 select objRef).ToList();

        if (objOList_token_variable_list_initialize_configitems_relationtypes.Any())
        {
            OItem_token_variable_list_initialize_configitems_relationtypes = new clsOntologyItem()
            {
                GUID = objOList_token_variable_list_initialize_configitems_relationtypes.First().ID_Other,
                Name = objOList_token_variable_list_initialize_configitems_relationtypes.First().Name_Other,
                GUID_Parent = objOList_token_variable_list_initialize_configitems_relationtypes.First().ID_Parent_Other,
                Type = Globals.Type_Object
            };
        }
        else
        {
            throw new Exception("config err");
        }

        var objOList_token_variable_list_initialize_configitems_token = (from objOItem in objDBLevel_Config1.ObjectRels
                                                                         where objOItem.ID_Object == cstrID_Ontology
                                                                         join objRef in objDBLevel_Config2.ObjectRels on objOItem.ID_Other equals objRef.ID_Object
                                                                         where objRef.Name_Object.ToLower() == "token_variable_list_initialize_configitems_token".ToLower() && objRef.Ontology == Globals.Type_Object
                                                                         select objRef).ToList();

        if (objOList_token_variable_list_initialize_configitems_token.Any())
        {
            OItem_token_variable_list_initialize_configitems_token = new clsOntologyItem()
            {
                GUID = objOList_token_variable_list_initialize_configitems_token.First().ID_Other,
                Name = objOList_token_variable_list_initialize_configitems_token.First().Name_Other,
                GUID_Parent = objOList_token_variable_list_initialize_configitems_token.First().ID_Parent_Other,
                Type = Globals.Type_Object
            };
        }
        else
        {
            throw new Exception("config err");
        }

        var objOList_token_variable_list_initialize_configitems_types = (from objOItem in objDBLevel_Config1.ObjectRels
                                                                         where objOItem.ID_Object == cstrID_Ontology
                                                                         join objRef in objDBLevel_Config2.ObjectRels on objOItem.ID_Other equals objRef.ID_Object
                                                                         where objRef.Name_Object.ToLower() == "token_variable_list_initialize_configitems_types".ToLower() && objRef.Ontology == Globals.Type_Object
                                                                         select objRef).ToList();

        if (objOList_token_variable_list_initialize_configitems_types.Any())
        {
            OItem_token_variable_list_initialize_configitems_types = new clsOntologyItem()
            {
                GUID = objOList_token_variable_list_initialize_configitems_types.First().ID_Other,
                Name = objOList_token_variable_list_initialize_configitems_types.First().Name_Other,
                GUID_Parent = objOList_token_variable_list_initialize_configitems_types.First().ID_Parent_Other,
                Type = Globals.Type_Object
            };
        }
        else
        {
            throw new Exception("config err");
        }

        var objOList_token_variable_list_properties = (from objOItem in objDBLevel_Config1.ObjectRels
                                                       where objOItem.ID_Object == cstrID_Ontology
                                                       join objRef in objDBLevel_Config2.ObjectRels on objOItem.ID_Other equals objRef.ID_Object
                                                       where objRef.Name_Object.ToLower() == "token_variable_list_properties".ToLower() && objRef.Ontology == Globals.Type_Object
                                                       select objRef).ToList();

        if (objOList_token_variable_list_properties.Any())
        {
            OItem_token_variable_list_properties = new clsOntologyItem()
            {
                GUID = objOList_token_variable_list_properties.First().ID_Other,
                Name = objOList_token_variable_list_properties.First().Name_Other,
                GUID_Parent = objOList_token_variable_list_properties.First().ID_Parent_Other,
                Type = Globals.Type_Object
            };
        }
        else
        {
            throw new Exception("config err");
        }

        var objOList_token_variable_name_configitem = (from objOItem in objDBLevel_Config1.ObjectRels
                                                       where objOItem.ID_Object == cstrID_Ontology
                                                       join objRef in objDBLevel_Config2.ObjectRels on objOItem.ID_Other equals objRef.ID_Object
                                                       where objRef.Name_Object.ToLower() == "token_variable_name_configitem".ToLower() && objRef.Ontology == Globals.Type_Object
                                                       select objRef).ToList();

        if (objOList_token_variable_name_configitem.Any())
        {
            OItem_token_variable_name_configitem = new clsOntologyItem()
            {
                GUID = objOList_token_variable_name_configitem.First().ID_Other,
                Name = objOList_token_variable_name_configitem.First().Name_Other,
                GUID_Parent = objOList_token_variable_name_configitem.First().ID_Parent_Other,
                Type = Globals.Type_Object
            };
        }
        else
        {
            throw new Exception("config err");
        }

        var objOList_token_xml_clslocalconfig_ontology_xml = (from objOItem in objDBLevel_Config1.ObjectRels
                                                              where objOItem.ID_Object == cstrID_Ontology
                                                              join objRef in objDBLevel_Config2.ObjectRels on objOItem.ID_Other equals objRef.ID_Object
                                                              where objRef.Name_Object.ToLower() == "token_xml_clslocalconfig_ontology_xml".ToLower() && objRef.Ontology == Globals.Type_Object
                                                              select objRef).ToList();

        if (objOList_token_xml_clslocalconfig_ontology_xml.Any())
        {
            OItem_token_xml_clslocalconfig_ontology_xml = new clsOntologyItem()
            {
                GUID = objOList_token_xml_clslocalconfig_ontology_xml.First().ID_Other,
                Name = objOList_token_xml_clslocalconfig_ontology_xml.First().Name_Other,
                GUID_Parent = objOList_token_xml_clslocalconfig_ontology_xml.First().ID_Parent_Other,
                Type = Globals.Type_Object
            };
        }
        else
        {
            throw new Exception("config err");
        }

        var objOList_token_xml_declaration_ontology_configitems = (from objOItem in objDBLevel_Config1.ObjectRels
                                                                   where objOItem.ID_Object == cstrID_Ontology
                                                                   join objRef in objDBLevel_Config2.ObjectRels on objOItem.ID_Other equals objRef.ID_Object
                                                                   where objRef.Name_Object.ToLower() == "token_xml_declaration_ontology_configitems".ToLower() && objRef.Ontology == Globals.Type_Object
                                                                   select objRef).ToList();

        if (objOList_token_xml_declaration_ontology_configitems.Any())
        {
            OItem_token_xml_declaration_ontology_configitems = new clsOntologyItem()
            {
                GUID = objOList_token_xml_declaration_ontology_configitems.First().ID_Other,
                Name = objOList_token_xml_declaration_ontology_configitems.First().Name_Other,
                GUID_Parent = objOList_token_xml_declaration_ontology_configitems.First().ID_Parent_Other,
                Type = Globals.Type_Object
            };
        }
        else
        {
            throw new Exception("config err");
        }

        var objOList_token_xml_initialize_relationtype__configitem_ontology_ = (from objOItem in objDBLevel_Config1.ObjectRels
                                                                                where objOItem.ID_Object == cstrID_Ontology
                                                                                join objRef in objDBLevel_Config2.ObjectRels on objOItem.ID_Other equals objRef.ID_Object
                                                                                where objRef.Name_Object.ToLower() == "token_xml_initialize_relationtype__configitem_ontology_".ToLower() && objRef.Ontology == Globals.Type_Object
                                                                                select objRef).ToList();

        if (objOList_token_xml_initialize_relationtype__configitem_ontology_.Any())
        {
            OItem_token_xml_initialize_relationtype__configitem_ontology_ = new clsOntologyItem()
            {
                GUID = objOList_token_xml_initialize_relationtype__configitem_ontology_.First().ID_Other,
                Name = objOList_token_xml_initialize_relationtype__configitem_ontology_.First().Name_Other,
                GUID_Parent = objOList_token_xml_initialize_relationtype__configitem_ontology_.First().ID_Parent_Other,
                Type = Globals.Type_Object
            };
        }
        else
        {
            throw new Exception("config err");
        }

        var objOList_token_xml_initialize_token__configitem_ontology_ = (from objOItem in objDBLevel_Config1.ObjectRels
                                                                         where objOItem.ID_Object == cstrID_Ontology
                                                                         join objRef in objDBLevel_Config2.ObjectRels on objOItem.ID_Other equals objRef.ID_Object
                                                                         where objRef.Name_Object.ToLower() == "token_xml_initialize_token__configitem_ontology_".ToLower() && objRef.Ontology == Globals.Type_Object
                                                                         select objRef).ToList();

        if (objOList_token_xml_initialize_token__configitem_ontology_.Any())
        {
            OItem_token_xml_initialize_token__configitem_ontology_ = new clsOntologyItem()
            {
                GUID = objOList_token_xml_initialize_token__configitem_ontology_.First().ID_Other,
                Name = objOList_token_xml_initialize_token__configitem_ontology_.First().Name_Other,
                GUID_Parent = objOList_token_xml_initialize_token__configitem_ontology_.First().ID_Parent_Other,
                Type = Globals.Type_Object
            };
        }
        else
        {
            throw new Exception("config err");
        }

        var objOList_token_xml_initilize_attribute__configitem_ontology_ = (from objOItem in objDBLevel_Config1.ObjectRels
                                                                            where objOItem.ID_Object == cstrID_Ontology
                                                                            join objRef in objDBLevel_Config2.ObjectRels on objOItem.ID_Other equals objRef.ID_Object
                                                                            where objRef.Name_Object.ToLower() == "token_xml_initilize_attribute__configitem_ontology_".ToLower() && objRef.Ontology == Globals.Type_Object
                                                                            select objRef).ToList();

        if (objOList_token_xml_initilize_attribute__configitem_ontology_.Any())
        {
            OItem_token_xml_initilize_attribute__configitem_ontology_ = new clsOntologyItem()
            {
                GUID = objOList_token_xml_initilize_attribute__configitem_ontology_.First().ID_Other,
                Name = objOList_token_xml_initilize_attribute__configitem_ontology_.First().Name_Other,
                GUID_Parent = objOList_token_xml_initilize_attribute__configitem_ontology_.First().ID_Parent_Other,
                Type = Globals.Type_Object
            };
        }
        else
        {
            throw new Exception("config err");
        }

        var objOList_token_xml_initilize_type__configitem_ontology_ = (from objOItem in objDBLevel_Config1.ObjectRels
                                                                       where objOItem.ID_Object == cstrID_Ontology
                                                                       join objRef in objDBLevel_Config2.ObjectRels on objOItem.ID_Other equals objRef.ID_Object
                                                                       where objRef.Name_Object.ToLower() == "token_xml_initilize_type__configitem_ontology_".ToLower() && objRef.Ontology == Globals.Type_Object
                                                                       select objRef).ToList();

        if (objOList_token_xml_initilize_type__configitem_ontology_.Any())
        {
            OItem_token_xml_initilize_type__configitem_ontology_ = new clsOntologyItem()
            {
                GUID = objOList_token_xml_initilize_type__configitem_ontology_.First().ID_Other,
                Name = objOList_token_xml_initilize_type__configitem_ontology_.First().Name_Other,
                GUID_Parent = objOList_token_xml_initilize_type__configitem_ontology_.First().ID_Parent_Other,
                Type = Globals.Type_Object
            };
        }
        else
        {
            throw new Exception("config err");
        }

        var objOList_token_xml_property_ontology_configitem = (from objOItem in objDBLevel_Config1.ObjectRels
                                                               where objOItem.ID_Object == cstrID_Ontology
                                                               join objRef in objDBLevel_Config2.ObjectRels on objOItem.ID_Other equals objRef.ID_Object
                                                               where objRef.Name_Object.ToLower() == "token_xml_property_ontology_configitem".ToLower() && objRef.Ontology == Globals.Type_Object
                                                               select objRef).ToList();

        if (objOList_token_xml_property_ontology_configitem.Any())
        {
            OItem_token_xml_property_ontology_configitem = new clsOntologyItem()
            {
                GUID = objOList_token_xml_property_ontology_configitem.First().ID_Other,
                Name = objOList_token_xml_property_ontology_configitem.First().Name_Other,
                GUID_Parent = objOList_token_xml_property_ontology_configitem.First().ID_Parent_Other,
                Type = Globals.Type_Object
            };
        }
        else
        {
            throw new Exception("config err");
        }


    }

    private void get_Config_Classes()
    {
        var objOList_class_code_templates = (from objOItem in objDBLevel_Config1.ObjectRels
                                             where objOItem.ID_Object == cstrID_Ontology
                                             join objRef in objDBLevel_Config2.ObjectRels on objOItem.ID_Other equals objRef.ID_Object
                                             where objRef.Name_Object.ToLower() == "class_code_templates".ToLower() && objRef.Ontology == Globals.Type_Class
                                             select objRef).ToList();

        if (objOList_class_code_templates.Any())
        {
            OItem_class_code_templates = new clsOntologyItem()
            {
                GUID = objOList_class_code_templates.First().ID_Other,
                Name = objOList_class_code_templates.First().Name_Other,
                GUID_Parent = objOList_class_code_templates.First().ID_Parent_Other,
                Type = Globals.Type_Class
            };
        }
        else
        {
            throw new Exception("config err");
        }

        var objOList_class_path = (from objOItem in objDBLevel_Config1.ObjectRels
                                   where objOItem.ID_Object == cstrID_Ontology
                                   join objRef in objDBLevel_Config2.ObjectRels on objOItem.ID_Other equals objRef.ID_Object
                                   where objRef.Name_Object.ToLower() == "class_path".ToLower() && objRef.Ontology == Globals.Type_Class
                                   select objRef).ToList();

        if (objOList_class_path.Any())
        {
            OItem_class_path = new clsOntologyItem()
            {
                GUID = objOList_class_path.First().ID_Other,
                Name = objOList_class_path.First().Name_Other,
                GUID_Parent = objOList_class_path.First().ID_Parent_Other,
                Type = Globals.Type_Class
            };
        }
        else
        {
            throw new Exception("config err");
        }

        var objOList_class_software_project = (from objOItem in objDBLevel_Config1.ObjectRels
                                               where objOItem.ID_Object == cstrID_Ontology
                                               join objRef in objDBLevel_Config2.ObjectRels on objOItem.ID_Other equals objRef.ID_Object
                                               where objRef.Name_Object.ToLower() == "class_software_project".ToLower() && objRef.Ontology == Globals.Type_Class
                                               select objRef).ToList();

        if (objOList_class_software_project.Any())
        {
            OItem_class_software_project = new clsOntologyItem()
            {
                GUID = objOList_class_software_project.First().ID_Other,
                Name = objOList_class_software_project.First().Name_Other,
                GUID_Parent = objOList_class_software_project.First().ID_Parent_Other,
                Type = Globals.Type_Class
            };
        }
        else
        {
            throw new Exception("config err");
        }

        var objOList_class_syntax_highlighting__scintillanet_ = (from objOItem in objDBLevel_Config1.ObjectRels
                                                                 where objOItem.ID_Object == cstrID_Ontology
                                                                 join objRef in objDBLevel_Config2.ObjectRels on objOItem.ID_Other equals objRef.ID_Object
                                                                 where objRef.Name_Object.ToLower() == "class_syntax_highlighting__scintillanet_".ToLower() && objRef.Ontology == Globals.Type_Class
                                                                 select objRef).ToList();

        if (objOList_class_syntax_highlighting__scintillanet_.Any())
        {
            OItem_class_syntax_highlighting__scintillanet_ = new clsOntologyItem()
            {
                GUID = objOList_class_syntax_highlighting__scintillanet_.First().ID_Other,
                Name = objOList_class_syntax_highlighting__scintillanet_.First().Name_Other,
                GUID_Parent = objOList_class_syntax_highlighting__scintillanet_.First().ID_Parent_Other,
                Type = Globals.Type_Class
            };
        }
        else
        {
            throw new Exception("config err");
        }

        var objOList_class_xml = (from objOItem in objDBLevel_Config1.ObjectRels
                                  where objOItem.ID_Object == cstrID_Ontology
                                  join objRef in objDBLevel_Config2.ObjectRels on objOItem.ID_Other equals objRef.ID_Object
                                  where objRef.Name_Object.ToLower() == "class_xml".ToLower() && objRef.Ontology == Globals.Type_Class
                                  select objRef).ToList();

        if (objOList_class_xml.Any())
        {
            OItem_class_xml = new clsOntologyItem()
            {
                GUID = objOList_class_xml.First().ID_Other,
                Name = objOList_class_xml.First().Name_Other,
                GUID_Parent = objOList_class_xml.First().ID_Parent_Other,
                Type = Globals.Type_Class
            };
        }
        else
        {
            throw new Exception("config err");
        }

        var objOList_type_database = (from objOItem in objDBLevel_Config1.ObjectRels
                                      where objOItem.ID_Object == cstrID_Ontology
                                      join objRef in objDBLevel_Config2.ObjectRels on objOItem.ID_Other equals objRef.ID_Object
                                      where objRef.Name_Object.ToLower() == "type_database".ToLower() && objRef.Ontology == Globals.Type_Class
                                      select objRef).ToList();

        if (objOList_type_database.Any())
        {
            OItem_type_database = new clsOntologyItem()
            {
                GUID = objOList_type_database.First().ID_Other,
                Name = objOList_type_database.First().Name_Other,
                GUID_Parent = objOList_type_database.First().ID_Parent_Other,
                Type = Globals.Type_Class
            };
        }
        else
        {
            throw new Exception("config err");
        }

        var objOList_type_database_on_server = (from objOItem in objDBLevel_Config1.ObjectRels
                                                where objOItem.ID_Object == cstrID_Ontology
                                                join objRef in objDBLevel_Config2.ObjectRels on objOItem.ID_Other equals objRef.ID_Object
                                                where objRef.Name_Object.ToLower() == "type_database_on_server".ToLower() && objRef.Ontology == Globals.Type_Class
                                                select objRef).ToList();

        if (objOList_type_database_on_server.Any())
        {
            OItem_type_database_on_server = new clsOntologyItem()
            {
                GUID = objOList_type_database_on_server.First().ID_Other,
                Name = objOList_type_database_on_server.First().Name_Other,
                GUID_Parent = objOList_type_database_on_server.First().ID_Parent_Other,
                Type = Globals.Type_Class
            };
        }
        else
        {
            throw new Exception("config err");
        }

        var objOList_type_database_schema = (from objOItem in objDBLevel_Config1.ObjectRels
                                             where objOItem.ID_Object == cstrID_Ontology
                                             join objRef in objDBLevel_Config2.ObjectRels on objOItem.ID_Other equals objRef.ID_Object
                                             where objRef.Name_Object.ToLower() == "type_database_schema".ToLower() && objRef.Ontology == Globals.Type_Class
                                             select objRef).ToList();

        if (objOList_type_database_schema.Any())
        {
            OItem_type_database_schema = new clsOntologyItem()
            {
                GUID = objOList_type_database_schema.First().ID_Other,
                Name = objOList_type_database_schema.First().Name_Other,
                GUID_Parent = objOList_type_database_schema.First().ID_Parent_Other,
                Type = Globals.Type_Class
            };
        }
        else
        {
            throw new Exception("config err");
        }

        var objOList_type_db_schema_type = (from objOItem in objDBLevel_Config1.ObjectRels
                                            where objOItem.ID_Object == cstrID_Ontology
                                            join objRef in objDBLevel_Config2.ObjectRels on objOItem.ID_Other equals objRef.ID_Object
                                            where objRef.Name_Object.ToLower() == "type_db_schema_type".ToLower() && objRef.Ontology == Globals.Type_Class
                                            select objRef).ToList();

        if (objOList_type_db_schema_type.Any())
        {
            OItem_type_db_schema_type = new clsOntologyItem()
            {
                GUID = objOList_type_db_schema_type.First().ID_Other,
                Name = objOList_type_db_schema_type.First().Name_Other,
                GUID_Parent = objOList_type_db_schema_type.First().ID_Parent_Other,
                Type = Globals.Type_Class
            };
        }
        else
        {
            throw new Exception("config err");
        }

        var objOList_type_dbschema_of_application = (from objOItem in objDBLevel_Config1.ObjectRels
                                                     where objOItem.ID_Object == cstrID_Ontology
                                                     join objRef in objDBLevel_Config2.ObjectRels on objOItem.ID_Other equals objRef.ID_Object
                                                     where objRef.Name_Object.ToLower() == "type_dbschema_of_application".ToLower() && objRef.Ontology == Globals.Type_Class
                                                     select objRef).ToList();

        if (objOList_type_dbschema_of_application.Any())
        {
            OItem_type_dbschema_of_application = new clsOntologyItem()
            {
                GUID = objOList_type_dbschema_of_application.First().ID_Other,
                Name = objOList_type_dbschema_of_application.First().Name_Other,
                GUID_Parent = objOList_type_dbschema_of_application.First().ID_Parent_Other,
                Type = Globals.Type_Class
            };
        }
        else
        {
            throw new Exception("config err");
        }

        var objOList_type_dev_structure = (from objOItem in objDBLevel_Config1.ObjectRels
                                           where objOItem.ID_Object == cstrID_Ontology
                                           join objRef in objDBLevel_Config2.ObjectRels on objOItem.ID_Other equals objRef.ID_Object
                                           where objRef.Name_Object.ToLower() == "type_dev_structure".ToLower() && objRef.Ontology == Globals.Type_Class
                                           select objRef).ToList();

        if (objOList_type_dev_structure.Any())
        {
            OItem_type_dev_structure = new clsOntologyItem()
            {
                GUID = objOList_type_dev_structure.First().ID_Other,
                Name = objOList_type_dev_structure.First().Name_Other,
                GUID_Parent = objOList_type_dev_structure.First().ID_Parent_Other,
                Type = Globals.Type_Class
            };
        }
        else
        {
            throw new Exception("config err");
        }

        var objOList_type_dev_structure__process_type_to_process_ = (from objOItem in objDBLevel_Config1.ObjectRels
                                                                     where objOItem.ID_Object == cstrID_Ontology
                                                                     join objRef in objDBLevel_Config2.ObjectRels on objOItem.ID_Other equals objRef.ID_Object
                                                                     where objRef.Name_Object.ToLower() == "type_dev_structure__process_type_to_process_".ToLower() && objRef.Ontology == Globals.Type_Class
                                                                     select objRef).ToList();

        if (objOList_type_dev_structure__process_type_to_process_.Any())
        {
            OItem_type_dev_structure__process_type_to_process_ = new clsOntologyItem()
            {
                GUID = objOList_type_dev_structure__process_type_to_process_.First().ID_Other,
                Name = objOList_type_dev_structure__process_type_to_process_.First().Name_Other,
                GUID_Parent = objOList_type_dev_structure__process_type_to_process_.First().ID_Parent_Other,
                Type = Globals.Type_Class
            };
        }
        else
        {
            throw new Exception("config err");
        }

        var objOList_type_dev_structure_invoke = (from objOItem in objDBLevel_Config1.ObjectRels
                                                  where objOItem.ID_Object == cstrID_Ontology
                                                  join objRef in objDBLevel_Config2.ObjectRels on objOItem.ID_Other equals objRef.ID_Object
                                                  where objRef.Name_Object.ToLower() == "type_dev_structure_invoke".ToLower() && objRef.Ontology == Globals.Type_Class
                                                  select objRef).ToList();

        if (objOList_type_dev_structure_invoke.Any())
        {
            OItem_type_dev_structure_invoke = new clsOntologyItem()
            {
                GUID = objOList_type_dev_structure_invoke.First().ID_Other,
                Name = objOList_type_dev_structure_invoke.First().Name_Other,
                GUID_Parent = objOList_type_dev_structure_invoke.First().ID_Parent_Other,
                Type = Globals.Type_Class
            };
        }
        else
        {
            throw new Exception("config err");
        }

        var objOList_type_development_libraries = (from objOItem in objDBLevel_Config1.ObjectRels
                                                   where objOItem.ID_Object == cstrID_Ontology
                                                   join objRef in objDBLevel_Config2.ObjectRels on objOItem.ID_Other equals objRef.ID_Object
                                                   where objRef.Name_Object.ToLower() == "type_development_libraries".ToLower() && objRef.Ontology == Globals.Type_Class
                                                   select objRef).ToList();

        if (objOList_type_development_libraries.Any())
        {
            OItem_type_development_libraries = new clsOntologyItem()
            {
                GUID = objOList_type_development_libraries.First().ID_Other,
                Name = objOList_type_development_libraries.First().Name_Other,
                GUID_Parent = objOList_type_development_libraries.First().ID_Parent_Other,
                Type = Globals.Type_Class
            };
        }
        else
        {
            throw new Exception("config err");
        }

        var objOList_type_development_module = (from objOItem in objDBLevel_Config1.ObjectRels
                                                where objOItem.ID_Object == cstrID_Ontology
                                                join objRef in objDBLevel_Config2.ObjectRels on objOItem.ID_Other equals objRef.ID_Object
                                                where objRef.Name_Object.ToLower() == "type_development_module".ToLower() && objRef.Ontology == Globals.Type_Class
                                                select objRef).ToList();

        if (objOList_type_development_module.Any())
        {
            OItem_type_development_module = new clsOntologyItem()
            {
                GUID = objOList_type_development_module.First().ID_Other,
                Name = objOList_type_development_module.First().Name_Other,
                GUID_Parent = objOList_type_development_module.First().ID_Parent_Other,
                Type = Globals.Type_Class
            };
        }
        else
        {
            throw new Exception("config err");
        }

        var objOList_type_development_structure = (from objOItem in objDBLevel_Config1.ObjectRels
                                                   where objOItem.ID_Object == cstrID_Ontology
                                                   join objRef in objDBLevel_Config2.ObjectRels on objOItem.ID_Other equals objRef.ID_Object
                                                   where objRef.Name_Object.ToLower() == "type_development_structure".ToLower() && objRef.Ontology == Globals.Type_Class
                                                   select objRef).ToList();

        if (objOList_type_development_structure.Any())
        {
            OItem_type_development_structure = new clsOntologyItem()
            {
                GUID = objOList_type_development_structure.First().ID_Other,
                Name = objOList_type_development_structure.First().Name_Other,
                GUID_Parent = objOList_type_development_structure.First().ID_Parent_Other,
                Type = Globals.Type_Class
            };
        }
        else
        {
            throw new Exception("config err");
        }

        var objOList_type_developmentconfig = (from objOItem in objDBLevel_Config1.ObjectRels
                                               where objOItem.ID_Object == cstrID_Ontology
                                               join objRef in objDBLevel_Config2.ObjectRels on objOItem.ID_Other equals objRef.ID_Object
                                               where objRef.Name_Object.ToLower() == "type_developmentconfig".ToLower() && objRef.Ontology == Globals.Type_Class
                                               select objRef).ToList();

        if (objOList_type_developmentconfig.Any())
        {
            OItem_type_developmentconfig = new clsOntologyItem()
            {
                GUID = objOList_type_developmentconfig.First().ID_Other,
                Name = objOList_type_developmentconfig.First().Name_Other,
                GUID_Parent = objOList_type_developmentconfig.First().ID_Parent_Other,
                Type = Globals.Type_Class
            };
        }
        else
        {
            throw new Exception("config err");
        }

        var objOList_type_developmentconfigitem = (from objOItem in objDBLevel_Config1.ObjectRels
                                                   where objOItem.ID_Object == cstrID_Ontology
                                                   join objRef in objDBLevel_Config2.ObjectRels on objOItem.ID_Other equals objRef.ID_Object
                                                   where objRef.Name_Object.ToLower() == "type_developmentconfigitem".ToLower() && objRef.Ontology == Globals.Type_Class
                                                   select objRef).ToList();

        if (objOList_type_developmentconfigitem.Any())
        {
            OItem_type_developmentconfigitem = new clsOntologyItem()
            {
                GUID = objOList_type_developmentconfigitem.First().ID_Other,
                Name = objOList_type_developmentconfigitem.First().Name_Other,
                GUID_Parent = objOList_type_developmentconfigitem.First().ID_Parent_Other,
                Type = Globals.Type_Class
            };
        }
        else
        {
            throw new Exception("config err");
        }

        var objOList_type_developmentversion = (from objOItem in objDBLevel_Config1.ObjectRels
                                                where objOItem.ID_Object == cstrID_Ontology
                                                join objRef in objDBLevel_Config2.ObjectRels on objOItem.ID_Other equals objRef.ID_Object
                                                where objRef.Name_Object.ToLower() == "type_developmentversion".ToLower() && objRef.Ontology == Globals.Type_Class
                                                select objRef).ToList();

        if (objOList_type_developmentversion.Any())
        {
            OItem_type_developmentversion = new clsOntologyItem()
            {
                GUID = objOList_type_developmentversion.First().ID_Other,
                Name = objOList_type_developmentversion.First().Name_Other,
                GUID_Parent = objOList_type_developmentversion.First().ID_Parent_Other,
                Type = Globals.Type_Class
            };
        }
        else
        {
            throw new Exception("config err");
        }

        var objOList_type_directions = (from objOItem in objDBLevel_Config1.ObjectRels
                                        where objOItem.ID_Object == cstrID_Ontology
                                        join objRef in objDBLevel_Config2.ObjectRels on objOItem.ID_Other equals objRef.ID_Object
                                        where objRef.Name_Object.ToLower() == "type_directions".ToLower() && objRef.Ontology == Globals.Type_Class
                                        select objRef).ToList();

        if (objOList_type_directions.Any())
        {
            OItem_type_directions = new clsOntologyItem()
            {
                GUID = objOList_type_directions.First().ID_Other,
                Name = objOList_type_directions.First().Name_Other,
                GUID_Parent = objOList_type_directions.First().ID_Parent_Other,
                Type = Globals.Type_Class
            };
        }
        else
        {
            throw new Exception("config err");
        }

        var objOList_type_export_mode = (from objOItem in objDBLevel_Config1.ObjectRels
                                         where objOItem.ID_Object == cstrID_Ontology
                                         join objRef in objDBLevel_Config2.ObjectRels on objOItem.ID_Other equals objRef.ID_Object
                                         where objRef.Name_Object.ToLower() == "type_export_mode".ToLower() && objRef.Ontology == Globals.Type_Class
                                         select objRef).ToList();

        if (objOList_type_export_mode.Any())
        {
            OItem_type_export_mode = new clsOntologyItem()
            {
                GUID = objOList_type_export_mode.First().ID_Other,
                Name = objOList_type_export_mode.First().Name_Other,
                GUID_Parent = objOList_type_export_mode.First().ID_Parent_Other,
                Type = Globals.Type_Class
            };
        }
        else
        {
            throw new Exception("config err");
        }

        var objOList_type_file = (from objOItem in objDBLevel_Config1.ObjectRels
                                  where objOItem.ID_Object == cstrID_Ontology
                                  join objRef in objDBLevel_Config2.ObjectRels on objOItem.ID_Other equals objRef.ID_Object
                                  where objRef.Name_Object.ToLower() == "type_file".ToLower() && objRef.Ontology == Globals.Type_Class
                                  select objRef).ToList();

        if (objOList_type_file.Any())
        {
            OItem_type_file = new clsOntologyItem()
            {
                GUID = objOList_type_file.First().ID_Other,
                Name = objOList_type_file.First().Name_Other,
                GUID_Parent = objOList_type_file.First().ID_Parent_Other,
                Type = Globals.Type_Class
            };
        }
        else
        {
            throw new Exception("config err");
        }

        var objOList_type_folder = (from objOItem in objDBLevel_Config1.ObjectRels
                                    where objOItem.ID_Object == cstrID_Ontology
                                    join objRef in objDBLevel_Config2.ObjectRels on objOItem.ID_Other equals objRef.ID_Object
                                    where objRef.Name_Object.ToLower() == "type_folder".ToLower() && objRef.Ontology == Globals.Type_Class
                                    select objRef).ToList();

        if (objOList_type_folder.Any())
        {
            OItem_type_folder = new clsOntologyItem()
            {
                GUID = objOList_type_folder.First().ID_Other,
                Name = objOList_type_folder.First().Name_Other,
                GUID_Parent = objOList_type_folder.First().ID_Parent_Other,
                Type = Globals.Type_Class
            };
        }
        else
        {
            throw new Exception("config err");
        }

        var objOList_type_gui_caption = (from objOItem in objDBLevel_Config1.ObjectRels
                                         where objOItem.ID_Object == cstrID_Ontology
                                         join objRef in objDBLevel_Config2.ObjectRels on objOItem.ID_Other equals objRef.ID_Object
                                         where objRef.Name_Object.ToLower() == "type_gui_caption".ToLower() && objRef.Ontology == Globals.Type_Class
                                         select objRef).ToList();

        if (objOList_type_gui_caption.Any())
        {
            OItem_type_gui_caption = new clsOntologyItem()
            {
                GUID = objOList_type_gui_caption.First().ID_Other,
                Name = objOList_type_gui_caption.First().Name_Other,
                GUID_Parent = objOList_type_gui_caption.First().ID_Parent_Other,
                Type = Globals.Type_Class
            };
        }
        else
        {
            throw new Exception("config err");
        }

        var objOList_type_gui_entires = (from objOItem in objDBLevel_Config1.ObjectRels
                                         where objOItem.ID_Object == cstrID_Ontology
                                         join objRef in objDBLevel_Config2.ObjectRels on objOItem.ID_Other equals objRef.ID_Object
                                         where objRef.Name_Object.ToLower() == "type_gui_entires".ToLower() && objRef.Ontology == Globals.Type_Class
                                         select objRef).ToList();

        if (objOList_type_gui_entires.Any())
        {
            OItem_type_gui_entires = new clsOntologyItem()
            {
                GUID = objOList_type_gui_entires.First().ID_Other,
                Name = objOList_type_gui_entires.First().Name_Other,
                GUID_Parent = objOList_type_gui_entires.First().ID_Parent_Other,
                Type = Globals.Type_Class
            };
        }
        else
        {
            throw new Exception("config err");
        }

        var objOList_type_image_video_management = (from objOItem in objDBLevel_Config1.ObjectRels
                                                    where objOItem.ID_Object == cstrID_Ontology
                                                    join objRef in objDBLevel_Config2.ObjectRels on objOItem.ID_Other equals objRef.ID_Object
                                                    where objRef.Name_Object.ToLower() == "type_image_video_management".ToLower() && objRef.Ontology == Globals.Type_Class
                                                    select objRef).ToList();

        if (objOList_type_image_video_management.Any())
        {
            OItem_type_image_video_management = new clsOntologyItem()
            {
                GUID = objOList_type_image_video_management.First().ID_Other,
                Name = objOList_type_image_video_management.First().Name_Other,
                GUID_Parent = objOList_type_image_video_management.First().ID_Parent_Other,
                Type = Globals.Type_Class
            };
        }
        else
        {
            throw new Exception("config err");
        }

        var objOList_type_language = (from objOItem in objDBLevel_Config1.ObjectRels
                                      where objOItem.ID_Object == cstrID_Ontology
                                      join objRef in objDBLevel_Config2.ObjectRels on objOItem.ID_Other equals objRef.ID_Object
                                      where objRef.Name_Object.ToLower() == "type_language".ToLower() && objRef.Ontology == Globals.Type_Class
                                      select objRef).ToList();

        if (objOList_type_language.Any())
        {
            OItem_type_language = new clsOntologyItem()
            {
                GUID = objOList_type_language.First().ID_Other,
                Name = objOList_type_language.First().Name_Other,
                GUID_Parent = objOList_type_language.First().ID_Parent_Other,
                Type = Globals.Type_Class
            };
        }
        else
        {
            throw new Exception("config err");
        }

        var objOList_type_localized_message = (from objOItem in objDBLevel_Config1.ObjectRels
                                               where objOItem.ID_Object == cstrID_Ontology
                                               join objRef in objDBLevel_Config2.ObjectRels on objOItem.ID_Other equals objRef.ID_Object
                                               where objRef.Name_Object.ToLower() == "type_localized_message".ToLower() && objRef.Ontology == Globals.Type_Class
                                               select objRef).ToList();

        if (objOList_type_localized_message.Any())
        {
            OItem_type_localized_message = new clsOntologyItem()
            {
                GUID = objOList_type_localized_message.First().ID_Other,
                Name = objOList_type_localized_message.First().Name_Other,
                GUID_Parent = objOList_type_localized_message.First().ID_Parent_Other,
                Type = Globals.Type_Class
            };
        }
        else
        {
            throw new Exception("config err");
        }

        var objOList_type_localized_names = (from objOItem in objDBLevel_Config1.ObjectRels
                                             where objOItem.ID_Object == cstrID_Ontology
                                             join objRef in objDBLevel_Config2.ObjectRels on objOItem.ID_Other equals objRef.ID_Object
                                             where objRef.Name_Object.ToLower() == "type_localized_names".ToLower() && objRef.Ontology == Globals.Type_Class
                                             select objRef).ToList();

        if (objOList_type_localized_names.Any())
        {
            OItem_type_localized_names = new clsOntologyItem()
            {
                GUID = objOList_type_localized_names.First().ID_Other,
                Name = objOList_type_localized_names.First().Name_Other,
                GUID_Parent = objOList_type_localized_names.First().ID_Parent_Other,
                Type = Globals.Type_Class
            };
        }
        else
        {
            throw new Exception("config err");
        }

        var objOList_type_localizeddescription = (from objOItem in objDBLevel_Config1.ObjectRels
                                                  where objOItem.ID_Object == cstrID_Ontology
                                                  join objRef in objDBLevel_Config2.ObjectRels on objOItem.ID_Other equals objRef.ID_Object
                                                  where objRef.Name_Object.ToLower() == "type_localizeddescription".ToLower() && objRef.Ontology == Globals.Type_Class
                                                  select objRef).ToList();

        if (objOList_type_localizeddescription.Any())
        {
            OItem_type_localizeddescription = new clsOntologyItem()
            {
                GUID = objOList_type_localizeddescription.First().ID_Other,
                Name = objOList_type_localizeddescription.First().Name_Other,
                GUID_Parent = objOList_type_localizeddescription.First().ID_Parent_Other,
                Type = Globals.Type_Class
            };
        }
        else
        {
            throw new Exception("config err");
        }

        var objOList_type_logentry = (from objOItem in objDBLevel_Config1.ObjectRels
                                      where objOItem.ID_Object == cstrID_Ontology
                                      join objRef in objDBLevel_Config2.ObjectRels on objOItem.ID_Other equals objRef.ID_Object
                                      where objRef.Name_Object.ToLower() == "type_logentry".ToLower() && objRef.Ontology == Globals.Type_Class
                                      select objRef).ToList();

        if (objOList_type_logentry.Any())
        {
            OItem_type_logentry = new clsOntologyItem()
            {
                GUID = objOList_type_logentry.First().ID_Other,
                Name = objOList_type_logentry.First().Name_Other,
                GUID_Parent = objOList_type_logentry.First().ID_Parent_Other,
                Type = Globals.Type_Class
            };
        }
        else
        {
            throw new Exception("config err");
        }

        var objOList_type_logstate = (from objOItem in objDBLevel_Config1.ObjectRels
                                      where objOItem.ID_Object == cstrID_Ontology
                                      join objRef in objDBLevel_Config2.ObjectRels on objOItem.ID_Other equals objRef.ID_Object
                                      where objRef.Name_Object.ToLower() == "type_logstate".ToLower() && objRef.Ontology == Globals.Type_Class
                                      select objRef).ToList();

        if (objOList_type_logstate.Any())
        {
            OItem_type_logstate = new clsOntologyItem()
            {
                GUID = objOList_type_logstate.First().ID_Other,
                Name = objOList_type_logstate.First().Name_Other,
                GUID_Parent = objOList_type_logstate.First().ID_Parent_Other,
                Type = Globals.Type_Class
            };
        }
        else
        {
            throw new Exception("config err");
        }

        var objOList_type_messages = (from objOItem in objDBLevel_Config1.ObjectRels
                                      where objOItem.ID_Object == cstrID_Ontology
                                      join objRef in objDBLevel_Config2.ObjectRels on objOItem.ID_Other equals objRef.ID_Object
                                      where objRef.Name_Object.ToLower() == "type_messages".ToLower() && objRef.Ontology == Globals.Type_Class
                                      select objRef).ToList();

        if (objOList_type_messages.Any())
        {
            OItem_type_messages = new clsOntologyItem()
            {
                GUID = objOList_type_messages.First().ID_Other,
                Name = objOList_type_messages.First().Name_Other,
                GUID_Parent = objOList_type_messages.First().ID_Parent_Other,
                Type = Globals.Type_Class
            };
        }
        else
        {
            throw new Exception("config err");
        }

        var objOList_type_module = (from objOItem in objDBLevel_Config1.ObjectRels
                                    where objOItem.ID_Object == cstrID_Ontology
                                    join objRef in objDBLevel_Config2.ObjectRels on objOItem.ID_Other equals objRef.ID_Object
                                    where objRef.Name_Object.ToLower() == "type_module".ToLower() && objRef.Ontology == Globals.Type_Class
                                    select objRef).ToList();

        if (objOList_type_module.Any())
        {
            OItem_type_module = new clsOntologyItem()
            {
                GUID = objOList_type_module.First().ID_Other,
                Name = objOList_type_module.First().Name_Other,
                GUID_Parent = objOList_type_module.First().ID_Parent_Other,
                Type = Globals.Type_Class
            };
        }
        else
        {
            throw new Exception("config err");
        }

        var objOList_type_objects = (from objOItem in objDBLevel_Config1.ObjectRels
                                     where objOItem.ID_Object == cstrID_Ontology
                                     join objRef in objDBLevel_Config2.ObjectRels on objOItem.ID_Other equals objRef.ID_Object
                                     where objRef.Name_Object.ToLower() == "type_objects".ToLower() && objRef.Ontology == Globals.Type_Class
                                     select objRef).ToList();

        if (objOList_type_objects.Any())
        {
            OItem_type_objects = new clsOntologyItem()
            {
                GUID = objOList_type_objects.First().ID_Other,
                Name = objOList_type_objects.First().Name_Other,
                GUID_Parent = objOList_type_objects.First().ID_Parent_Other,
                Type = Globals.Type_Class
            };
        }
        else
        {
            throw new Exception("config err");
        }

        var objOList_type_ontology_export = (from objOItem in objDBLevel_Config1.ObjectRels
                                             where objOItem.ID_Object == cstrID_Ontology
                                             join objRef in objDBLevel_Config2.ObjectRels on objOItem.ID_Other equals objRef.ID_Object
                                             where objRef.Name_Object.ToLower() == "type_ontology_export".ToLower() && objRef.Ontology == Globals.Type_Class
                                             select objRef).ToList();

        if (objOList_type_ontology_export.Any())
        {
            OItem_type_ontology_export = new clsOntologyItem()
            {
                GUID = objOList_type_ontology_export.First().ID_Other,
                Name = objOList_type_ontology_export.First().Name_Other,
                GUID_Parent = objOList_type_ontology_export.First().ID_Parent_Other,
                Type = Globals.Type_Class
            };
        }
        else
        {
            throw new Exception("config err");
        }

        var objOList_type_parameter_dev_structure = (from objOItem in objDBLevel_Config1.ObjectRels
                                                     where objOItem.ID_Object == cstrID_Ontology
                                                     join objRef in objDBLevel_Config2.ObjectRels on objOItem.ID_Other equals objRef.ID_Object
                                                     where objRef.Name_Object.ToLower() == "type_parameter_dev_structure".ToLower() && objRef.Ontology == Globals.Type_Class
                                                     select objRef).ToList();

        if (objOList_type_parameter_dev_structure.Any())
        {
            OItem_type_parameter_dev_structure = new clsOntologyItem()
            {
                GUID = objOList_type_parameter_dev_structure.First().ID_Other,
                Name = objOList_type_parameter_dev_structure.First().Name_Other,
                GUID_Parent = objOList_type_parameter_dev_structure.First().ID_Parent_Other,
                Type = Globals.Type_Class
            };
        }
        else
        {
            throw new Exception("config err");
        }

        var objOList_type_process = (from objOItem in objDBLevel_Config1.ObjectRels
                                     where objOItem.ID_Object == cstrID_Ontology
                                     join objRef in objDBLevel_Config2.ObjectRels on objOItem.ID_Other equals objRef.ID_Object
                                     where objRef.Name_Object.ToLower() == "type_process".ToLower() && objRef.Ontology == Globals.Type_Class
                                     select objRef).ToList();

        if (objOList_type_process.Any())
        {
            OItem_type_process = new clsOntologyItem()
            {
                GUID = objOList_type_process.First().ID_Other,
                Name = objOList_type_process.First().Name_Other,
                GUID_Parent = objOList_type_process.First().ID_Parent_Other,
                Type = Globals.Type_Class
            };
        }
        else
        {
            throw new Exception("config err");
        }

        var objOList_type_programinglanguage = (from objOItem in objDBLevel_Config1.ObjectRels
                                                where objOItem.ID_Object == cstrID_Ontology
                                                join objRef in objDBLevel_Config2.ObjectRels on objOItem.ID_Other equals objRef.ID_Object
                                                where objRef.Name_Object.ToLower() == "type_programinglanguage".ToLower() && objRef.Ontology == Globals.Type_Class
                                                select objRef).ToList();

        if (objOList_type_programinglanguage.Any())
        {
            OItem_type_programinglanguage = new clsOntologyItem()
            {
                GUID = objOList_type_programinglanguage.First().ID_Other,
                Name = objOList_type_programinglanguage.First().Name_Other,
                GUID_Parent = objOList_type_programinglanguage.First().ID_Parent_Other,
                Type = Globals.Type_Class
            };
        }
        else
        {
            throw new Exception("config err");
        }

        var objOList_type_scene = (from objOItem in objDBLevel_Config1.ObjectRels
                                   where objOItem.ID_Object == cstrID_Ontology
                                   join objRef in objDBLevel_Config2.ObjectRels on objOItem.ID_Other equals objRef.ID_Object
                                   where objRef.Name_Object.ToLower() == "type_scene".ToLower() && objRef.Ontology == Globals.Type_Class
                                   select objRef).ToList();

        if (objOList_type_scene.Any())
        {
            OItem_type_scene = new clsOntologyItem()
            {
                GUID = objOList_type_scene.First().ID_Other,
                Name = objOList_type_scene.First().Name_Other,
                GUID_Parent = objOList_type_scene.First().ID_Parent_Other,
                Type = Globals.Type_Class
            };
        }
        else
        {
            throw new Exception("config err");
        }

        var objOList_type_sem_items_to_expot_with_children = (from objOItem in objDBLevel_Config1.ObjectRels
                                                              where objOItem.ID_Object == cstrID_Ontology
                                                              join objRef in objDBLevel_Config2.ObjectRels on objOItem.ID_Other equals objRef.ID_Object
                                                              where objRef.Name_Object.ToLower() == "type_sem_items_to_expot_with_children".ToLower() && objRef.Ontology == Globals.Type_Class
                                                              select objRef).ToList();

        if (objOList_type_sem_items_to_expot_with_children.Any())
        {
            OItem_type_sem_items_to_expot_with_children = new clsOntologyItem()
            {
                GUID = objOList_type_sem_items_to_expot_with_children.First().ID_Other,
                Name = objOList_type_sem_items_to_expot_with_children.First().Name_Other,
                GUID_Parent = objOList_type_sem_items_to_expot_with_children.First().ID_Parent_Other,
                Type = Globals.Type_Class
            };
        }
        else
        {
            throw new Exception("config err");
        }

        var objOList_type_server = (from objOItem in objDBLevel_Config1.ObjectRels
                                    where objOItem.ID_Object == cstrID_Ontology
                                    join objRef in objDBLevel_Config2.ObjectRels on objOItem.ID_Other equals objRef.ID_Object
                                    where objRef.Name_Object.ToLower() == "type_server".ToLower() && objRef.Ontology == Globals.Type_Class
                                    select objRef).ToList();

        if (objOList_type_server.Any())
        {
            OItem_type_server = new clsOntologyItem()
            {
                GUID = objOList_type_server.First().ID_Other,
                Name = objOList_type_server.First().Name_Other,
                GUID_Parent = objOList_type_server.First().ID_Parent_Other,
                Type = Globals.Type_Class
            };
        }
        else
        {
            throw new Exception("config err");
        }

        var objOList_type_softwaredevelopment = (from objOItem in objDBLevel_Config1.ObjectRels
                                                 where objOItem.ID_Object == cstrID_Ontology
                                                 join objRef in objDBLevel_Config2.ObjectRels on objOItem.ID_Other equals objRef.ID_Object
                                                 where objRef.Name_Object.ToLower() == "type_softwaredevelopment".ToLower() && objRef.Ontology == Globals.Type_Class
                                                 select objRef).ToList();

        if (objOList_type_softwaredevelopment.Any())
        {
            OItem_type_softwaredevelopment = new clsOntologyItem()
            {
                GUID = objOList_type_softwaredevelopment.First().ID_Other,
                Name = objOList_type_softwaredevelopment.First().Name_Other,
                GUID_Parent = objOList_type_softwaredevelopment.First().ID_Parent_Other,
                Type = Globals.Type_Class
            };
        }
        else
        {
            throw new Exception("config err");
        }

        var objOList_type_structure_type = (from objOItem in objDBLevel_Config1.ObjectRels
                                            where objOItem.ID_Object == cstrID_Ontology
                                            join objRef in objDBLevel_Config2.ObjectRels on objOItem.ID_Other equals objRef.ID_Object
                                            where objRef.Name_Object.ToLower() == "type_structure_type".ToLower() && objRef.Ontology == Globals.Type_Class
                                            select objRef).ToList();

        if (objOList_type_structure_type.Any())
        {
            OItem_type_structure_type = new clsOntologyItem()
            {
                GUID = objOList_type_structure_type.First().ID_Other,
                Name = objOList_type_structure_type.First().Name_Other,
                GUID_Parent = objOList_type_structure_type.First().ID_Parent_Other,
                Type = Globals.Type_Class
            };
        }
        else
        {
            throw new Exception("config err");
        }

        var objOList_type_structure_type_with_aspects = (from objOItem in objDBLevel_Config1.ObjectRels
                                                         where objOItem.ID_Object == cstrID_Ontology
                                                         join objRef in objDBLevel_Config2.ObjectRels on objOItem.ID_Other equals objRef.ID_Object
                                                         where objRef.Name_Object.ToLower() == "type_structure_type_with_aspects".ToLower() && objRef.Ontology == Globals.Type_Class
                                                         select objRef).ToList();

        if (objOList_type_structure_type_with_aspects.Any())
        {
            OItem_type_structure_type_with_aspects = new clsOntologyItem()
            {
                GUID = objOList_type_structure_type_with_aspects.First().ID_Other,
                Name = objOList_type_structure_type_with_aspects.First().Name_Other,
                GUID_Parent = objOList_type_structure_type_with_aspects.First().ID_Parent_Other,
                Type = Globals.Type_Class
            };
        }
        else
        {
            throw new Exception("config err");
        }

        var objOList_type_structure_validity = (from objOItem in objDBLevel_Config1.ObjectRels
                                                where objOItem.ID_Object == cstrID_Ontology
                                                join objRef in objDBLevel_Config2.ObjectRels on objOItem.ID_Other equals objRef.ID_Object
                                                where objRef.Name_Object.ToLower() == "type_structure_validity".ToLower() && objRef.Ontology == Globals.Type_Class
                                                select objRef).ToList();

        if (objOList_type_structure_validity.Any())
        {
            OItem_type_structure_validity = new clsOntologyItem()
            {
                GUID = objOList_type_structure_validity.First().ID_Other,
                Name = objOList_type_structure_validity.First().Name_Other,
                GUID_Parent = objOList_type_structure_validity.First().ID_Parent_Other,
                Type = Globals.Type_Class
            };
        }
        else
        {
            throw new Exception("config err");
        }

        var objOList_type_tooltip_messages = (from objOItem in objDBLevel_Config1.ObjectRels
                                              where objOItem.ID_Object == cstrID_Ontology
                                              join objRef in objDBLevel_Config2.ObjectRels on objOItem.ID_Other equals objRef.ID_Object
                                              where objRef.Name_Object.ToLower() == "type_tooltip_messages".ToLower() && objRef.Ontology == Globals.Type_Class
                                              select objRef).ToList();

        if (objOList_type_tooltip_messages.Any())
        {
            OItem_type_tooltip_messages = new clsOntologyItem()
            {
                GUID = objOList_type_tooltip_messages.First().ID_Other,
                Name = objOList_type_tooltip_messages.First().Name_Other,
                GUID_Parent = objOList_type_tooltip_messages.First().ID_Parent_Other,
                Type = Globals.Type_Class
            };
        }
        else
        {
            throw new Exception("config err");
        }

        var objOList_type_user = (from objOItem in objDBLevel_Config1.ObjectRels
                                  where objOItem.ID_Object == cstrID_Ontology
                                  join objRef in objDBLevel_Config2.ObjectRels on objOItem.ID_Other equals objRef.ID_Object
                                  where objRef.Name_Object.ToLower() == "type_user".ToLower() && objRef.Ontology == Globals.Type_Class
                                  select objRef).ToList();

        if (objOList_type_user.Any())
        {
            OItem_type_user = new clsOntologyItem()
            {
                GUID = objOList_type_user.First().ID_Other,
                Name = objOList_type_user.First().Name_Other,
                GUID_Parent = objOList_type_user.First().ID_Parent_Other,
                Type = Globals.Type_Class
            };
        }
        else
        {
            throw new Exception("config err");
        }

        var objOList_type_wiki_implementation = (from objOItem in objDBLevel_Config1.ObjectRels
                                                 where objOItem.ID_Object == cstrID_Ontology
                                                 join objRef in objDBLevel_Config2.ObjectRels on objOItem.ID_Other equals objRef.ID_Object
                                                 where objRef.Name_Object.ToLower() == "type_wiki_implementation".ToLower() && objRef.Ontology == Globals.Type_Class
                                                 select objRef).ToList();

        if (objOList_type_wiki_implementation.Any())
        {
            OItem_type_wiki_implementation = new clsOntologyItem()
            {
                GUID = objOList_type_wiki_implementation.First().ID_Other,
                Name = objOList_type_wiki_implementation.First().Name_Other,
                GUID_Parent = objOList_type_wiki_implementation.First().ID_Parent_Other,
                Type = Globals.Type_Class
            };
        }
        else
        {
            throw new Exception("config err");
        }


    }

        public string IdLocalConfig
        {
            get
            {
                var attrib =
                      Assembly.GetExecutingAssembly()
                          .GetCustomAttributes(true)
                          .FirstOrDefault(objAttribute => objAttribute is GuidAttribute);
                if (attrib != null)
                {
                    return ((GuidAttribute)attrib).Value;
                }
                else
                {
                    return null;
                }
            }
        }
    }

}